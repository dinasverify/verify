<!-- global js -->

		<script src="<?php echo $themes_url; ?>js/app.js" type="text/javascript"></script>
		<!-- end of global js -->
		<script src="<?php echo $themes_url; ?>vendors/moment/js/moment.min.js" type="text/javascript"></script>
		<script src="<?php echo $themes_url; ?>vendors/datetime/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
		<script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.min.js" type="text/javascript"></script>
		<script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.en.js" type="text/javascript"></script>
		<script src="<?php echo $themes_url; ?>js/custom_js/advanceddate_pickers.js"></script>

		<!-- begining of page level js -->
		<script src="<?php echo $themes_url; ?>vendors/iCheck/js/icheck.js"></script>
		<script src="<?php echo $themes_url; ?>js/custom_js/radio_checkbox.js"></script>

		<!-- begining of page level js -->
		<script src="<?php echo $themes_url; ?>vendors/select2/js/select2.js" type="text/javascript"></script>
		<script src="<?php echo $themes_url; ?>vendors/bootstrapwizard/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
		<script src="<?php echo $themes_url; ?>vendors/bootstrapvalidator/js/bootstrapValidator.min.js" type="text/javascript"></script>
		<script src="<?php echo $themes_url; ?>js/custom_js/form_wizards.js" type="text/javascript"></script>

		<!-- begining of page level js -->
		<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/dataTables.buttons.js"></script>
		<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/dataTables.responsive.js"></script>
		<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/dataTables.rowReorder.js"></script>
		<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/buttons.colVis.js"></script>
		<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/buttons.html5.js"></script>
		<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/buttons.bootstrap.js"></script>
		<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/buttons.print.js"></script>
		<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/dataTables.scroller.js"></script>
		<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/simple-table.js"></script>
		<script src="<?php echo $themes_url; ?>js/custom_js/advanced_datatables.js" type="text/javascript"></script>
		
		<!-- begining of page level js -->
		<script src="<?php echo $themes_url; ?>vendors/bootstrap-fileinput/js/fileinput.min.js" type="text/javascript"></script>
		<script src="<?php echo $themes_url; ?>js/custom_js/form_elements.js"></script>

		<!-- Select2 -->
		<script src="<?php echo $themes_url; ?>vendors/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript"></script>
		<script src="<?php echo $themes_url; ?>vendors/select2/js/select2.js" type="text/javascript"></script>
		<script src="<?php echo $themes_url; ?>vendors/selectize/js/standalone/selectize.min.js" type="text/javascript"></script>
		<script src="<?php echo $themes_url; ?>vendors/selectric/js/jquery.selectric.min.js" type="text/javascript"></script>
		<script src="<?php echo $themes_url; ?>js/custom_js/custom_elements.js" type="text/javascript"></script>
		
		<!-- DATATABLE -->
		<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/editable-table/js/mindmup-editabletable.js"></script>
		<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/bootstrap-table/js/bootstrap-table.min.js"></script>
		<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/tableExport.jquery.plugin/tableExport.min.js"></script>
		<script src="<?php echo $themes_url; ?>js/custom_js/bootstrap_tables.js" type="text/javascript"></script>
		<!--<script src="<?php echo $themes_url; ?>bootstrap-datetimepicker-master/build/js/bootstrap-datetimepicker.min.js"></script>-->
		<!-- end of page level js -->

<script>
	setTimeout(function() {
		location.href = '<?php echo site_url('auth/login'); ?>';
	}, 216000);
</script>