<?php

class Mdashboard extends CI_Model {

    function countDataNew(){
        $this->db->select('COUNT(*) AS total');
        $this->db->where('status','1');
        $result = $this->db->get('ms_verify')->row();
        return $result->total;
    }

    function countDataVerify(){
        $this->db->select('COUNT(*) AS total');
        $this->db->where('status','2');
        $result = $this->db->get('ms_verify')->row();
        return $result->total;
    }

    function countDataAccept(){
        $this->db->select('COUNT(*) AS total');
        $this->db->where('status','3');
        $result = $this->db->get('ms_verify')->row();
        return $result->total;
    }

    function getDataNew(){

        $result = $this->db->get_where('ms_verify',array('status' => '1'))->result_array();
        return $result;
    }

    function saveNewDocument(){
        $data = array(
            'document_number' => $this->input->post('input_document_number'),
            'date_added' => date('Y-m-d h:i:s'),
            'date_modified' => date('Y-m-d h:i:s'),
            'status' => '1'
        );

        $this->db->insert('ms_verify',$data);

    }

    function getDataVerify(){

        $result = $this->db->get_where('ms_verify',array('status' => '2'))->result_array();
        return $result;
    }

    function saveVerifyDocument(){
        $input_document_number = $this->input->post('input_document_number');
        $data = array(
            'date_modified' => date('Y-m-d h:i:s'),
            'status' => '2'
        );
        $this->db->where('document_number',$input_document_number);
        $this->db->update('ms_verify',$data);
    }

    function getDataAccept(){

        $result = $this->db->get_where('ms_verify',array('status' => '3'))->result_array();
        return $result;
    }

    function saveAcceptDocument(){
        $input_document_number = $this->input->post('input_document_number');
        $data = array(
            'date_modified' => date('Y-m-d h:i:s'),
            'status' => '3'
        );
        $this->db->where('document_number',$input_document_number);
        $this->db->update('ms_verify',$data);
    }

}
