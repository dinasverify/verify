<?php

class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('dashboard/Mdashboard','mdash');
    }



    function index() {
        $this->template->
        // $data['total_new'] = $this->mdash->countDataNew();
        // $data['total_verify'] = $this->mdash->countDataVerify();
        // $data['total_accept'] = $this->mdash->countDataAccept();
        // $data['content'] = 'content';
        // $this->load->view('template', $data);
   }

    function new_document() {
        $data['status'] = $this->getStatus();
        $data['results'] = $this->mdash->getDataNew();
        $data['content'] = 'document/v_new_document';
        $this->load->view('template', $data);
    }
	
	function verify_start(){
		$data['status'] = $this->getStatus();
        $data['results'] = $this->mdash->getDataNew();
        $data['content'] = 'document/v_new_document';
        $this->load->view('template', $data);
	}
	
	function verify_kasi(){
		$data['status'] = $this->getStatus();
        $data['results'] = $this->mdash->getDataNew();
        $data['content'] = 'document/v_new_document';
        $this->load->view('template', $data);
	}
	
	function verify_kabid(){
		$data['status'] = $this->getStatus();
        $data['results'] = $this->mdash->getDataNew();
        $data['content'] = 'document/v_new_document';
        $this->load->view('template', $data);
	}
	
	function kasda(){
		$data['status'] = $this->getStatus();
        $data['results'] = $this->mdash->getDataNew();
        $data['content'] = 'document/v_new_document';
        $this->load->view('template', $data);
	}

    function saveNewDocument(){
        $this->mdash->saveNewDocument();
        header('location:' . base_url() . 'dashboard/new_document');
    }

    function verify_document() {
        $data['status'] = $this->getStatus();
        $data['results'] = $this->mdash->getDataVerify();
        $data['content'] = 'document/v_verify_document';
        $this->load->view('template', $data);
    }

    function saveVerifyDocument(){
        $this->mdash->saveVerifyDocument();
        header('location:' . base_url() . 'dashboard/verify_document');
    }

    function accept_document() {
        $data['status'] = $this->getStatus();
        $data['results'] = $this->mdash->getDataAccept();
        $data['content'] = 'document/v_accept_document';
        $this->load->view('template', $data);
    }

    function saveAcceptDocument(){
        $this->mdash->saveAcceptDocument();
        header('location:' . base_url() . 'dashboard/accept_document');
    }

    function getStatus(){
        $result = array(
            '1' => 'New',
            '2' => 'Verified',
            '3' => 'Accepted',
        );
        return $result;
    }

    public function ajax_list()
    {
        $dep = $this->mdoc->getDepartment();
        $status = $this->input->get('stat');
        
        $list = $this->mdoc->get_datatables($status);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $mdoc) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $mdoc->document_number;
            $row[] = $mdoc->posting_date;
            $row[] = $mdoc->department ? $this->mdoc->getDepName($mdoc->department) : '';
            $row[] = $mdoc->status ? $this->mdoc->getStatusName($mdoc->status) : '';
            // $row[] = $mdoc->status;
            $row[] = $mdoc->description;
 
            //add html for action
            $row[] = $this->html_button($status,$mdoc->status,$mdoc->document_number);
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->mdoc->count_all($status),
                        "recordsFiltered" => $this->mdoc->count_filtered($status),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

}
