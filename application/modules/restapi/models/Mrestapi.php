<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Mrestapi extends CI_Model {
 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_dokument_by_user($user){
        return $this->db->query('SELECT c.id as user_id,c.`first_name`,e.`description`, a.*,d.dep_name FROM ms_verify a JOIN ms_maping_status b ON a.`status`=b.`id_status` JOIN users c ON b.`id_job`=c.`id_job` LEFT JOIN ms_department d ON d.`dep_code`=a.`department` LEFT JOIN ms_status e ON a.`status`=e.`id` WHERE c.id=?',$user);
    }

    public function get_dokumen_by_id($id){
        return $this->db->query('SELECT c.id as user_id,c.`first_name`,e.`description`, a.*,d.dep_name FROM ms_verify a JOIN ms_maping_status b ON a.`status`=b.`id_status` JOIN users c ON b.`id_job`=c.`id_job` LEFT JOIN ms_department d ON d.`dep_code`=a.`department` LEFT JOIN ms_status e ON a.`status`=e.`id` WHERE a.id=?',$id);
    }

    public function get_dokumen_by_all(){
        return $this->db->query('SELECT c.id as user_id,c.`first_name`,e.`description`, a.*,d.dep_name FROM ms_verify a JOIN ms_maping_status b ON a.`status`=b.`id_status` JOIN users c ON b.`id_job`=c.`id_job` LEFT JOIN ms_department d ON d.`dep_code`=a.`department` LEFT JOIN ms_status e ON a.`status`=e.`id`')->result();
    }

    public function edit($id,$status,$remark){
        // if($approve == 1){
        //     $changestatus = [
        //                         'status' => $status,
        //                         'status_approve' => 1,
        //                         'description' => $remark
        //                     ];
        // }else{
        date_default_timezone_set('Asia/Jakarta');
        $changestatus = [
                            'status' => $status,
                            'description' => $remark,
                            'date_modified' => date('Y-m-d H:i:s')
                        ];
        // }
        return $this->db->where('id',$id)
                           ->update('ms_verify',$changestatus);
    }

    public function delete($id){
        return $this->db->delete('ms_verify',
                            ['id' => $id]
                        );
    }

    public function department($id){
        return $this->db->get_where('ms_department',['dep_code' => $id])->result_array();
    }

    public function status_doc($id){
        return $this->db->get_where('ms_status',['id' => $id])->result_array();   
    }

    public function get_count_per_dokument(){
        return $this->db->query('SELECT ms_status.*, count(ms_verify.status) as count FROM ms_verify JOIN ms_status WHERE ms_verify.status = ms_status.id group BY ms_verify.status')->result_array();
    }
    


      // public function get_dokument_by_role($role){
    //     return $this->db->query('SELECT c.`first_name`, a.*,d.dep_name FROM ms_verify a JOIN ms_maping_status b ON a.`status`=b.`id_status` JOIN users c ON b.`id_job`=c.`id_job` LEFT JOIN ms_department d ON d.`dep_code`=a.`department` WHERE a.status = ?',$role);
    // }


    // public function get_dokument_by_role_status($status=null,$role=null){
    //     if($status && $role){
    //        return $query = $this->db->query('SELECT c.`first_name`, a.* FROM ms_verify a JOIN ms_maping_status b ON a.`status`=b.`id_status` JOIN users c ON b.`id_job`=c.`id_job` WHERE  c.id_job=? AND a.status=?',array($role,$status))->result_array();    
    //     }else{
    //         if($status){
    //         return $query = $this->db->query('SELECT c.`first_name`, a.* FROM ms_verify a JOIN ms_maping_status b ON a.`status`=b.`id_status` JOIN users c ON b.`id_job`=c.`id_job` WHERE  a.status=?',$status)->result_array();    
    //         }
    //         if($role){
    //          return $query = $this->db->query('SELECT c.`first_name`, a.* FROM ms_verify a JOIN ms_maping_status b ON a.`status`=b.`id_status` JOIN users c ON b.`id_job`=c.`id_job` WHERE  a.id_job=?',$role)->result_array();    
    //         }
    //     }
        
    // }

}