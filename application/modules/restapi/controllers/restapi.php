<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class restapi extends CI_controller {
 
    public function __construct()
    {
        parent::__construct();
        $this->load->model('document/Mdocument','mdoc');
        $this->load->model('restapi/Mrestapi','api');
        
    }
 

    // get dokumen semua data
    public function get_dokument_all(){
            $list       = $this->api->get_dokumen_by_all();    
            
            $row    = array();
            $data1  = array();
            foreach ($list as $dt) {
                $row['id_user']  = $dt->user_id;
                $row['id_doc']  = $dt->id;
                $row['document_number'] = $dt->document_number;
                $row['department']  = $this->api->department($dt->department);
                $row['posting_date']  = $dt->posting_date;
                $row['status' ]  = $this->api->status_doc($dt->status);
                $row['date_added']  = $dt->date_added;
                $row['date_modified']  = $dt->date_modified;
                $row['status_approve']  = $dt->status_approve;
                $data1[] = $row;
            }
            if(empty($data1)){
                echo json_encode("Tidak ada document.");
            }else{
                echo json_encode($data1);
            }
        
        
    }

     // get dokumen per role
    public function dokumen_by_role(){
        if(isset($_GET['role'])){
            $role           = $_GET['role'];
            $list           = $this->api->get_dokument_by_user($role)->result();    
            $row    = array();
            $data1  = array();
             foreach ($list as $dt) {
                $row['id_user']  = $dt->user_id;
                $row['id_doc']  = $dt->id;
                $row['document_number'] = $dt->document_number;
                $row['department']  = $this->api->department($dt->department);
                $row['posting_date']  = $dt->posting_date;
                $row['status' ]  = $this->api->status_doc($dt->status);
                $row['date_added']  = $dt->date_added;
                $row['date_modified']  = $dt->date_modified;
                $row['status_approve']  = $dt->status_approve;
                $data1[] = $row;
            }
            if(empty($data1)){
                echo json_encode("Tidak ada document.");
            }else{
                echo json_encode($data1);
            }
            
        }

        // if(isset($_GET['status'])){
        //     $status         = $_GET['status'];
        //     $list           = $this->api->get_dokument_by_status($status);    
        //     $row    = array();
        //     $data1  = array();
        //      foreach ($list as $dt) {
        //         $row['id']  = $dt->id;
        //         $row['document_number'] = $dt->document_number;
        //         $row['department']  = $this->api->department($dt->department);
        //         $row['posting_date']  = $dt->posting_date;
        //         $row['status' ]  = $this->api->status_doc($dt->status);
        //         $row['date_added']  = $dt->date_added;
        //         $row['date_modified']  = $dt->date_modified;
        //         $row['status_approve']  = $dt->status_approve;
        //         $data1[] = $row;
        //     }
        //     if(empty($data1)){
        //         echo json_encode("Tidak ada document.");
        //     }else{
        //         echo json_encode($data1);
        //     }
        // }

        
    }

    //get data dokument per id
    public function get_dokument_by_id(){
        if(isset($_GET['iddoc'])){
            $list   = $this->api->get_dokumen_by_id($_GET['iddoc'])->result();
            
            $data1  = array();
            $row    = array();
             foreach ($list as $dt) {
                $row['id_user']  = $dt->user_id;
                $row['id_doc']  = $dt->id;
                $row['document_number'] = $dt->document_number;
                $row['department']  = $this->api->department($dt->department);
                $row['posting_date']  = $dt->posting_date;
                $row['status' ]  = $this->api->status_doc($dt->status);
                $row['date_added']  = $dt->date_added;
                $row['date_modified']  = $dt->date_modified;
                $row['status_approve']  = $dt->status_approve;
                $data1 = $row;
            }
            

            if(empty($data1)){
                echo json_encode("Tidak ada document.");
            }else{
                 echo json_encode($data1);
            }
        }
        
    }

    //get update status approve dokumen
    public function dokumen_update_approve(){
        $id     = $_GET['id'];
        $status = $_GET['status'];
        if($status == 9 || $status == 10 || $status == 11 || $status == 12){
            $ketstatus = $this->api->status_doc($status);
            $remark = $ketstatus[0]['description'];
        }else{
            if(!isset($_GET['remark'])){
                $remark = '';
            }else{
                $remark = $_GET['remark'];
            }
            
        }
        

        if($this->api->edit($id,$status,$remark) == TRUE){
            echo json_encode("Data berhasil di update.");    
        }else{
            echo json_encode("Data gagal di update.");
        }
    }

    //get update status approve dokumen
    public function get_count_per_dokument(){
        $list = $this->api->get_count_per_dokument();
        echo json_encode($list);
    }
 
}