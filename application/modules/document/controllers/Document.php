<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Document extends CI_Controller {
 
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mdocument','mdoc');
    }
 
    public function index()
    {
        $this->template->load('maintemplate','document/views/v_new_document', 
            [
                'job_id'     => $this->ion_auth->get_id_job(),
                'themes_url' => $this->config->item('theme_url'),
                'department' => $this->mdoc->getDepartment()
            ]
        );
    }
	
	public function html_button($status,$value,$doc_number){
        if($status == 1 && strlen($status)==1){ //staf penerimaan
			$button = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="accept_mdoc('."'".$doc_number."'".','."'".$value."'".')"><i class="fa fa-pencil"></i> Ajukan</a>';
		} else {
			// if($value == substr($status,-1)){
			// 	if($value == '2'){
   //                  $button = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="accept_mdoc('."'".$doc_number."'".','."'".$value."'".')"><i class="fa fa-pencil"></i> Setuju</a>
   //                <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="reject_mdoc('."'".$doc_number."'".')"><i class="fa fa-recycle"></i> Tolak</a>';
   //              } else {
   //                  $button = '-';
   //              }
			// } else {
				$button = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="accept_mdoc('."'".$doc_number."'".','."'".$value."'".')"><i class="fa fa-pencil"></i> Setuju</a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="reject_mdoc('."'".$doc_number."'".')"><i class="fa fa-recycle"></i> Tolak</a>';
			// }
		}
        if($status == 3){ //kasubid
                $button = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Verifikasi Kabid" onclick="accept_mdoc('."'".$doc_number."'".','."'".$value."'".')"><i class="fa fa-pencil"></i> Verifikasi Kabid</a>
                    <a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Cetak SP2D" onclick="accept_mdoc('."'".$doc_number."'".','."'".$value."'".')"><i class="fa fa-print"></i> Cetak SP2D</a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Reject" onclick="reject_mdoc('."'".$doc_number."'".')"><i class="fa fa-recycle"></i> Tolak</a>';
        }
		
		return $button;
	}
    public function list($job_id){
        $list       = $this->mdoc->get_table($job_id);
        echo json_encode($list);
    }
    public function ajax_list()
    {
        $user_id    = $this->ion_auth->get_user_id();
        $job_id     = $this->ion_auth->get_id_job();
        $list       = $this->mdoc->get_datatables($job_id);
        $data       = array();
        $no         = $_POST['start'];
        foreach ($list as $mdoc) {
            $no++;
            $row = array();
			$row[] = $no;
            $row[] = $mdoc->document_number;
            $row[] = $mdoc->posting_date;
            $row[] = $mdoc->department ? $this->mdoc->getDepName($mdoc->department) : '';
            $row[] = $mdoc->status ? $this->mdoc->getStatusName($mdoc->status) : '';
            // $row[] = $mdoc->status;
            $row[] = $mdoc->description;
            //add html for action
            $row[] = $this->html_button($job_id,$mdoc->status,$mdoc->document_number);
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->mdoc->count_all($job_id),
                        "recordsFiltered" => $this->mdoc->count_filtered($job_id),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
 
    public function ajax_edit($id)
    {
        $data = $this->mdoc->get_by_id($id);
        echo json_encode($data);
    }
 
    public function ajax_add()
    {
        $data = array(
                'document_number' => $this->input->post('document_number'),
                'posting_date' => date('Y-m-d'),
                'status' => '1',
                'department' => $this->input->post('department'),
                'date_added' => date('Y-m-d H:i:s'),
				'date_modified' => date('Y-m-d H:i:s'),
                'description' => $this->input->post('keterangan')
            );


        $insert = $this->mdoc->save($data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_update()
    {
        $document_number = $this->input->post('document_number');
        $cek = $this->mdoc->getDataByDocument($document_number);

        if($cek->status <> '1'){
            $status = '1';
        } else {
            $status = $cek->status + 1;
        }

        $data = array(
                // 'posting_date' => $this->input->post('posting_date'),
                'status' => $status,
                'description' => $this->input->post('description'),
				'date_modified' => date('Y-m-d H:i:s'),
            );
        $this->mdoc->update(array('document_number' => $document_number), $data);
        echo json_encode(array("status" => TRUE));
    }
	
	
	// Accept verify document 
	public function ajax_accept(){
		$data = array(
			'status' => $this->input->post('status')+1,
			'date_modified' => date('Y-m-d H:i:s')
		);
		
		$this->mdoc->update(array('document_number' => $this->input->post('document_number')), $data);
        echo json_encode(array("status" => TRUE));
	}
	
	public function ajax_reject(){
		
	}
 
    public function ajax_delete($id)
    {
        $this->mdoc->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
	
	public function verify_start()
    {
		$data['department'] = $this->mdoc->getDepartment();
		$data['content'] = 'document/v_verify_start';
        $this->load->view('template', $data);
    }
	
	public function verify_kasubid()
    {
		$data['department'] = $this->mdoc->getDepartment();
		$data['content'] = 'document/v_verify_kasubid';
        $this->load->view('template', $data);
    }
	
	public function verify_kabid()
    {
		$data['department'] = $this->mdoc->getDepartment();
		$data['content'] = 'document/v_verify_kabid';
        $this->load->view('template', $data);
    }
	
	public function kasda()
    {
		$data['department'] = $this->mdoc->getDepartment();
		$data['content'] = 'document/v_kasda';
        $this->load->view('template', $data);
    }

    public function bank()
    {
        $data['department'] = $this->mdoc->getDepartment();
        $data['content'] = 'document/v_bank';
        $this->load->view('template', $data);
    }

    public function upload_data(){
        $data['content'] = 'document/v_upload';
        $this->load->view('template', $data);
    }
 
}