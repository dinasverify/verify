<script type="text/javascript">
    $(document).ready(function () {
        $('#btn_save').click(function () {
            var data = {
                input_document_number: $('#input_document_number').val()
            };

            $.post('<?php echo base_url() ?>dashboard/saveAcceptDocument', data, function () {
                window.location.href = '<?php echo base_url() ?>dashboard/accept_document';
            });
        });
    });
</script>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Document</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <!-- Tab panes -->
                <div class="tab-content">
                    <br>
                    <div class="row">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label for="input_document_number">Document Number</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control input-sm" id="input_document_number" name="input_document_number" value="">
                                    </div>
                                    <div class="col-lg-3">
                                        <button class="btn btn-info" id="btn_save" style="width: 50px"><i class="fa fa-save"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Document Number</th>
                                <th>Date Added</th>
                                <th>Date Modified</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($results as $row){?>
                                <tr>
                                    <td><?php echo $row['document_number']?></td>
                                    <td><?php echo $row['date_added']?></td>
                                    <td><?php echo $row['date_modified']?></td>
                                    <td><?php echo $status[$row['status']]?></td>
                                </tr>
                            <?php }?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->