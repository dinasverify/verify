<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		<?php echo lang('deactivate_heading');?>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url();?>"><i class="fa fa-desktop"></i> Dashboard</a></li>
		<li><a href="<?php echo base_url();?>menu">Menu</a></li>
		<li class="active">Status User</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-gear"></i> Status User
					</h3>
					<span class="pull-right">
						<i class="fa fa-fw ti-angle-up clickable"></i>
						<i class="fa fa-fw ti-close removepanel clickable"></i>
					</span>
				</div>
				
				<div class="panel-body">
					<div class="col-xs-12">
						<p><?php echo sprintf(lang('deactivate_subheading'), $user->username);?> ?</p>
						<?php echo form_open("auth/deactivate/".$user->id);?>
							<div class="form-group">
								<p>
									<?php echo lang('deactivate_confirm_y_label', 'confirm');?>
									<input type="radio" name="confirm" value="yes" checked="checked" /></p>
							</div>
							<div class="form-group">
								<p><?php echo lang('deactivate_confirm_n_label', 'confirm');?>
									<input type="radio" name="confirm" value="no" /></p>
							</div>
							<?php echo form_hidden($csrf); ?>
							<?php echo form_hidden(array('id'=>$user->id)); ?>
							<div class="row">
								<hr>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<button class="btn btn-primary" type="submit" id="btn_submit" name="btn_submit" value="Save">Submit</button>
								</div>
							</div>
						<?php echo form_close();?>
					</div><!-- /.col -->
				</div><!-- /.panel-body -->
			</div><!-- /.panel -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</section><!-- /.content -->
