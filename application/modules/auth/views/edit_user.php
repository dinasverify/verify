<script>
	$(document).ready(function() {
		$("#group_id").on('change', function() {
			$("#btn_select_group").click();
		});
		
		$('input').iCheck({
		  checkboxClass: 'icheckbox_square-blue',
		  radioClass: 'iradio_square-blue',
		  increaseArea: '20%' // optional
		});
	})
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		<?php echo lang('edit_user_heading');?>
		<small><?php echo lang('edit_user_subheading');?></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url();?>"><i class="fa fa-desktop"></i> Dashboard</a></li>
		<li><a href="<?php echo base_url();?>menu">Menu</a></li>
		<li class="active">Edit User</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-pencil"></i> Edit User
					</h3>
					<span class="pull-right">
						<i class="fa fa-fw ti-angle-up clickable"></i>
						<i class="fa fa-fw ti-close removepanel clickable"></i>
					</span>
				</div>
				
				<?php if (!empty($message)): ?> 
				<div class="row">
					<div class="col-xs-12">
						<div id="notifications">
							<div class="alert alert-warning fade in">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								<?php echo $message;?>
							</div>
						</div>
					</div>
				</div>
				<?php endif; ?>
				
				<div class="panel-body">
					<div class="col-xs-12">
						<?php echo form_open(uri_string(),array('class'=>'form-horizontal')); ?>
							<div class="form-group">
								<label class="col-md-3 control-label" for="first_name">
									<?php echo lang('edit_user_fname_label', 'first_name'); ?>
								</label>
								<div class="col-md-8">
									<p><?php echo form_input($first_name,$first_name,array('class'=>'form-control', 'id'=>'first_name', 'name'=>'first_name', 'type'=>'text'));?></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="last_name">
									<?php echo lang('edit_user_lname_label', 'last_name'); ?>
								</label>
								<div class="col-md-8">
									<p><?php echo form_input($last_name,$last_name,array('class'=>'form-control', 'id'=>'last_name', 'name'=>'last_name', 'type'=>'text'));?></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="id_perusahaan">
									<?php echo lang('create_user_comp_id_label', 'id_perusahaan');?>
									<span class="text-danger">*</span>
								</label>
								<div class="col-md-8">
									<select id="id_perusahaan" name="id_perusahaan" class="form-control">
										<option value="0">Please select</option>
										<?php foreach ($data_perusahaan as $perusahaan) { ?>
											<option <?php if($idperusahaan == $perusahaan['id_perusahaan']){echo 'Selected';}?> value="<?php echo $perusahaan['id_perusahaan'];?>"><?php echo $perusahaan['nama_perusahaan'];?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="id_candidate">
									<?php echo lang('create_user_candidate_id_label', 'id_candidate');?>
									<span class="text-danger">*</span>
								</label>
								<div class="col-md-8">
									<select id="id_candidate" name="id_candidate" class="form-control">
										<option value="0">Please select</option>
										<?php foreach ($data_kandidat as $kandidat) { ?>
											<option <?php if($idkandidat == $kandidat['id_candidate']){echo 'Selected';} ?> value="<?php echo $kandidat['id_candidate'];?>"><?php echo $kandidat['nama_lengkap'];?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="id_employee">
									<?php echo lang('create_user_emp_id_label', 'id_employee');?>
									<span class="text-danger">*</span>
								</label>
								<div class="col-md-8">
									<select id="id_employee" name="id_employee" class="form-control">
										<option value="0">Please select</option>
										<?php foreach ($data_pegawai as $pegawai) { ?>
											<option <?php if($idemployee == $pegawai['id_employee']){echo 'Selected';} ?> value="<?php echo $pegawai['id_employee'];?>"><?php echo $pegawai['nama_karyawan'];?></option>";
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="company">
									<?php echo lang('edit_user_company_label', 'company'); ?>
								</label>
								<div class="col-md-8">
									<p><?php echo form_input($company,$company,array('class'=>'form-control', 'id'=>'company', 'name'=>'company', 'type'=>'text'));?></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="phone">
									<?php echo lang('edit_user_phone_label', 'phone'); ?>
								</label>
								<div class="col-md-8">
									<p><?php echo form_input($phone,$phone,array('class'=>'form-control', 'id'=>'phone', 'name'=>'phone', 'type'=>'text'));?></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="password">
									<?php echo lang('edit_user_password_label', 'password'); ?>
								</label>
								<div class="col-md-8">
									<p><?php echo form_input($password,'',array('class'=>'form-control', 'id'=>'password', 'name'=>'password', 'type'=>'text'));?></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="group_name">
									<?php echo lang('edit_user_password_confirm_label', 'password_confirm'); ?>
								</label>
								<div class="col-md-8">
									<p><?php echo form_input($password_confirm,'',array('class'=>'form-control', 'id'=>'password_confirm', 'name'=>'password_confirm', 'type'=>'text'));?></p>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-8 col-md-offset-3">
									<?php if ($this->ion_auth->is_admin()): ?>
										<h3><?php echo lang('edit_user_groups_heading'); ?></h3>
										<?php foreach ($groups as $group): ?>
											<label class="checkbox">
												<?php
												$gID = $group['id'];
												$checked = null;
												$item = null;
												foreach ($currentGroups as $grp) {
													if ($gID == $grp->id) {
														$checked = ' checked="checked"';
														break;
													}
												}
												?>
												<input type="radio" name="groups[]" value="<?php echo $group['id']; ?>"<?php echo $checked; ?>>
												<?php echo htmlspecialchars($group['name'], ENT_QUOTES, 'UTF-8'); ?>
											</label>
										<?php endforeach ?>

									<?php endif ?>
								</div>
							</div>
							<?php echo form_hidden('id', $user->id); ?>
							<?php echo form_hidden($csrf); ?>
							<div class="row">
								<hr>
							</div>
							<div class="form-group">
								<div class="col-md-8 col-md-offset-3">
									<button class="btn btn-primary" type="submit" id="btn_submit" name="btn_submit" value="Save">Edit User</button>
								</div>
							</div>
						<?php echo form_close();?>
					</div><!-- /.col -->
				</div><!-- /.panel-body -->
			</div><!-- /.panel -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</section><!-- /.content -->

<script> 
    $('#notifications').slideDown('slow').delay(3500).slideUp('slow');
</script>