<!DOCTYPE html>
<html>
<?php $session_data = $this->session->userdata('logged_in'); 
	$nama_perusahaan = ""; $logo_perusahaan = ""; $ico_perusahaan = "";
	if (sizeof($site_corp)>0) {
		$nama_perusahaan = $site_corp[0]["nama_perusahaan"];
		$logo_perusahaan = $site_corp[0]["logo"];
		$ico_perusahaan = $site_corp[0]["icon"];
} ?>
<head>
<style>
    .cardtengah{
        box-shadow: 0 30px 30px 20px rgba(0,0,0,0.2);
        transition: 0.3s;
        border-radius: 15px;
        width: 60%;
        height: 60%;
       
        background: white;
       
    }
    
    .titles{
         margin-top: 5%;
         margin-bottom: 5%;
       
    }
    
    h3{
        letter-spacing: 5px;
        color:  #120f66 !important;
    }
    h6{
        letter-spacing: 5px;
         color:  grey !important;
    }
    
    body{
        background-image: url("assets/loginasset/bg.png");
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center bottom; 
        background-color: #aaa;
        background-size: 101% 65%;
          zoom: 0.8; 
    }
    .bgleft{
        background-image: url("assets/loginasset/bg2.png");
        background-position:  center bottom;
        background-repeat: no-repeat;
        background-color: #120f66;
        height: 100%;
         border-top-left-radius: 15px;
         border-bottom-left-radius: 15px;
        background-size: cover;
    }
    .fts{
        font-size: 12px;
        font-weight: 100;
        color: white;
    }
    
    .ft-er{
        margin-top:30px;
        margin-top:30px;
    }
    
    .man{
        margin-top: 10%
    }
    
    .awan{
        position: absolute;
        margin-left: -45%;
        margin-top: 20%;
    }
    
    .awan2{
        position: absolute;
        margin-left: 20%;
        margin-top: 34%;
    }
    .bintang{
        position: absolute;
        margin-left: 0%;
        margin-top: 10%;
    }
    .dalemright{
        margin: 40px
    }
    
    .btn-custom{
        background: #120f66;
        color: white;
        width: 100%;
        margin-top: 30px;
        border-radius: 90px !important;
    }
    
    .titleheader{
        color: purple;
        margin-top: 0px;
        margin-bottom: 20px
    }
</style>

		<meta charset="UTF-8">
		<title>Human Resource</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<link rel="stylesheet" href="<?php echo ($folder_themes.'adminto/plugins/morris/morris.css');?>">
        <link href="<?php echo ($folder_themes.'adminto/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo ($folder_themes.'adminto/css/responsive.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo ($folder_themes.'adminto/plugins/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo ($folder_themes.'adminto/plugins/iCheck/square/blue.css');?>" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" type="image/png" href="<?php echo base_url('assets/'.$ico_perusahaan);?>"/>
	<meta name="csrf-name" content="ci_csrf_token">
<meta name="csrf-token" content="">
</head>

	<body>
        <center>
            <div class="titles">
                <h3>WELCOME TO PMIS</h3>
                <h6>PROJECT MANAGEMENT INFORMATION SYSTEM</h6>
            </div>
            
            <div class="row cardtengah">



                <div class="col-lg-6 col-md-6 col-xs-6 bgleft">
                    <img src="assets/loginasset/cloud.png" class="awan" />
                    <img src="assets/loginasset/man.png" height="80%" class="man" />
                    <img src="assets/loginasset/star.png" class="bintang" />
                    <img src="assets/loginasset/cloud2.png" class="awan2" />
                </div>




                <div class="col-lg-6 col-md-6 col-xs-12" >
                    <div class="dalemright" >
                        <center>
                            <img src="http://pmis.swamedia.co.id/assets/img/pmisnew.png" width="210px" height="160px" align="middle"	 class="img-responsive">
                         <div class="titleheader">
                            <span>Please, sign in to PMIS</span>
                         </div>
                        </center>

                            <div class="alert alert-danger alert-dismissable" style="display:none">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                                <span class="content"></span>
                            </div>
						
                            <?php echo form_open('auth/login',array('method' => 'post', 'id' => 'authenticationLogin', 'class' => 'login_validator'));?>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <input type="text" class="form-control" placeholder="Email" name="identity" id="identity" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <input type="password" class="form-control" placeholder="Password"  name="password" id="password"/>
                                </div>
                            </div>
                            <div class="form-group">
								<label for="terms">
									<?php /*<input type="checkbox" name="terms" id="terms">&nbsp; Calon Pegawai</br>*/ ?>
									<?php echo form_checkbox('remember', '1', FALSE, 'id="remember"', 'type="checkbox"');?> Remember Me
								</label>
							</div>   
                            <div class="form-group text-center m-t-30">
                                <div class="col-xs-12">
                                    <button type="submit" class="btn btn-custom btn-block btn-lg" name="btn_login" id="btn_login" value="submit">Login</button>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
						<div class="col-md-6 pull-left">
							<p><a href="<?php echo site_url('auth/forgot_password');?>" id="forgot" class="forgot"> Lupa Password ?</a></p>
						</div>
						<div class="col-md-6 pull-right">
							<p><a href="<?php echo site_url('auth/registrasi'); ?>" id="forgot" class="forgot">Daftar Calon Pegawai</a></p>
						</div>
                    </div>
                </div>
            </div>
            
            <div class="ft-er">
             <span class="fts">Copyright &copy; 2018 Swamedia Informatika | All rights reserved.</span>
            </div>
        </center>
	</body>


<script src="<?php echo ($folder_themes.'adminto/js/modernizr.min.js');?>"></script>
<script src="<?php echo ($folder_themes.'adminto/js/jquery.min.js');?>"></script>
<script src="<?php echo ($folder_themes.'adminto/js/bootstrap.min.js');?>"></script>
<script src="<?php echo ($folder_themes.'adminto/plugins/iCheck/icheck.min.js');?>" type="text/javascript"></script>
<script src="<?php echo ($folder_themes.'adminto/plugins/cookie/jquery.cookie.js');?>" type="text/javascript"></script>


</html>

