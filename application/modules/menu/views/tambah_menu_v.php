<!--page level css -->
<link rel="stylesheet" href="<?php echo $themes_url; ?>css/passtrength/passtrength.css">
<link href="<?php echo $themes_url; ?>vendors/bootstrapvalidator/css/bootstrapValidator.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/sweetalert2/css/sweetalert2.min.css"/>
<link rel="stylesheet" href="<?php echo $themes_url; ?>css/custom_css/form2.css"/>
<link rel="stylesheet" href="<?php echo $themes_url; ?>css/custom_css/form3.css"/>

<!--prettycheckable -->
<link href="<?php echo $themes_url; ?>vendors/prettycheckable/css/prettyCheckable.css" rel="stylesheet" type="text/css"/>
<!-- labelauty -->
<link href="<?php echo $themes_url; ?>vendors/jquerylabel/css/jquery-labelauty.css" rel="stylesheet" type="text/css"/>

	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Learning Management System</h1>
		<ol class="breadcrumb">
			<li>
				<a href="<?php echo site_url($themes_url,'dashboard');?>">
					<i class="fa fa-fw ti-home"></i> Dashboard
				</a>
			</li>
			<li> <a href="<?php echo site_url('menu');?>">Menu</a></li>
			<li> Tambah MEnu</li>
		</ol>
	</section>
	<section class="content">
		<!--main content-->
		<div class="row">
			<div class="col-md-12">
				<div class="panel ">
					<div class="panel-heading">
						<h3 class="panel-title">
							<i class="fa fa-fw ti-sitemap"></i> Tambah Menu
						</h3>
						<span class="pull-right">
							<i class="fa fa-fw ti-angle-up clickable"></i>
							<i class="fa fa-fw ti-close removepanel clickable"></i>
						</span>
					</div>
					
					<div class="panel-body">
						<div class="col-md-12">
							<?php if($pesan != "") { ?>
								<div id="notifications">
									<div class="alert alert-success fade in">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										<?php echo $pesan; ?>
									</div>
								</div>
							<?php } ?>
							<form id="form-validation-menu" action="" method="POST" class="form-horizontal">
								<div class="form-group">
									<label class="col-md-3 control-label" for="val-parent">
										Parent Menu
										<span class="text-danger">*</span>
									</label>
									<div class="col-md-8">
										<select class="form-control" id="parent" name="parent" style="width:100%;">
											<option value="0">Pilih Parent</option>
											<?php foreach($data_parent as $parent) {
												echo "<option value='".$parent['idmenu']."'>".$parent['nama_menu']."</option>";
											} ?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="nama_menu	">
										Nama Menu
										<span class="text-danger">*</span>
									</label>
									<div class="col-md-8">
										<input type="text" id="nama_menu" name="nama_menu" class="form-control"
											placeholder="Enter your nama menu">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="url	">
										Url
										<span class="text-danger">*</span>
									</label>
									<div class="col-md-8">
										<input type="text" id="url" name="url" class="form-control"
											placeholder="Enter your url">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="status">
										Status
										<span class="text-danger">*</span>
									</label>
									<div class="col-md-8">
										<label>
											<input type="radio" name="status" value="1" class="square-blue"
												   checked>
										</label>
										<label class="m-l-10">
											Active
										</label>
										<label>
											<input type="radio" name="status" value="0" class="square-blue">
										</label>
										<label class="m-l-10">
											Not Active
										</label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="icon">
										Icon
										<span class="text-danger">*</span>
									</label>
									<div class="col-md-8">
										<input type="text" id="icon" name="icon" class="form-control"
											placeholder="fa fa-contoh (contoh : user)">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="sequence">
										Urutan Menu
										<span class="text-danger">*</span>
									</label>
									<div class="col-md-8">
										<select class="form-control" id="sequence" name="sequence" style="width:100%;">
											<option value="0">Pilih Urutan</option>
											<?php for($i=1;$i<=10;$i++){
												echo "<option value='".$i."'>".$i."</option>";
											} ?>
										</select>
									</div>
								</div>
								<div class="row"><hr></div>
								<div class="form-group form-actions">
									<div class="pull-right">
										<div class="col-md-12">
											<button type="submit" name="simpan" value="simpan" class="btn btn-effect-ripple btn-primary">Simpan</button>
											<button type="reset" class="btn btn-effect-ripple btn-default reset_btn">Reset</button>
										</div>
									</div>
								</div>
							</form>
						</div>

					</div>
				</div>
				
			</div>
			<!-- col-md-12 -->
		</div>
	</section>

<script>   
    $('#notifications').slideDown('slow').delay(3500).slideUp('slow');
</script>

<script>
$(document).ready(function () {
	$('#form-validation-menu').bootstrapValidator({
		fields: {
			parent: {
				validators: {
					notEmpty: {
						message: 'The parent is required and cannot be empty'
					}
				}
			},
			nama_menu: {
				validators: {
					notEmpty: {
						message: 'The nama_menu is required and cannot be empty'
					}
				}
			},
			url: {
				validators: {

					notEmpty: {
						message: 'The url is required and cannot be empty'
					}
				}
			},
			icon: {
				validators: {
					notEmpty: {
						message: 'The icon is required and cannot be empty'
					}
				}
			},
			sequence: {
				validators: {
					notEmpty: {
						message: 'The secuence is required and cannot be empty'
					}
				}
			},
		},
	}).on('reset', function (event) {
		$('#form-validation-menu').data('bootstrapValidator').resetForm();
	});
	
	$('#terms').on('ifChanged', function (event) {
		$('#form-validation-menu').bootstrapValidator('revalidateField', $('#terms'));
	});
	$('.reset_btn').on('click', function () {
		var icheckbox = $('.custom_icheck');
		var radiobox = $('.custom_radio');
		icheckbox.prop('defaultChecked') == false ? icheckbox.iCheck('uncheck') : icheckbox.iCheck('check').iCheck('update');
		radiobox.prop('defaultChecked') == false ? radiobox.iCheck('uncheck') : radiobox.iCheck('check').iCheck('update');
	});
});
</script>

<!-- begining of page level js -->
<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/bootstrapvalidator/js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/bootstrap-maxlength/js/bootstrap-maxlength.js"></script>
<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/sweetalert2/js/sweetalert2.min.js"></script>
<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/card/jquery.card.js"></script>
<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/iCheck/js/icheck.js"></script>
<script src="<?php echo $themes_url; ?>js/passtrength/passtrength.js"></script>
<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/form2.js"></script>
<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/form3.js"></script>
<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/form_validations.js"></script>
<!-- end of page level js -->

