<?php

Class Pengaturan extends CI_controller{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Pengaturan_model');
		$this->load->model('personalia/Personalia_model');
		$this->load->model('auth/User_model');
		$this->load->library('form_validation');
	}
	
	public function index() {
		$themes_url 		= 'themes/hr/';
		$data['themes_url'] = base_url($themes_url); 	
		
		
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}
		$this->template->load('maintemplate', 'pengaturan/views/bagian_pekerjaan_v',$data);
	}
	
	public function bagian() 
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url); 	 	
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$data['data_bagian'] 	= $this->Pengaturan_model->get_bagian();
		$data['data_job']		= $this->Pengaturan_model->get_job();

		$this->template->load('maintemplate', 'pengaturan/views/bagian_pekerjaan_v',$data);
	}

	public function adddepartment()
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		if($this->input->post('simpan')){
			$kode 	= $this->input->post('kodedept');
			$dept 	= $this->input->post('namadept');
			$desk 	= $this->input->post('descdept');
			$status	= $this->input->post('status');

			$this->Pengaturan_model->insertdept($kode,$dept,$desk,$status);
			redirect('pengaturan/bagian');
		}
		$this->template->load('maintemplate', 'pengaturan/views/tambah_department_v',$data);
	}

	public function addjob()
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp']		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));

		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		if($this->input->post('simpan')){
			$input = array(
				'kode_job_t' 	=> $this->input->post('kodejob'),
				'nama_job' 	=> $this->input->post('namajob'),
				'description' 	=> $this->input->post('descjob'),
				'status_job'	=> $this->input->post('status')
			);

			$this->Pengaturan_model->insertjob($input);
			redirect('pengaturan/bagian');
		}
		$this->template->load('maintemplate', 'pengaturan/views/tambah_job_v',$data);
	}
	
	public function edit_kalender($id) 
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp']		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		
        $user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		 $data = array(
			'tanggal'		=>	$this->input->post('tgl', TRUE),
			'nama_event'		=>	$this->input->post('nama_event', TRUE),
			'tipe_event'		=>	$this->input->post('tipe', TRUE)
		);
		$this->Pengaturan_model->update_kal($id,$data);
		redirect('pengaturan/kalender');
    }
	
	public function update_dept() 
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$id 	= $this->input->get('id');
		$dept 	= $this->Pengaturan_model->get_bagian($id);
		
		if(sizeof($dept)>0){
			$data['id_departemen'] 	= $id;
			$data['kode_dep'] 	= $dept[0]['kode_dep'];
			$data['nama_dep'] 	= $dept[0]['nama_dep'];
			$data['desc'] 		= $dept[0]['description'];
			$data['status'] 	= $dept[0]['status'];
		}
		if($this->input->post('simpan')){
			$data =array('kode_dep' 	=> $this->input->post('kodedept'),
						 'nama_dep'		=> $this->input->post('namadept'),
						 'description'	=> $this->input->post('descdept'),
						 'status'		=> $this->input->post('status')
						);
			
			$this->Pengaturan_model->update_dept($id,$data);
			redirect('pengaturan/bagian');
		}
		$this->template->load('maintemplate', 'pengaturan/views/edit_department_v',$data);
	}
	

	public function update_job() 
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));

		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$id 	= $this->input->get('id');
		$job 	= $this->Pengaturan_model->get_job($id);
		
		if(sizeof($job)>0){
			$data['id_job_t'] 	= $id;
			$data['kode_job'] 	= $job[0]['kode_job_t'];
			$data['nama_job'] 	= $job[0]['nama_job'];
			$data['desc'] 		= $job[0]['description'];
			$data['status'] 	= $job[0]['status_job'];
		}
		if($this->input->post('simpan')){
			$data =array(
				'kode_job_t' 	=> $this->input->post('kodejob'),
				'nama_job'		=> $this->input->post('namajob'),
				'description'	=> $this->input->post('descjob'),
				'status_job'		=> $this->input->post('status')
			);
			$this->Pengaturan_model->update_job($id,$data);
			redirect('pengaturan/bagian');
		}
		$this->template->load('maintemplate', 'pengaturan/views/edit_job_v',$data);
	}

	public function deletekalender() 
	{
		$id = $_GET['id'];
		$row = $this->Pengaturan_model->delete_kal($id);
		if(count($row)>0) {
    	$pesan = "<div id='notifications' class='alert alert-danger' role='alert'><i class='fa fa-info-circle icon_id'></i> Data berhasil dihapus.</div>";
			$this->session->set_flashdata("pesan",$pesan);
			redirect(base_url('pengaturan/kalender'));
		}
		redirect('pengaturan/kalender');
	}
	
	public function kalender() 
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url']	 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));

		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$data['data_kal']	  = $this->Pengaturan_model->get_kalender();
		
		$this->template->load('maintemplate', 'pengaturan/views/kalender_v',$data);
	}
	
	public function add_kalender() 
	{
        $data = array(
			'tanggal'		=>	date('Y-m-d',strtotime($this->input->post('tgl', TRUE))),
			'nama_event'		=>	$this->input->post('nama_event', TRUE),
    		'tipe_event'		=>	$this->input->post('tipe', TRUE)
    	);
        		
    	$result = $this->Pengaturan_model->add_event($data);
    	if(count($result)>0) {
    	$pesan = "<div id='notifications' class='alert alert-success' role='alert'><i class='fa fa-info-circle icon_id'></i> Data berhasil disimpan</div>";
			$this->session->set_flashdata("pesan",$pesan);
			redirect(base_url('pengaturan/kalender'));
		}
    }

    public function editkalender(){
    	$themes_url 			= 'themes/hr/';
		$data['themes_url']	 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));

		$id 					= $this->input->get('id');
		$data['id']				= $id;
		$kalender  				= $this->Pengaturan_model->get_kalender($id);
		$data['tanggal']		= date('d F Y',strtotime($kalender[0]['tanggal']));
		$data['event']			= $kalender[0]['nama_event'];
		$data['tipe']			= $kalender[0]['tipe_event'];
		if(isset($_POST['update'])){
			$id= $_GET['id'];
			$update = array(
				'tanggal' => date('Y-m-d',strtotime($this->input->post('tanggal'))),
				'nama_event' => $this->input->post('nama_event'),
				'tipe_event' => $this->input->post('tipe')
			);
			$row = $this->Pengaturan_model->update_kal($id,$update);
			if(count($row)>0){
				$pesan = "<div id='notifications' class='alert alert-success' role='alert'><i class='fa fa-info-circle icon_id'></i> Perubahan Data berhasil disimpan. </div>";
				$this->session->set_flashdata("pesan",$pesan);
				redirect(base_url('pengaturan/kalender'));
			}
		}

		$this->template->load('maintemplate', 'pengaturan/views/edit_kalender_v',$data);
    }
	
	public function versikalender() 
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));

		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}
		$this->template->load('maintemplate', 'pengaturan/views/versi_kalender_v',$data);
	}
	
	
	public function penggajian() 
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
        $data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));

        $user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}
        
		$data['grupgaji'] 			= $this->Pengaturan_model->grupgaji();
		$id_grup 					= $this->input->post("idgrup",true);
		
		if(is_null($id_grup)){
			$data['namalevelgrup']		= "Semua Level Jabatan";
		}else{
			$data['namalevelgrup']		= $data['grupgaji'][0]['nama_grup'];
		}
		
		$data["id_grup"] 			= $id_grup; 
		$data['itempendapatan']		= $this->Pengaturan_model->get_itempendapatan($id_grup);
		$get_aturangaji				= $this->Pengaturan_model->get_aturangaji();
		$data['data_set_sal']		= $this->Pengaturan_model->get_tunjangan();

		if(sizeof($get_aturangaji)>0){
			for($i=0;$i<sizeof($get_aturangaji);$i++){
				$id_ref 		= $get_aturangaji[$i]['id_referensi'];
				$nm_ref 		= $get_aturangaji[$i]['nama_ref'];
				$parentcode	 	= $get_aturangaji[$i]['parent_code'];
				$value		 	= $get_aturangaji[$i]['value'];
				$status		 	= $get_aturangaji[$i]['status'];
				
				$st 			= $get_aturangaji[0]['status'];
				if($st == 0){ $data['status']="Tidak";}else{$data['status']="Ya";}
				$data['nominal'] 		= $get_aturangaji[8]['value'];
				$data['mindapetthr']	= $get_aturangaji[2]['value'];
				$data['besaranthr']		= $get_aturangaji[3]['value'];
				$data['thrminkurangthn'] = $get_aturangaji[4]['value'];
				$data['rubahkarir']		 = $get_aturangaji[5]['value'];
			}
		}						
		$this->template->load('maintemplate', 'pengaturan/views/penggajian_v',$data);		
	}
	
	public function tambahitempendapatan()
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$data['item_grup_gaji'] = $this->Pengaturan_model->grupgaji();
		$data['komponen_pend'] = $this->Pengaturan_model->komponen_pend();
		
		if($this->input->post('submit')){
			
			$input = array(
				'id_grup' 	=> $this->input->post('namagrup'),
				'id_kom' 	=> $this->input->post('komponen_pend'),
				'jenis'		=> 35,
				'status'	=> 1
			);
			$this->Pengaturan_model->createitempendapatan($input);
			redirect('pengaturan/penggajian');
		}
		$this->template->load('maintemplate', 'pengaturan/views/tambah_itempendapata_v',$data);		

	}
	
	public function deleteitempenggajian($id)
	{
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$row = $this->Pengaturan_model->deleteitempenggajian($id);
		if(count($row)>0){
		$pesan = "<div id='notifications' class='alert alert-success' role='alert'><i class='fa fa-info-circle icon_id'><span class='close' aria-hidden='true'>&times;</span></button></i>Komponen Tunjangan berhasil dihapus.</div>";
			$this->session->set_flashdata("pesan1",$pesan);
			redirect('pengaturan/penggajian');
		}
		
	}
	
	
	public function tambahpenggajian() 
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));

		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}
		
		$this->template->load('maintemplate', 'pengaturan/views/tambah_penggajian_v',$data);
	}
	
	public function tambahkomponen_pend() 
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));

		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}
		
		if($this->input->post("submit")){
			$input = array(
				'nama_pendapatan' 	=> $this->input->post('nama_pendapatan'),
				'tipe_pendapatan' 	=> $this->input->post('tipependapatan'),
				'is_pph21'		  	=> $this->input->post('kenapph21'),
				'status'			=> $this->input->post('status'),
				'tipe_a1'		 	=> $this->input->post('tipea1'),
				'nilai'			 	=> $this->input->post('nilai')
			);
			$this->Pengaturan_model->createtunjangan($input);
			redirect('pengaturan/penggajian');
		}
		$this->template->load('maintemplate', 'pengaturan/views/tambah_tunjangan_v',$data);			
	}
	
	public function hapuspotongan($id=null)
	{
		$session_data		= $this->session->userdata('logged_in');
		$id_group 			= $session_data['ugroup'];
		$id_user 			= $session_data['id_user'];
		$id_emp 			= $session_data['id'];

		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}
		$this->Pengaturan_model->hapuspotongan($id);
		redirect('pengaturan/gaji');
	}
	
	public function editpotongan()
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$id_pot = $this->input->get('id');

		$data["potongan"] = $this->Pengaturan_model->get_komp_pot($id_pot);
		
		if($this->input->post('submit')){
			$input = array(
				'nama_potongan' 	=> $this->input->post('nm_pot'),
				'tipe_potongan' 	=> $this->input->post('tipe'),
				'status'		  	=> $this->input->post('status'),
				'nilai'				=> $this->input->post('nilai')
			);
			$this->Pengaturan_model->updatepotongan($id_pot,$input);
			redirect('pengaturan/gaji');
		}
		$this->template->load('maintemplate', 'pengaturan/views/edit_potongan_v',$data);
	}

	public function editpendapatanperjabatan()
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$id_pendapatan = $this->input->get('id');
		$data['id_pendapatan'] = $id_pendapatan;
		
		$data["pendapatan"] 		= $this->Pengaturan_model->get_item_penggajian($id_pendapatan);
		$data['id_item_penggajian']	= $data["pendapatan"][0]['id_item_penggajian'];
		$data['id_kom']				= $data["pendapatan"][0]['id_kom'];
		$data['status']				= $data["pendapatan"][0]['status'];
		$data['id_grup']			= $data["pendapatan"][0]['id_grup'];
		$data['tunjanganpendpt']	= $this->Pengaturan_model->komponen_pend();
		//$data['id_kom_pend']		= $data['tunjanganpendpt'][0]['id_kom_pend'];
		//$data['nama_pendapatan']	= $data['tunjanganpendpt'][0]['nama_pendapatan'];
		$data['jabatan']			= $this->Pengaturan_model->grupgaji($data['id_grup']);
		$data['id_grup']			= $data['jabatan'][0]['id_grup'];
		$data['nama_grup']			= $data['jabatan'][0]['nama_grup'];
		

		if($this->input->post('submit')){
			//$id_item_penggajian = $this->input->post('id_item_penggajian');
			$input = array(
				'id_grup' 	=> $this->input->post('jabatan'),
				'id_kom' 	=> $this->input->post('tunjanganpendpt'),
				'status'  	=> $this->input->post('status'),
			);
			$this->Pengaturan_model->updatetunjanganperjabatan($id_pendapatan,$input);
			redirect('pengaturan/penggajian');
		}
		$this->template->load('maintemplate', 'pengaturan/views/edit_pendapatan_v',$data);
	}
	
	public function changesetlembur()
	{
		if($this->input->post('submit')){
			$this->Pengaturan_model->changesetlembur();
		}
	}
	
	public function edittunjangan($id_pen)
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$data["tunjangan"] = $this->Pengaturan_model->get_tunjangan($id_pen);
		if($this->input->post('submit')){
			$input = array(
				'nama_pendapatan' 	=> $this->input->post('nama_pendapatan'),
				'tipe_pendapatan' 	=> $this->input->post('tipependapatan'),
				'is_pph21'		  	=> $this->input->post('kenapph21'),
				'status'			=> $this->input->post('status'),
				'tipe_a1'		 	=> $this->input->post('tipea1'),
				'nilai'			 	=> $this->input->post('nilai')
			);
			$this->Pengaturan_model->updatetunjangan($id_pen,$input);
			redirect('pengaturan/penggajian');
		}
		$this->template->load('maintemplate', 'pengaturan/views/edit_tunjangan_v',$data);
	}
	
	public function hapustunjangan($id)
	{
		$row = $this->Pengaturan_model->hapustunjangan($id);
		if(count($row)>0){
		$pesan = "<div id='notifications' class='alert alert-success' role='alert'><i class='fa fa-info-circle icon_id'><span class='close' aria-hidden='true'>&times;</span></button></i>komponen Pendapatan berhasil dihapus.</div>";
			$this->session->set_flashdata("pesan",$pesan);
			redirect('pengaturan/penggajian');
		}
		
	}
	
	public function tambahkomponen_pot() 
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}
		
		if($this->input->post('submit')){
			$input = array(
				'nama_potongan' 	=> $this->input->post('nm_pot'),
				'tipe_potongan' 	=> $this->input->post('tipe'),
				'status'		  	=> $this->input->post('status'),
				'nilai'				=> $this->input->post('nilai'),
			);
			$this->Pengaturan_model->createpotongan($id_pen,$input);
			redirect('pengaturan/gaji');
		}
		$this->template->load('maintemplate', 'pengaturan/views/tambah_potongan_v',$data);		
	}
	
	public function cuti() 
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}
		
		$tgl = $this->db->get_where('t_referensi_data',array('id_referensi'=>46))->result_array();
		$data['periodecuti'] = date('d F Y',strtotime($tgl[0]['value']));
		
		$this->template->load('maintemplate', 'pengaturan/views/cuti_v',$data);		
	}
	
	public function tambahperaturan_baru() 
	{
		$themes_url			 	= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
        $data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
        
        $user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}
		$this->template->load('maintemplate', 'pengaturan/views/tambah_peraturan_cuti_v',$data);			
	}
	
	public function izin() 
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$data['data_izin'] = $this->Pengaturan_model->get_izin();
		
		$this->template->load('maintemplate', 'pengaturan/views/izin_v',$data);		
	}
	public function addperaturanizin(){

		$tmp = $this->db->query('select count(*) id from t_referensi_data where parent_code=12')->result_array();
		$id  = $tmp[0]['id']+1;
		
		$input  = array(
			'kode_ref' => $id,
			'parent_code' => 12,
			'nama_ref' => $_POST['nama_jenis_izin'],
			'value' => $_POST['maks_pengajuan'],
			'status' => $_POST['status']
		);

		$row = $this->Pengaturan_model->add_izin($input);
		if(count($row)>0){
			$pesan = "<div id='notifications' class='alert alert-success' role='alert'><i class='fa fa-info-circle icon_id'><span class='close' aria-hidden='true'>&times;</span></button></i>Peraturan Izin berhasil ditambahkan.</div>";
			$this->session->set_flashdata("pesan",$pesan);
			redirect('pengaturan/izin');
		}
	}

	public function deleteizin($id){
		$row=$this->Pengaturan_model->deleteizin($id);
		if(count($row)>0){
			$pesan = "<div id='notifications' class='alert alert-danger' role='alert'><i class='fa fa-info-circle icon_id'><span class='close' aria-hidden='true'>&times;</span></button></i>Peraturan Izin berhasil dihapus.</div>";
			$this->session->set_flashdata("pesan",$pesan);
			redirect('pengaturan/izin');
		}
	}

	public function edit_izin_pengaturan($id=null)
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$data['data_izin'] 		= $this->Pengaturan_model->get_izin();
		$data['data_izin_id'] 	= $this->Pengaturan_model->get_izin_by_id($id);
		
		if($this->input->post('submit')){
			$id_izin = $id;
			$input = array(
			'nama_ref' => $this->input->post('nama_jenis'),
			'value' => $this->input->post('max_hari'),
			'status' => $this->input->post('status')
			);
			$this->Pengaturan_model->update_izin($id_izin,$input);
			redirect('pengaturan/izin');
		}
		$this->template->load('maintemplate', 'pengaturan/views/edit_izin',$data);
	}
	
	public function pph21() 
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url); 	 	
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));

		$user_id = $this->ion_auth->get_user_id();
		$comp_id = $this->ion_auth->get_perusahaan_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}
		
		$id_perusahaan			= $comp_id;
		
		$data['aktifpajak']		= $this->Pengaturan_model->get_referensi_pp21();
		$data['statuspajak']	= $data['aktifpajak'][0]['value'];
		$data['pajak']			= $this->Pengaturan_model->methodpajak($id_perusahaan);
		
		if(sizeof($data['pajak'])>0) {
			$data['npwp']			= $data['pajak'][0]['npwp'];
			$status					= $data['pajak'][0]['status'];
			if($status == 1){ 
				$data['status'] = "Sudah Diterapkan";
			}else{
				$data['status'] = "Tidak Diterapkan";
			}
			
			$data['nama_perusahaan']= $data['pajak'][0]['nama_pemotong'];
			$data['netmethod']		= $data['pajak'][0]['netmethod'];
			$data['grossupmethod']	= $data['pajak'][0]['grossupmethod'];
			$data['grossmethod']	= $data['pajak'][0]['grossmethod'];
			
			if($data['netmethod'] == 1){$data['method'] = 'Net Method';}
			if($data['grossupmethod'] == 1){$data['method'] = 'Gross Up Method';}
			if($data['grossmethod'] == 1){$data['method'] = 'Gross Method';}
		}else{
			$data['npwp']			= "";
			$status					= 0;
			if($status == 1){ 
				$data['status'] = "Sudah Diterapkan";
			}else{
				$data['status'] = "Tidak Diterapkan";
			}
			
			$data['nama_perusahaan']= "";
			$data['netmethod']		= 1;
			$data['grossupmethod']	= 1;
			$data['grossmethod']	= 1;
			
			if($data['netmethod'] == 1){$data['method'] = 'Net Method';}
			if($data['grossupmethod'] == 1){$data['method'] = 'Gross Up Method';}
			if($data['grossmethod'] == 1){$data['method'] = 'Gross Method';}
		}
		
		$data['status_ptkp']	= $this->Pengaturan_model->status_ptkp();

		$this->template->load('maintemplate', 'pengaturan/views/pph21_v',$data);		
	}

	public function editpph21()
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url); 	 	
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));

		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$label = $this->input->post('radios3');
		$this->db->where('id_referensi',103);
		$this->db->update('t_referensi_data',array('value' =>$label));
		redirect('pengaturan/pph21');
	}

	public function editptkp(){
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url); 	 	
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));

		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}
		$id = $this->input->get('id');
		$data['id'] = $id;
		$ptkp = $this->Pengaturan_model->status_ptkp($id);
		$data['uraian'] = $ptkp[0]['uraian'];
		$data['statuspajak'] = $ptkp[0]['statuspajak'];
		$data['PTKP'] = $ptkp[0]['PTKP'];
		$data['status'] = $ptkp[0]['status'];
		
		if(isset($_POST['update'])){
			$id = $_GET['id'];
			$update = array(
				'uraian' => $_POST['uraian'],
				'statuspajak' => $_POST['statuspajak'],
				'PTKP' => $_POST['ptkp'],
				'status' => $_POST['status']
			);

			$row = $this->Pengaturan_model->updateptkp($id,$update);
			if(count($row)>0){
				$pesan = "<div id='notifications' class='alert alert-success' role='alert'><i class='fa fa-info-circle icon_id'><span class='close' aria-hidden='true'>&times;</span></button></i>Peraturan PTKP berhasil dirubah.</div>";
				$this->session->set_flashdata("pesan",$pesan);
				redirect('pengaturan/pph21');
			}

		}

		$this->template->load('maintemplate', 'pengaturan/views/edit_ptkp_v',$data);		
	}

	public function editpemotong()
	{
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$id_perusahaan	= $session_data['id_perusahaan'];
		$nama_pemotong	= $this->input->post('nama_pemotong');
		$npwp 			= $this->input->post('npwp');
		$status			= $this->input->post('status');
		
		$this->db->where('id_pemotong',$id_perusahaan);
		$this->db->update('t_pemotong_pph21',array('nama_pemotong' =>$nama_pemotong,'npwp'=>$npwp,'status'=>$status));
		redirect('pengaturan/pph21');
	}

	public function editmethod()
	{
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$id_perusahaan			= $session_data['id_perusahaan'];
		$method 				= $this->input->post('method');
		if($method=='Net Method'){ $netmethod = 1;}else{$netmethod = 0;}
		if($method=='Gross Up Method'){ $grossupmethod = 1;}else{$grossupmethod = 0;}
		if($method=='Gross Method'){ $grossmethod = 1;}else{$grossmethod = 0;}
		$this->db->where('id_pemotong',$id_perusahaan);
		$this->db->update('t_pemotong_pph21',array('netmethod' =>$netmethod,'grossupmethod' => $grossupmethod,'grossmethod'=> $grossmethod));
		redirect('pengaturan/pph21');
	}
	
	public function bpjs() 
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp']		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		//$id_perusahaan 			= $data['site_corp'][0]['id_perusahaan'];
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$data['ump'] 			= $this->Pengaturan_model->get_ump();
		$data['data_kom_pot'] 	= $this->Pengaturan_model->getbpjs();
		$data['referensi']		= $this->Pengaturan_model->get_bpjs_referensi();
		$data['bpjstng']		= $data['referensi'][0]['value'];
		$data['bpjskes']		= $data['referensi'][1]['value'];
		if($data['bpjstng'] == 0) {$data['show'] = " visibility: hidden;";}
		else{$data['show'] = "";}
		
		$this->template->load('maintemplate', 'pengaturan/views/bpjs_v',$data);	
	}

	public function editbpjstenaga()
	{
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}
		
		$label = $this->input->post('radios3');
		$this->db->where('id_referensi',71);
		$this->db->update('t_referensi_data',array('value' =>$label));
		redirect('pengaturan/bpjs');
	}

	public function editbpjskesehatan()
	{
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}
		
		$label = $this->input->post('radios3');
		$this->db->where('id_referensi',72);
		$this->db->update('t_referensi_data',array('value' =>$label));
		redirect('pengaturan/bpjs');
	}
	
	public function histori_bpjs() 
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));

		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}
		$this->template->load('maintemplate', 'pengaturan/views/histori_bpjs_v',$data);		
	}

	public function addump(){
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));

		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		if(isset($_POST['simpan'])){
			$input = array(
				'nama_ump' => $_POST['daerah'],
				'nilai' => $_POST['jmlh_uang']
			);
			$row = $this->Pengaturan_model->insertump($input);
			if(count($row)>0){
				$pesan = "<div id='notifications' class='alert alert-success' role='alert'><i class='fa fa-info-circle icon_id'><span class='close' aria-hidden='true'>&times;</span></button></i>Data berhasil ditambahkan.</div>";
				$this->session->set_flashdata("pesan",$pesan);
				redirect('pengaturan/bpjs');
			}
		}

		$this->template->load('maintemplate', 'pengaturan/views/tambah_ump_v',$data);			
	}

	public function editump(){
		$id = $_GET['id'];
		$data['id'] = $id;

		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));

		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$ump 	= $this->Pengaturan_model->get_ump($id);
		$data['nama_ump'] = $ump[0]['nama_ump'];
		$data['nilai'] = $ump[0]['nilai'];

		if(isset($_POST['simpan'])){
			$id = $_GET['id'];
			$update = array(
				'nama_ump' => $_POST['daerah'],
				'nilai' => $_POST['jmlh_uang']
			);
			$row = $this->Pengaturan_model->updateump($id,$update);
			if(count($row)>0){
				$pesan = "<div id='notifications' class='alert alert-success' role='alert'><i class='fa fa-info-circle icon_id'><span class='close' aria-hidden='true'>&times;</span></button></i>Data berhasil dirubah.</div>";
				$this->session->set_flashdata("pesan",$pesan);
				redirect('pengaturan/bpjs');
			}
		}

		$this->template->load('maintemplate', 'pengaturan/views/edit_ump_v',$data);
	}
	
	public function deleteump($id){
		$row = $this->Pengaturan_model->deleteump($id,$update);
		if(count($row)>0){
			$pesan = "<div id='notifications' class='alert alert-danger' role='alert'><i class='fa fa-info-circle icon_id'><span class='close' aria-hidden='true'>&times;</span></button></i>Data berhasil dihapus.</div>";
			$this->session->set_flashdata("pesan",$pesan);
			redirect('pengaturan/bpjs');
		}
	}

	public function polker() 
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		
		
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$data['polakerja']		= $this->Personalia_model->get_pola();
		
		$this->template->load('maintemplate', 'pengaturan/views/polker_v',$data);		
	}
	
	public function tambahpolker() 
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));

		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}
		$this->template->load('maintemplate', 'pengaturan/views/tambah_polker_v',$data);		
	}
	
	public function insertpolker()
	{
		$masuk	= $this->input->post('jadwalmasuk');
		$keluar	= $this->input->post('jadwalpulang');
		
		$input 	= array(
			'nama_pola' 		=> $this->input->post('nama_polker'),
			'jumlah_hari_kerja' => $this->input->post('jumlahharipola'),
			'toleransi_late' 	=> $this->input->post('toleransitelat'),
			'jumlah_libur' 		=> $this->input->post('lamapola'),
			'lama_pola' 		=> $this->input->post('lamapola'),
			'jadwal_masuk' 		=> date('h:m:s',strtotime($masuk)),
			'jadwal_keluar' 	=> date('h:m:s',strtotime($keluar))
		);
				
		$row = $this->Pengaturan_model->createpolker($input);
		if(count($row)){
			$pesan = "<div id='notifications' class='alert alert-success' role='alert'><i class='fa fa-info-circle icon_id'><span class='close' aria-hidden='true'>&times;</span></button></i>Data berhasil disimpan.</div>";
			$this->session->set_flashdata("pesan",$pesan);
			redirect('pengaturan/polker');
		}
		
	}
	
	public function editaturangaji()
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$data['get_aturangaji'] = $this->Pengaturan_model->get_aturangaji();
		if($this->input->post("submit")){
			
			
			$nilaiuanglembur = array(
				'value' =>  $this->input->post("nilaiuanglembur")
			);
			$nilai = array(
				'value' =>  $this->input->post("nilai")
			);
			$no3 = array(
				'value' =>  $this->input->post("radios3")
			);
			$no6 = array(
				'value' =>  $this->input->post("radios6")
			);
			$no7 = array(
				'value' =>  $this->input->post("radios7")
			);
			$no8 = array(
				'value' =>  $this->input->post("radios8")
			);
			$this->Pengaturan_model->updatesetgaji($no3,$nilaiuanglembur,$nilai,$no6,$no7,$no8);
			redirect('pengaturan/penggajian');
			
		}
		$this->template->load('maintemplate', 'pengaturan/views/edit_peraturan_gaji_v',$data);
	}
	
	public function configurasi()
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}
		
		$perusahaan			= $this->Pengaturan_model->getperusahaan();
		if(sizeof($perusahaan)>0){
			$data['namaperusanaan']	= $perusahaan[0]['nama_perusahaan'];
			$data['bidang']	= $perusahaan[0]['bidang'];
			$data['alamat']	= $perusahaan[0]['alamat'];
			$data['tgl_reg']= $perusahaan[0]['tgl_reg'];
			$data['end_reg']= $perusahaan[0]['end_reg'];
			$data['status']	= $perusahaan[0]['status_reg'];
		}
		$this->template->load('maintemplate', 'pengaturan/views/configurasi_v',$data);
	}
	
	public function editsetperusahaan()
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$perusahaan			= $this->Pengaturan_model->getperusahaan();
		if(sizeof($perusahaan)>0){
			$data['id_perusahaan']	= $perusahaan[0]['id_perusahaan'];
			$data['namaperusanaan']	= $perusahaan[0]['nama_perusahaan'];
			$data['bidang']	= $perusahaan[0]['bidang'];
			$data['alamat']	= $perusahaan[0]['alamat'];
			$data['tgl_reg']= $perusahaan[0]['tgl_reg'];
			$data['end_reg']= $perusahaan[0]['end_reg'];
			$data['status']	= $perusahaan[0]['status_reg'];
		}
		if($this->input->post('simpanfile')){
			
			$nama_perusahaan = $this->input->post('namaperusanaan');
			$bidang 		= $this->input->post('bidang');
			$alamat 		= $this->input->post('alamat');
			$id 			= $this->input->post('id_perusahaan');

			$namalogoback	= $_FILES['file_back']['name'];
			$namalogofront	= $_FILES['file_front']['name'];
			$namaicon		= $_FILES['file_icon']['name'];

			$sizeback		= $_FILES['file_back']['size'];
			$sizefront		= $_FILES['file_front']['size'];
			$sizeicon		= $_FILES['file_icon']['size'];
			
			$maxsize 		= 1024 * 500;
			$extensionList 	= array("jpg", "PNG", "png", "jpeg","JPG");

			if(!is_null($namalogoback) || !is_null($namalogofront) || !is_null($namaicon)){
				if($sizeback < $maxsize || $sizefront < $maxsize || $sizeicon < $maxsize){
					$pecah1 		= explode(".", $namalogoback);
					$pecah2 		= explode(".", $namalogofront);
					$pecah3 		= explode(".", $namaicon);
					
					$ekstensi1 	= $pecah1[1];
					$ekstensi2 	= $pecah2[1];
					$ekstensi3 	= $pecah3[1];

					$namaDir1 	= 'upload/gambar/';
					
					
					$pathFile1 	= time()."_".$namalogoback;
					$pathFile2 	= time()."_".$namalogofront;
					$pathFile3 	= time()."_".$namaicon;

					if(in_array($ekstensi1, $extensionList) || in_array($ekstensi2, $extensionList) || in_array($ekstensi3, $extensionList)){
						
						$tmp1 	= $_FILES['file_back']['tmp_name'];
						$tmp2 	= $_FILES['file_front']['tmp_name'];
						$tmp3 	= $_FILES['file_icon']['tmp_name'];
						if(move_uploaded_file($tmp1, $namaDir1."".$pathFile1) && 
							move_uploaded_file($tmp2, $namaDir1."".$pathFile2) && 
							move_uploaded_file($tmp3, $namaDir1."".$pathFile3)){
							$update	= array(
								'nama_perusahaan' => $nama_perusahaan,
								'bidang'		  => $bidang,	
								'alamat'		  => $alamat,	
								'logodalam'		  => $pathFile1,	
								'logodepan'		  => $pathFile2,
								'icon'			  => $pathFile3
							);
							$this->Pengaturan_model->updateperusahaan($id,$update);
							redirect('pengaturan/configurasi');
						}
					}else{
						var_dump("extensi salah");die;
					}
				}else{
					var_dump("ukurang gagal");die;
				}
			}else{
				var_dump("kosong");die;
			}
		}
		$this->template->load('maintemplate', 'pengaturan/views/edit_configurasi_v',$data);
	}
	
	// public function accountuser()
	// {
	// 	$themes_url 			= 'themes/hr/';
	// 	$data['themes_url'] 	= base_url($themes_url);
	// 	$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		
	// 	$user_id = $this->ion_auth->get_user_id();
	// 	$emp_id  = $this->ion_auth->get_employee_id();
	// 	$nama    = $this->ion_auth->get_name_user();
		
	// 	if($user_id == ""){
	// 		$this->session->sess_destroy();
	// 		redirect(site_url("auth/logout"));
	// 	}

	// 	$data['dataemp']	         = $this->Pengaturan_model->datauser();

	
	// 	$this->template->load('maintemplate', 'pengaturan/views/accountuser_v',$data);
	// }
	
	// public function updateaccount()
	// {
	// 	$themes_url 			= 'themes/hr/';
	// 	$data['themes_url']		= base_url($themes_url);
	// 	$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		
	// 	$user_id = $this->ion_auth->get_user_id();
	// 	$emp_id  = $this->ion_auth->get_employee_id();
	// 	$nama    = $this->ion_auth->get_name_user();
		
	// 	if($user_id == ""){
	// 		$this->session->sess_destroy();
	// 		redirect(site_url("auth/logout"));
	// 	}


	// 	$data['user']		= $user_id;
	// 	$id_user			  	= $this->input->get('id');
	// 	$id_emp				  	= $this->input->get('idemp');
		
	// 	if($user_id == 1){
	// 		$dataemp 			= $this->Pengaturan_model->datauseremp($id_user);
	// 	}else{
	// 		$dataemp 			= $this->Pengaturan_model->datauser($user_id);	
	// 	}
		
	// 	$data['nama_karyawan']	= $dataemp[0]['nama_karyawan'];
	// 	$data['id_user']		= $dataemp[0]['id_user'];
	// 	$data['password']	  	= $dataemp[0]['password'];
	// 	$data['usergroup']	  	= $dataemp[0]['id'];
	// 	$data['email']		  	= $dataemp[0]['email'];
	// 	$data['msg']		  	= '';

	// 	if(isset($_POST['update'])){
	// 		var_dump("expression");die;
	// 	}
		
	// 	$this->template->load('maintemplate', 'pengaturan/views/update_account_v',$data);
	// }

	// public function updateusergroup()
	// {
	// 	$data['id']			  = $this->input->get('id');
	// 	$usergroup			  = $this->input->post('usergroup');
	// 	$oldpass			  = $this->input->post('oldpass');
	// 	$hasholdpass 		  = hash('sha256',$oldpass);

	// 	$user_id = $this->ion_auth->get_user_id();
	// 	$emp_id  = $this->ion_auth->get_employee_id();
	// 	$nama    = $this->ion_auth->get_name_user();
		
	// 	if($user_id == ""){
	// 		$this->session->sess_destroy();
	// 		redirect(site_url("auth/logout"));
	// 	}

	// 	$newpass			  = $this->input->post('newpass');
	// 	$newpass2			  = $this->input->post('newpass2');
	// 	$filefoto 			  = $_FILES['foto']['size']; 
	// 	$maxsize 			  = 1024 * 500;

	// 	$dataemp			  = $this->Personalia_model->datauser($data['id']);
	// 	$data['password']	  = $dataemp[0]['password'];
		
	// 	if($data['password'] != $hasholdpass){
	// 		$data['msg'] = 'tidak sama';
	// 		redirect('personalia/account_setting?id='.$data['id'],$data);
	// 		//'<script>alert("Format Email Salah, Silahkan Cek Ulang Email Anda.");</script>';
	// 	}
		
	// 	if($user_id <= 2){$namauser="Superadmin";}elseif($user_id == 2){$namauser="Admin";}else{$namauser="User";}
		
	// 	if($filefoto <= $maxsize){
	// 		$extensionList 	= array("jpg", "png", "PNG", "jpeg");
	// 		$pecah			= ""; 
	// 		$ekstensi		= null; 
			
	// 		if($_FILES['foto']['name']  != "")
	// 		{
	// 			$pecah 		= explode(".", $_FILES['foto']['name']);
	// 			$ekstensi 	= $pecah[1];
				
	// 			$namaDir 	= 'upload/fotopribadi/';
				
	// 			// membuat path nama direktori + nama file.
	// 			$pathFile 	= time()."_".$_FILES['foto']['name'];
				
	// 		}
			
	// 		if (in_array($ekstensi, $extensionList)){ 
	// 				// memindahkan file ke temporary
	// 				$tmpfoto  	= $_FILES['foto']['tmp_name'];

	// 				// proses upload file dari temporary ke path file
	// 				if (move_uploaded_file($_FILES['foto']['tmp_name'], $namaDir."".$pathFile.".".$ekstensi)){
	// 					$this->db->where('id_employee',$idemp)->update('t_employee',array('foto' => $pathFile));
	// 				}
	// 			}else{
	// 				$pesan = "Maaf, file yang diupload hanya berextensi *.pdf,*.png,*.jpg,*.jpeg";
	// 				$this->session->set_flashdata('pesan', $pesan);
	// 			}
	// 	}else{
	// 		$pesan = "Maaf, file yang diupload tidak boleh lebih dari 500 Kb.";
	// 		$this->session->set_flashdata('pesan', $pesan);
	// 	}

	// 	$update = array(
	// 		'nama' 	   => $namauser,
	// 		'password' => hash('sha256',$newpass),
	// 		'usergroup'=> $usergroup
	// 	);

	// 	$row = $this->Personalia_model->changeuser($user_id,$update);
	// 	if(count($row)>0){
	// 		$pesan = "Perubahan Data sukses tersimpan.";
			
	// 		redirect('personalia/account_setting',$this->session->set_flashdata('pesan', $pesan));
	// 	}else{
	// 		$pesan = "Perubahan Data gagal tersimpan.!";
	// 		$this->session->set_flashdata('pesan', $pesan);
	// 		redirect('personalia/account_setting');
	// 	}
	// }
	
	public function updatepolker()
	{
		$themes_url 			= 'themes/hr/';
		$data['themes_url'] 	= base_url($themes_url);
		$data['site_corp'] 		= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$id 							= $this->input->get('id');
		$data['polker']					= $this->Pengaturan_model->get_polker($id);
		if(sizeof($data['polker'])>0){
			$data['id_pola']			= $data['polker'][0]['id_pola'];
			$data['pola']				= $data['polker'][0]['nama_pola'];
			$data['jumlah_hari_kerja']	= $data['polker'][0]['jumlah_hari_kerja'];
			$data['toleransi_late']		= $data['polker'][0]['toleransi_late'];
			$data['jumlah_libur']		= $data['polker'][0]['jumlah_libur'];
			$data['lama_pola']			= $data['polker'][0]['lama_pola'];
			$data['jadwal_masuk']		= $data['polker'][0]['jadwal_masuk'];
			$data['jadwal_keluar']		= $data['polker'][0]['jadwal_keluar'];
		}

		if(isset($_POST['update'])){

			$id		= $this->input->get('id');
			$masuk	= $this->input->post('jadwalmasuk');
			$pulang	= $this->input->post('jadwalpulang');
			$update = array(
				'nama_pola' 		=> $this->input->post('nama_polker'),
				'jumlah_hari_kerja' => $this->input->post('jumlahharipola'),
				'jumlah_libur' 		=> $this->input->post('jumlahlibur'),
				'lama_pola' 		=> $this->input->post('lamapola'),
				'jadwal_masuk' 		=> $masuk, //date('h:m:s',strtotime($masuk)),
				'jadwal_keluar' 	=> $pulang, //date('h:m:s',strtotime($pulang)),
				'toleransi_late' 	=> $this->input->post('toleransitelat')
			);
			$row = $this->Pengaturan_model->updatepolker($id,$update);
			if(count($row)>0){
				$pesan = "<div id='notifications' class='alert alert-success' role='alert'><i class='fa fa-info-circle icon_id'><span class='close' aria-hidden='true'>&times;</span></button></i>Data berhasil berubah.</div>";
				$this->session->set_flashdata("pesan",$pesan);
				redirect('pengaturan/polker');
			}
			
		}	

		$this->template->load('maintemplate', 'pengaturan/views/edit_polker_v',$data);
	}
	
	public function updatepolkerrow()
	{
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}
		
		
	}
	
	public function deletepolker()
	{
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$id 	= $this->input->get('id');
		$row = $this->Pengaturan_model->deletepolker($id);
		if(count($row)>0){
			$pesan = "<div id='notifications' class='alert alert-danger' role='alert'><i class='fa fa-info-circle icon_id'><span class='close' aria-hidden='true'>&times;</span></button></i>Data berhasil dihapus.</div>";
			$this->session->set_flashdata("pesan",$pesan);
			redirect('pengaturan/polker');
		}
		
	}
	
	public function deleteaccount()
	{
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$id 	= $this->input->get('id');
		$this->Pengaturan_model->deleteaccount($id);
		redirect('pengaturan/accountuser');		
	}
	
	public function aktifkanuser()
	{
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$id 	= $this->input->get('id');
		$this->Pengaturan_model->aktifuser($id);
		redirect('pengaturan/accountuser');		
	}

	public function editberlakuperiodecuti()
	{
		$user_id = $this->ion_auth->get_user_id();
		$emp_id  = $this->ion_auth->get_employee_id();
		$nama    = $this->ion_auth->get_name_user();
		
		if($user_id == ""){
			$this->session->sess_destroy();
			redirect(site_url("auth/logout"));
		}

		$a = $this->input->post('tanggal');
		$b = $this->input->post('bulan');
		if(strlen($a)!=1){
			$tgl = date("Y")."-".$b."-".$a;	
		}else{
			$tgl = date("Y").'-'.$b."-0".$a;	
		}
		
		$this->db->where('id_referensi',46);
		$this->db->update('t_referensi_data',array('value' => $tgl));

		redirect('pengaturan/cuti');
	}

	//---------------------------- Master Pangkat -------------------------------//
	public function pangkat(){
		$themes_url 		= 'themes/hr/';
		$data['themes_url'] = base_url($themes_url);
		$data['site_corp'] 	= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		
		$data['pangkat']	= $this->Pengaturan_model->get_pangkat();

		$this->template->load('maintemplate', 'pengaturan/views/pangkat/pangkat_v',$data);
	}

	public function insertpangkat(){

			$input 			= array(
				'nama_pangkat' 	=> $this->input->post('pangkat'),
				'golongan' 		=> $this->input->post('gol')
			);
			$row 			= $this->Pengaturan_model->insert_pangkat($input);
			if(count($row)>0){
				$pesan = "<div id='notifications' class='alert alert-success' role='alert'><i class='fa fa-info-circle icon_id'><span class='close' aria-hidden='true'>&times;</span></button></i>Data berhasil disimpan.</div>";
				$this->session->set_flashdata("pesan",$pesan);
				redirect('pengaturan/pangkat');
			}else{
				$pesan = "<div id='notifications' class='alert alert-danger' role='alert'><i class='fa fa-info-circle icon_id'><span class='close' aria-hidden='true'>&times;</span></button></i>Data Gagal disimpan.</div>";
				$this->session->set_flashdata("pesan",$pesan);
				redirect('pengaturan/pangkat');
			}
	}

	public function editpangkat(){
		$themes_url 		= 'themes/hr/';
		$data['themes_url'] = base_url($themes_url);
		$data['site_corp'] 	= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		
		$id 				= $this->input->get('id');
		$data['id']			= $id;
		$pangkat			= $this->Pengaturan_model->get_pangkat($id);
		$data['nama_p']		= $pangkat[0]['nama_pangkat'];
		$data['gol']		= $pangkat[0]['golongan'];
		if($this->input->post('simpan')){
			$id 			= $this->input->get('id');
			$update 		= array(
				'nama_pangkat'=> $this->input->post('pangkat'),
				'golongan'=> $this->input->post('gol')
			);
			$row 			= $this->Personalia_model->updatepangkat($id,$update);
			if(count($row)>0){
				$pesan = "<div id='notifications' class='alert alert-success' role='alert'><i class='fa fa-info-circle icon_id'><span class='close' aria-hidden='true'>&times;</span></button></i>Data berhasil diubah.</div>";
				$this->session->set_flashdata("pesan",$pesan);
				redirect('pengaturan/pangkat');
			}else{
				$pesan = "<div id='notifications' class='alert alert-danger' role='alert'><i class='fa fa-info-circle icon_id'><span class='close' aria-hidden='true'>&times;</span></button></i>Data Gagal diubah.</div>";
				$this->session->set_flashdata("pesan",$pesan);
				redirect('pengaturan/pangkat');
			}
		}

		$this->template->load('maintemplate', 'pengaturan/views/pangkat/edit_pangkat_v',$data);
	}

	public function deletepangkat(){
		$id = $this->input->get('id');
		$this->db->where('id_pangkat',$id)->delete('t_pangkat');
		$pesan = "<div id='notifications' class='alert alert-danger' role='alert'><i class='fa fa-info-circle icon_id'><span class='close' aria-hidden='true'>&times;</span></button></i>Data berhasil dihapus.</div>";
		$this->session->set_flashdata("pesan",$pesan);
		redirect('pengaturan/pangkat');
	}

	//---------------------------- Master Pola Karir -------------------------------//
	public function polakarir(){
		$themes_url 		= 'themes/hr/';
		$data['themes_url'] = base_url($themes_url);
		$data['site_corp'] 	= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		
		$data['polakarir']		= $this->Pengaturan_model->get_polakarir();
		$data['deptcurrent'] 	= $this->Pengaturan_model->get_bagian();
		$data['deptnext'] 		= $this->Pengaturan_model->get_bagian();

		if($this->input->post('simpan')){
			$input = array(
				'id_dept_current' 	=> $this->input->post('current'),
				'id_dept_next' 		=> $this->input->post('next'),
				'status' 			=> $this->input->post('status')
			);
			$row 				= $this->Pengaturan_model->insertpolakarir($input);
			if(count($row)>0){
				$pesan = "<div id='notifications' class='alert alert-success' role='alert'><i class='fa fa-info-circle icon_id'><span class='close' aria-hidden='true'>&times;</span></button></i>Data berhasil disimpan.</div>";
				$this->session->set_flashdata("pesan",$pesan);
				redirect('pengaturan/polakarir');
			}else{
				$pesan = "<div id='notifications' class='alert alert-danger' role='alert'><i class='fa fa-info-circle icon_id'><span class='close' aria-hidden='true'>&times;</span></button></i>Data Gagal disimpan.</div>";
				$this->session->set_flashdata("pesan",$pesan);
				redirect('pengaturan/polakarir');
			}
		}

		$this->template->load('maintemplate', 'pengaturan/views/masterpemetaankarir/pola_karir_v',$data);
		
	}

	public function editpolakarir(){
		$themes_url 		= 'themes/hr/';
		$data['themes_url'] = base_url($themes_url);
		$data['site_corp'] 	= $this->User_model->get_dataperusahaan_by_domain($this->config->item("domain"));
		$id 				= $this->input->get('id');
		$data['id']			= $id;
		$data['polakarir']		= $this->Pengaturan_model->get_polakarir();
		$data['id_current']		= $data['polakarir'][0]['id_dept_current'];
		$data['id_next']		= $data['polakarir'][0]['id_dept_next'];
		$data['status']			= $data['polakarir'][0]['status'];
		$data['deptcurrent']	= $this->Pengaturan_model->get_bagian();
		$data['deptnext']		= $this->Pengaturan_model->get_bagian();

		if($this->input->post('simpan')){
			$id 	= $this->input->get("id");
			$input = array(
				'id_dept_current' 	=> $this->input->post('current'),
				'id_dept_next' 		=> $this->input->post('next'),
				'status' 			=> $this->input->post('status')
			);
			$row 				= $this->Pengaturan_model->updatepolakarir($id,$input);
			if(count($row)>0){
				$pesan = "<div id='notifications' class='alert alert-success' role='alert'><i class='fa fa-info-circle icon_id'><span class='close' aria-hidden='true'>&times;</span></button></i>Data berhasil diubah.</div>";
				$this->session->set_flashdata("pesan",$pesan);
				redirect('pengaturan/polakarir');
			}else{
				$pesan = "<div id='notifications' class='alert alert-danger' role='alert'><i class='fa fa-info-circle icon_id'><span class='close' aria-hidden='true'>&times;</span></button></i>Data Gagal digagal.</div>";
				$this->session->set_flashdata("pesan",$pesan);
				redirect('pengaturan/polakarir');
			}
		}

		$this->template->load('maintemplate', 'pengaturan/views/masterpemetaankarir/edit_polakarir_v',$data);
		
	}

	public function deletepolakarir(){
		$id = $this->input->get('id');
		$this->db->where('id_pola_karir',$id)->delete('t_pola_karir');
		$pesan = "<div id='notifications' class='alert alert-danger' role='alert'><i class='fa fa-info-circle icon_id'><span class='close' aria-hidden='true'>&times;</span></button></i>Data berhasil dihapus.</div>";
		$this->session->set_flashdata("pesan",$pesan);
		redirect('pengaturan/polakarir');
	}

	

	
}
	
	
	
	
