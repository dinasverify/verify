			<!--smoott-->   
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">
    <link href="<?php echo $themes_url; ?>vendors/hover/css/hover-min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $themes_url; ?>vendors/laddabootstrap/css/ladda-themeless.min.css">
    <link href="<?php echo $themes_url; ?>css/buttons_sass.css" rel="stylesheet">
    <link href="<?php echo $themes_url; ?>css/advbuttons.css" rel="stylesheet">

		<!-- Content Header (Page header) -->

		<section class="content-header">

            <h1>Cuti</h1>

            <ol class="breadcrumb">

                <li>

                    <a href="<?php echo site_url('dashboard');?>">

                        <i class="fa fa-fw ti-home"></i> Dashboard

                    </a>

                </li>

                <li> <a href="<?php echo site_url('pengaturan');?>">Pengaturan</a></li>

				<li> Cuti </li>

            </ol>

		</section>

		<!-- Main content -->

        <section class="content p-l-r-15">

            <div class="row">			

                <div class="col-md-12">

                    <div class="panel">

                        <div class="panel-heading">

							<h4 class="panel-title">

                                <i class="ti-car"></i> Cuti

                            </h4>	
                             <span class="pull-right">
	                                <i class="fa fa-fw ti-angle-up clickable"></i>
	                                <i class="fa  fa-fw ti-close removepanel clickable"></i>
                            </span>

                        </div>

						

						<div class="panel-body">

							<div class="row">

								<div class="col-xs-12">

									<div class="row">

										<div class="col-md-3">

											<select class="form-control" name="Periode" id="Periode" required="">

												<option selected="selected" value="">Periode Bersama 2017-10-31</option>

												<option value="">Periode Bersama 2018-01-01</option>

											</select>

										</div>

										<div class="col-md-3 col-md-offset-6">

											<div class="pull-right">

												<!-- <a href="<?php echo site_url('pengaturan/tambahperaturan_baru'); ?>" class="btn btn-primary btn-sm"  data-style="Peraturan Baru">

													<i class="fa ti-plus" aria-hidden="true"></i> Peraturan Baru

												</a> -->

											</div>

										</div>

									</div>

								</div>

							</div></br>

							

							<div class="row">

								<div class="col-xs-12">

									<div class="row">

										<div class="col-md-10">

											<h4>1. Pilih periode cuti yang berlaku di perusahaan Anda:

											<a data-target="#editpoint1" data-toggle="modal" data-animate-modal="fadeIn"><button type="button" id="myBtn1" class="btn btn-sm btn-primary" ><i class="fa ti-pencil" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></button></a></h4>

										</div>

										<div class="col-md-2">

											<div class="pull-right">

												<button type="button" id="myBtn2" class="btn btn-sm btn-default"  data-target="#info" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>

											</div>

										</div>

									</div>

								</div>

							</div>

							

							<div class="row">

								<div class="col-xs-12">

									<div class="table col-md-9">

										<table>

											<tr>

												<td style="width:20px"><i class="fa fa-check"></i></td>

												<td style="width:400px">Periode bersama yang diperbaharui setiap tanggal <?php echo date('d F',strtotime($periodecuti))?></td>

											</tr>

										</table>

									</div>

								</div>

							</div>

							

							<div class="row">

								<div class="col-xs-12">

									<div class="row">

										<div class="col-md-10">

											<h4>2. Berapa jumlah maksimal hari cuti (tahunan) yang diizinkan perusahaan Anda?

											<a data-target="#editpoint2" data-toggle="modal" data-animate-modal="fadeIn"><button type="button" id="myBtn3" class="btn btn-sm btn-primary" ><i class="fa ti-pencil" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></button></a></h4>

										</div>

										<div class="col-md-2">

											<div class="pull-right">

												<button type="button" id="myBtn4" class="btn btn-sm btn-default"  data-target="#info2" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>

											</div>

										</div>

									</div>

								</div>

							</div>

							<div class="row">

								<div class="col-xs-12">

									<div class="table col-md-9">

										<table>

											<tr>

												<td style="width:20px"><i class="fa fa-check"></i></td>

												<td style="width:200px">Jumlah hari :</td>

												<td style="width:50px">15</td>

												<td style="width:50px">hari</td>

											</tr>

										</table>

									</div>

								</div>

							</div>

							<div class="row">

								<div class="col-xs-12">

									<div class="row">

										<div class="col-md-10">

											<h4>3. Berapakah sisa jatah cuti di akhir periode cuti yang boleh digunakan di periode berikutnya (carry forward)?

											<a data-target="#editpoint3" data-toggle="modal" data-animate-modal="fadeIn"><button type="button" id="myBtn3" class="btn btn-sm btn-primary" ><i class="fa ti-pencil" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></button></a></h4>

										</div>

										<div class="col-md-2">

											<div class="pull-right">

												<button type="button" id="myBtn6" class="btn btn-sm btn-default"  data-target="#info3" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>

											</div>

										</div>

									</div>

								</div>

							</div>

							

							<div class="row">

								<div class="col-xs-12">

									<div class="table col-md-9">

										<table>

											<tr>

												<td style="width:20px"><i class="fa fa-check"></i></td>

												<td style="width:200px">Jumlah maksimal :</td>

												<td style="width:50px">3</td>

												<td style="width:50px">hari</td>

											</tr>

											<tr>

												<td style="width:20px"><i class="fa fa-check"></i></td>

												<td style="width:200px">Berlaku sampai :</td>

												<td style="width:50px">12</td>

												<td style="width:300px">Bulan sejak periode berikutnya dimulai</td>

											</tr>

										</table>

									</div>

								</div>

							</div>

							

							<div class="row">

								<div class="col-xs-12">

									<div class="table-responsive">

										<table class="table table-bordered">

											<thead style="background:#0088cc;color:white">

												<tr>

													<th>Catatan : </br>

														- Sisa jatah cuti setiap personalia yang ada sebelum perusahaan Anda menggunakan Gadjian dapat diisi sesudah Anda melengkapi Data Personalia.</br>

														- Di Gadjian, personalia yang berhak mendapatkan jatah cuti adalah personalia yang telah bekerja minimal selama 12 bulan (berdasarkan UU Ketenagakerjaan No.13 Tahun 2003 pasal 79 ayat 2)

													</th>

												</tr>

											</thead>

										</table>

									</div>

								</div>

							</div>

							

							<div class="row">

								<div class="col-xs-12">

									<div class="center">

										<a href="<?php echo site_url('pengaturan/penggajian'); ?>" type="button" class="btn btn-sm btn-labeled btn-danger" >

												<span class="btn-label">

												<i class="glyphicon glyphicon-chevron-left"></i>

											</span> Kembali ke Daftar Slip Gaji

										</a>

									</div>

								</div>

							</div></br>

							

							<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="Heading" aria-hidden="true">

								<div class="modal-dialog">

									<div class="modal-content">

										<div class="modal-header">

											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×

											</button>

											<h4 class="modal-title custom_align" id="Heading">Delete User</h4>

										</div>

										<div class="modal-body">

											<div class="alert alert-warning">

												<span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to

												delete this Account?

											</div>

										</div>

										<div class="modal-footer ">

											<button onClick="document.location='<?php echo site_url('pengaturan'); ?>'" type="submit" class="btn btn-danger">

												<span class="glyphicon glyphicon-ok-sign"></span> Yes

											</button>

											<button type="button" class="btn btn-success" data-dismiss="modal">

												<span class="glyphicon glyphicon-remove"></span> No

											</button>

										</div>

									</div>

									<!-- /.modal-content -->

								</div>

							</div>

							

							<div id="info" class="modal fade animated" role="dialog">

								<div class="modal-dialog">

									<div class="modal-content">

										<div class="modal-body">

											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>

											<p class="text-center">

												Gadjian akan menghitung jatah cuti bagi tiap personalia terdaftar berdasarkan periode cuti yang Anda pilih.

											</p>

										</div>

									</div>

								</div>

							</div>

							

							<div id="info2" class="modal fade animated" role="dialog">

								<div class="modal-dialog">

									<div class="modal-content">

										<div class="modal-body">

											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>

											<p class="text-center">

												Pengaturan ini hanya berlaku untuk cuti tahunan bagi personalia (karyawan) tetap.

											</p>

										</div>

									</div>

								</div>

							</div>

							

							<div id="info3" class="modal fade animated" role="dialog">

								<div class="modal-dialog">

									<div class="modal-content">

										<div class="modal-body">

											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>

											<p class="text-center">

												Jatah cuti periode lalu akan hangus secara otomatis di akhir periode carry forward.

											</p>

										</div>

									</div>

								</div>

							</div>

							<!--Pop Up Point 1-->

							<div class='modal fade' id='editpoint1' tabindex='-1' role='dialog' aria-labelledby='edit' aria-hidden='true'>

								<div class='modal-dialog'>

									<div class='modal-content'>

										<div class='modal-header'>

											<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>

											<h4 class='modal-title custom_align' id='Heading'>Edit Point 1</h4>

										</div>

										<div class='modal-body'>

											<form class="form-horizontal" id="form_edit_kalender" method="POST" action="<?php echo base_url('pengaturan/editberlakuperiodecuti');?>">

											<div class='modal-header'>

											<div class='form-group'>

												<h4>1. Pilih periode cuti yang berlaku di perusahaan Anda:</h4>

											</div>

											<div class='form-group'>

												<div class="col-md-8">

													<div class="col-md-3"><h4>Tanggal </h4></div>

													<div class="col-md-5" style="width: 65%">

														<select class="form-control" name="tanggal" required="">

														<option value="">Pilih Tanggal</option>

														<?php for($tgl=1;$tgl<=31;$tgl++){?>

														<option value="<?php echo $tgl;?>"><?php echo $tgl;?></option>

														<?php } ?>

														</select>

													</div>

												</div>

											</div>

											<div class='form-group'>

												<div class="col-md-8">

													<div class="col-md-3"><h4>Bulan </h4></div>

													<div class="col-md-5" style="width: 75%">

														<select class="form-control" name="bulan" required="">

															<option value="">Pilih Bulan</option>

															<option value="01">Januari</option>

															<option value="02">Februari</option>

															<option value="03">Maret</option>

															<option value="04">April</option>

															<option value="05">Mei</option>

															<option value="06">Juni</option>

															<option value="07">Juli</option>

															<option value="08">Agustus</option>

															<option value="09">September</option>

															<option value="10">Oktober</option>

															<option value="11">November</option>

															<option value="12">Desember</option>

														</select>

													</div>

												</div>

											</div>

										</div>

										<div class='modal-footer'>

											<button type='submit' class='btn btn-sm btn-primary'>

												<span class='glyphicon glyphicon-ok-sign'></span> Update

											</button>

										</div>

									</form>

									</div>

									<!-- /.modal-content -->

								</div>

							</div>

							</div>



							<div class='modal fade' id='editpoint2' tabindex='-1' role='dialog' aria-labelledby='edit' aria-hidden='true'>

								<div class='modal-dialog'>

									<div class='modal-content'>

										<div class='modal-header'>

											<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>

											<h4 class='modal-title custom_align' id='Heading'>Edit Point 2</h4>

										</div>

										<div class='modal-body'>

											<form class="form-horizontal" id="form_edit_kalender" method="POST" action="<?php echo base_url('pengaturan/editmaxcutitahuan');?>">

											<div class='modal-header'>

											<div class='form-group'>

												<h4>2. Berapa jumlah maksimal hari cuti (tahunan) yang diizinkan perusahaan Anda?</h4>

											</div>

											<div class='form-group'>

												<div class="col-md-8">

													<div class="col-md-3" style="width: 40%"><h4>Jumlah Hari </h4></div>

													<div class="col-md-5" style="width: 50%">

														<select class="form-control" name="tanggal" required="">

														<option value="">Berapa hari :</option>

														<?php for($tgl=1;$tgl<=15;$tgl++){?>

														<option value="<?php echo $tgl;?>"><?php echo $tgl;?></option>

														<?php } ?>

														</select>

													</div>

												</div>

											</div>

										</div>

										<div class='modal-footer'>

											<button type='submit' class='btn btn-sm btn-primary'>

												<span class='glyphicon glyphicon-ok-sign'></span> Update

											</button>

										</div>

									</form>

									</div>

									<!-- /.modal-content -->

								</div>

							</div>

							</div>



							<div class='modal fade' id='editpoint3' tabindex='-1' role='dialog' aria-labelledby='edit' aria-hidden='true'>

								<div class='modal-dialog'>

									<div class='modal-content'>

										<div class='modal-header'>

											<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>

											<h4 class='modal-title custom_align' id='Heading'>Edit Point 3</h4>

										</div>

										<div class='modal-body'>

											<form class="form-horizontal" id="form_edit_kalender" method="POST" action="<?php echo base_url('pengaturan/editcarryforward');?>">

											<div class='modal-header'>

												<div class='form-group'>

													<h4>3. Berapakah sisa jatah cuti di akhir periode cuti yang boleh digunakan di periode berikutnya (carry forward)?</h4>

												</div>

												<div class='form-group'>

													<div class="col-md-8">

														<div class="col-md-3"><h4>Jumlah Max </h4></div>

														<div class="col-md-5" style="width: 65%">

															<select class="form-control" name="tanggal" required="">

															<option value="">Berapa hari :</option>

															<?php for($tgl=1;$tgl<=12;$tgl++){?>

															<option value="<?php echo $tgl;?>"><?php echo $tgl;?></option>

															<?php } ?>

															</select>

														</div>

													</div>

												</div>

												<div class='form-group'>

													<div class="col-xs-12">

													<div class="col-md-8">

														<div class="col-md-3"><h4>Berlaku sampai</h4></div>

														<div class="col-md-5" style="width: 65%">

															<select class="form-control" name="bulan" required="">

																<option value="">Pilih Bulan</option>

																<option value="01">Januari</option>

																<option value="02">Februari</option>

																<option value="03">Maret</option>

																<option value="04">April</option>

																<option value="05">Mei</option>

																<option value="06">Juni</option>

																<option value="07">Juli</option>

																<option value="08">Agustus</option>

																<option value="09">September</option>

																<option value="10">Oktober</option>

																<option value="11">November</option>

																<option value="12">Desember</option>

															</select>

														</div>

													</div>

													<div class="col-md-4">

														<h5>Bulan sejak periode berikutnya dimulai</h5>

													</div>

												</div>

													

												</div>

											</div>

										<div class='modal-footer'>

											<button type='submit' class='btn btn-sm btn-primary'>

												<span class='glyphicon glyphicon-ok-sign'></span> Update

											</button>

										</div>

									</form>

									</div>

									<!-- /.modal-content -->

								</div>

							</div>

							</div>

						</div>

					</div>

				</div>

			</div>

            <div class="background-overlay"></div>

        </section>