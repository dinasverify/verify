    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom_css/radio_checkbox.css">
		<!-- Content Header (Page header) -->
		<section class="content-header">
           <h1>Pengaturan Bagian & Pekerjaan</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php base_url('dashboard');?>">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li> <a href="<?php base_url('pengaturan');?>">Pengaturan</a></li>
				 <li> <a href="<?php base_url('pengaturan/bagian');?>">Gaji & LTHR</a></li>
             </ol>
		</section>
		<!-- Main content -->
        <section class="content p-l-r-15">
            <div class="row">			
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
							<h4 class="panel-title">
                                <i class="ti-briefcase"></i> Pengaturan Gaji & LTHR
                            </h4>	
                        </div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12">
									<div class="col-md-10">
										<h4>1. Komponen pendapatan apa saja yang tersedia di perusahaan Anda (selain tunjangan PPh 21 dan BPJS)?</h4>
									</div>
									<div class="col-md-2">
										<div class="pull-right">
											<button type="button" id="myBtn1" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button>
										</div>
									</div>
								</div>
							</div></br>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="pull-right">
										<a href="<?php echo site_url('pengaturan/tambahkomponen_pend');?>"><button class="ladda-button btn btn-info btn-sm button_normal" data-style="Nama Pendapatan" >
											<i class="fa ti-plus" aria-hidden="true"></i> Nama Pendapatan
										</button></a>
									</div>
								</div>
							</div></br>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="table-responsive">
										<table class="table table-striped" id="table">
											<thead style="background:#e9f0d8">
												<tr class="filters">
													<th style="width:2%">No</th>
													<th style="width:20%">Nama Pendapatan</th>
													<th style="width:15%">Tipe</th>
													<th style="width:10%">Nilai</th>
													<th style="width:5%">PPh 21</th>
													<th style="width:10%">Tipe A1</th>
													<th style="width:10%;text-align:center">Status</th>
													<th style="width:5%;text-align:center">Aksi</th>
												</tr>
											</thead>
											<tbody>
											<?php
												if(sizeof($data_set_sal)>0){
													$no = 1;
													for($i=0;$i<sizeof($data_set_sal);$i++){
														$id_pen 	= $data_set_sal[$i]['id_kom_pend'];
														$nama_pen 	= $data_set_sal[$i]['nama_pendapatan'];
														$tp 		= $data_set_sal[$i]['tipe_pendapatan'];
														if($tp == 0){$tipe ="Tidak Ada";}elseif($tp == 7){$tipe = "Gaji Pokok";}elseif($tp == 10){$tipe="Tergantung Kehadiran";}
														$nilai 		= $data_set_sal[$i]['nilai'];
														$pph 		= $data_set_sal[$i]['is_pph21'];
														if($pph==1){$pph21="Ya";}else{$pph21="Tidak";}
														$st 		= $data_set_sal[$i]['status'];
														if($st==1){$cek="checked";}else{$cek="";}
														//$setting = $data_set_sal[$i]['setting'];
														//if($setting==1){$set="<a href='#' class='btn btn-default btn-sm'>Default</a>";}else{$set="<a href='#'><i class='fa fa-fw ti-pencil text-default actions_icon' data-toggle='tooltip' data-placement='top' title data-original-title='Edit'></i></a>&nbsp;&nbsp;
														//<a href='#' data-toggle='modal' data-target='#delete'><i class='fa fa-fw ti-trash text-default actions_icon' data-toggle='tooltip' data-placement='top' title data-original-title='Delete'></i></a>";}
														$tipea1 	= $data_set_sal[$i]['tipe_a1'];
														echo "<tr>
																<td>".$no++."</td>
																<td>".$nama_pen."</td>
																<td>".$tipe."</td><td>".number_format($nilai)."</td><td>".$pph21."</td>
																<td>".$tipea1."</td><td style='text-align:center'>
																	<input type='checkbox' id='toggle_real' name='my-checkbox' data-size='small' ".$cek." readonly>
																</td>
																<td>
																	<a href='".site_url('pengaturan/edittunjangan/'.$id_pen)."'><i class='fa fa-fw ti-pencil actions_icon' data-toggle='tooltip' data-placement='top' title data-original-title='Edit'></i></a>
																	<a href='".site_url('pengaturan/hapustunjangan/'.$id_pen)."'><i class='fa fa-fw ti-trash actions_icon' data-toggle='tooltip' data-placement='top' title data-original-title='Hapus'></i></a>
																</td>
															</tr>";
													}
												}
											?>
											</tbody>
										</table>
									</div>
									<div id="edittunjangan" class="modal fade">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close"
																data-dismiss="modal">&times;</button>
														<h4 class="modal-title">Tambah Nama Potongan</h4>
													</div>
													<div class="modal-body">
														<form class="form-horizontal" id="form_add_pot" method="POST" action="">
																<table>
																	<tr>
																		<td style=" width: 80px;">Nama Pendapatan</td>
																		<td style=" width: 50px;"></td>
																		<td style=" width: 375px;"><input type="text" class="form-control" id="nampot" name="nampot" value="<?php echo $nama_pen;?>" required></td>
																	</tr>
																	<tr>
																</table>
															</form>
													</div>
													<div class="modal-footer ">
														<a href="deleted_users.html" class="btn btn-danger">
															<span class="glyphicon glyphicon-ok-sign"></span> Yes
														</a>
														<a href="deleted_users.html"  class="btn btn-success" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>No</a>
													</div>
												</div>
											</div>
										</div>
									</div>
							</div></br>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="col-md-10 col-md-offset-1 noteInfo">
										<div class="table-responsive">
											<table class="table table-bordered">
												<thead style="background:#34586e;color:white">
													<tr>
														<th>
															Catatan : Bagi personalia yang baru masuk atau berhenti bekerja di tengah periode penggajian, perhitungan gaji pokok dan tunjangan tetap proposional dihitung berdasarkan jumlah hari kalender yang dilalui personalia tersebut dibagi dengan total hari kalender dalam suatu periode penggajian.
														</th>
													</tr>
												</thead>
											</table>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="col-md-10">
										<h4>2. Komponen potongan apa saja yang ada di perusahaan Anda?</h4> <!-- (selain potongan PPh 21, Premi BPJS dan Unpaid Leave)-->
									</div>
									<div class="col-md-2">
										<div class="pull-right">
											<button type="button" id="myBtn2" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button>
										</div>
									</div>
								</div>
							</div></br>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="pull-right">
										<a href="<?php echo site_url('pengaturan/tambahkomponen_pot');?>"><button class="ladda-button btn btn-info btn-sm button_normal" data-toggle="modal" data-target="#modaladd_pot" data-style="Nama Potongan">
											<i class="fa ti-plus" aria-hidden="true"></i> Nama Potongan
										</button></a>
									</div>
								</div>
							</div></br>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="table-responsive">
										<table class="table table-striped" id="table">
											<thead style="background:#e9f0d8">
												<tr class="filters">
													<th style="width:2%">No</th>
													<th style="width:25%">Nama Potongan</th>
													<th>Tipe</th>
													<th style="width:20%;text-align:center">Nilai</th>
													<th style="width:20%;text-align:center">Status</th>
													<th style="width:20%;text-align:center">Aksi</th>
												</tr>
											</thead>
											<tbody>
											<?php
												if(sizeof($data_kom_pot)>0){
													$no = 1;
													for($i=0;$i<sizeof($data_kom_pot);$i++){
														$id_kom 	= $data_kom_pot[$i]['id_kom_pot'];
														$nama_pot 	= $data_kom_pot[$i]['nama_potongan'];
														$tipe_pot 	= $data_kom_pot[$i]['tipe_potongan'];
														$nilai 		= $data_kom_pot[$i]['nilai'];
														$sta_pot 	= $data_kom_pot[$i]['status'];
														if($sta_pot==1){$cek="checked";}else{$cek="";}
														
														echo "<tr>
																<td>".$no++."</td>
																<td>".$nama_pot."</td>
																<td>".$tipe_pot."</td>
																<td style='text-align:center'>".number_format($nilai)."</td>
																<td style='text-align:center'>
																	<input type='checkbox' id='toggle_real' name='my-checkbox' data-size='small' ".$cek.">
																</td>
																<td style='text-align:center'>
																	<a href=".site_url('pengaturan/editpotongan/'.$id_kom)."><i class='fa fa-fw ti-pencil text-default actions_icon' data-toggle='tooltip' data-placement='top' title data-original-title='Edit'></i></a>&nbsp;&nbsp;
																	<a href=".site_url('pengaturan/hapuspotongan/'.$id_kom)."><i class='fa fa-fw ti-trash text-default actions_icon' data-toggle='tooltip' data-placement='top' title data-original-title='Delete'></i></a>
																</td>
															</tr>";
													}
												}
											?>
												<!--<tr>
													<td>Potongan Lainnya</td>
													<td>Manual</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>-->
											</tbody>
										</table>
									</div>
								</div>
							</div></br>
							</br>
							<?php 
								if(sizeof($get_aturangaji)>0){
									
									for($i=0;$i<sizeof($get_aturangaji);$i++){
										$id_ref 		= $get_aturangaji[$i]['id_referensi'];
										$nm_ref 		= $get_aturangaji[$i]['nama_ref'];
										$parentcode	 	= $get_aturangaji[$i]['parent_code'];
										$value		 	= $get_aturangaji[$i]['value'];
										$status		 	= $get_aturangaji[$i]['status'];
										
										//$no3 = $this->input->post("radios3"); //15
										//$nilaiuanglembur = $this->input->post("nilaiuanglembur"); //24
										//$nilai = $this->input->post("nilai"); //17
										//$no6 = $this->input->post("radios6"); //18
										//$no7 = $this->input->post("radios7"); //19
										//$no8 = $this->input->post("radios8"); //20
										
										$st 			= $get_aturangaji[0]['status'];
										if($st == 0){ $status="Tidak";}else{$status="Ya";}
										$nominal 		= $get_aturangaji[8]['value'];
										$mindapetthr	= $get_aturangaji[2]['value'];
										$besaranthr		= $get_aturangaji[3]['value'];
										$thrminkurangthn = $get_aturangaji[4]['value'];
										$rubahkarir		 = $get_aturangaji[5]['value'];
									}
								}
								?>
							<table id="user" class="table table-striped">
                                <tbody>
								<tr>
                                    <td class="table_simple"></td>
                                    <td><a href="<?php echo site_url('pengaturan/editaturangaji');?>"><button class="ladda-button btn btn-info btn-sm button_normal pull-right">
											<i class="fa fa-edit" aria-hidden="true"></i> Perubahan Aturan
										</button></a></td>
                                </tr>
                                <tr>
                                    <td class="table_simple"><h4>3. Apakah perusahaan Anda menerapkan penghitungan lembur? </h4></td>
                                    <td><button  type="button" id="myBtn4" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button> </td>
                                </tr>
                                <tr>
                                    <td  colspan='2'><i class="fa fa-check"></i><?php echo " ".$status;?></td>
								</tr>
                                <tr>
									<td style="background:#34586e;color:white">
										Catatan : </br>Lompati pertanyaan no. 4 jika Anda memilih 'Tidak' pada pertanyaan no. 3.
									</td><td></td>
                                </tr>
                                <tr>
                                    <td><h4>4. Pilih formula yang Anda gunakan untuk menghitung uang lembur:</h4></td>
									<td><button  type="button" id="myBtn4" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button> </td>
                                </tr>
                                <tr>
									<td colspan="2"><i class="fa fa-check"> Nominal Jumlah Rupiah Per Jam: <?php echo number_format($nominal);?></i></td>
								</tr>
                                <tr>
                                    <td><h4>5. Agar menerima Tunjangan Hari Raya (THR), berapa lama seorang personalia harus melalui masa kerja perusahaan Anda? </h4></td>
									<td><button  type="button" id="myBtn4" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button> </td>
								</tr>
								<tr>
									<td colspan="2"><i class="fa fa-check"></i><?php echo " ".$mindapetthr; ?> Bulan</td>
								</tr>
                                <tr>
									<td><h4>6. Berapa besar THR yang Anda bayarkan untuk tiap personalia di perusahaan Anda? </h4></td>
									<td><button  type="button" id="myBtn4" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button> </td>
								</tr>  
								<tr>
									<td colspan="2"><i class="fa fa-check"></i><?php 
										if($besaranthr == 26){
											echo " Gaji Pokok";
										}elseif($besaranthr == 27){
											echo " Gaji Pokok + Tunjangan Tetap";
										}
									?></td>
								</tr>								
								<tr>
									<td><h4>7. Untuk personalia yang bekerja kurang dari satu tahun, bagaimanakah cara Anda menentukan besaran THR untuknya?</h4></td>
									<td><button  type="button" id="myBtn4" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button> </td>
								</tr>
								<tr>
									<td colspan="2"><i class="fa fa-check"></i>
										<?php 
										if($thrminkurangthn == 29)
										{
											echo "Berdasarkan Jumlah Hari Kalender: Aantara Tanggal Ia Mulai Bekerja Hingga Tanggal Pembuatan THR Dibagi 365 Hari";
										}elseif($thrminkurangthn == 30){
											echo "Berdasarkan jumlah bulan: antara bulan ia mulai bekerja dengan bulan pembuatan";
										}
									?>
									</td>
								</tr>
								<tr>
									<td><h4>8. Bagaimanakah kebijakan perusahaan untuk menentukan besaran gaji bagi personalia yang mengalami perubahan karir (contoh: promosi) di tengah periode penggajian?</h4></td>
									<td><button  type="button" id="myBtn4" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button> </td>
								</tr>
								<tr>
                                   <td colspan="2"><i class="fa fa-check"></i>
								   <?php 
										if($rubahkarir == 32){
											echo "Proporsional. Menggabungkan perhitungan pro rata gaji lama dan gaji baru";
										}elseif($rubahkarir == 33){
											echo "Hanya nilai gaji baru saja yang berlaku";
										}
									?>
								   </td>
                                </tr>
                                </tbody>
                            </table>	
						</div>
					</div>
				</div>
			</div>
            <!-- row-->
        </section>
		   <div class="background-overlay"></div>
        <!-- /.content --> 
	</aside>
	<!--Modal Point 3-->
	<div id="modal3" class="modal fade animated" role="dialog">
		<div class="modal-dialog">
		<form class="form-horizontal" action="<?php echo base_url('pengaturan/changesetlembur');?>" method="post">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close"
								data-dismiss="modal">&times;</button>
						<h4 class="modal-title">3. Apakah perusahaan Anda menerapkan penghitungan lembur? </h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group has-pretty-child">
									<div class="clearfix prettyradio labelright margin-right blue">
									<input type="radio" name="radios" value="option1" class="square-blue" checked>
									<label name="yes" for="Test3_0">Yes</label></div>
									<div class="clearfix prettyradio labelright margin-right blue">
									<input type="radio" name="radios" value="option2" class="square-blue" >
									<label name="no" for="Test3_1">No</label></div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="padding-leftRight">
									<button type="button" class="btn btn-warning btn-gadjian btn-sm simpan" id="simpanTerapHtgLembur" data-dismiss="modal" name="submit">
										Simpan</button>
									<button type="button" class="btn btn-danger btn-gadjian btn-sm simpan" id="batalTerapHtgLembur" data-dismiss="modal">
										Batal			  											</button>
								</div>
							</div>
						</div>
					</div>
				</div>
		</form>
		</div>
	</div>
	<!--RADIO BUTTON-->
	<script src="<?php echo $themes_url; ?>js/custom_js/radio_checkbox.js"></script>

	<!--Modal Tambah Nama Pendapatan-->
	<div id="modaladd_pend" class="modal fade animated" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close"
							data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Tambah Nama Pendapatan</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" id="form_add_pend" method="POST" action="<?php echo base_url('pengaturan/tambahpenggajian')?>">
						<table>
							<tr>
								<td  style=" width: 80px;"><label class="control-label col-sm-2" for="np">Nama Pendapatan</label></td><td style=" width: 80px;"></td>
								<td style=" width: 400px;"><input type="text" class="form-control" id="nama_pend" name="nama_pend" required></td>
							</tr>
							<tr>
							<td>
								<label class="control-label col-sm-2" for="np">Tipe</label></td><td style=" width: 80px;"></td>
								<td style=" width: 400px;"><input type="text" class="form-control" id="tipe_pend" name="tipe_pend" required></td>
							</tr>
							<tr>
							<td>
								<label class="control-label col-sm-2" for="np">PPh 21</label></td><td style=" width: 80px;"></td>
								<td style=" width: 400px;">
									<div class="selectric-wrapper selectric-form-control"><div class="selectric-hide-select">
									<select id="dynamic" class="form-control" tabindex="-1">
                                                <option value="0">Ya</option>
                                                <option value="1">Tidak</option>
									</select></div>
									</div>
								</td>
							</tr>
							<tr>
							<td>
								<label class="control-label col-sm-2" for="np">Tipe A1</label></td><td style=" width: 80px;"></td>
								<td style=" width: 400px;"><input type="text" class="form-control" id="tipea1" name="tipea1" required></td>
							</tr>
							<tr>
							<td  style=" width: 80px;">
								<label class="control-label col-sm-2" for="np">Status</label>
							</td><td style=" width: 80px;"></td>
								<td style=" width: 400px;">
									<div class="selectric-wrapper selectric-form-control"><div class="selectric-hide-select">
									<select id="status_pen" class="form-control" tabindex="-1">
                                                <option value="0">Ya</option>
                                                <option value="1">Tidak</option>
									</select></div></div>
								</td>
							</tr><br>
							<tr>
								<td>
									<label class="control-label col-sm-2" for="npc">Setting</label>
								</td>
								<td style=" width: 80px;"></td><br>
									<td style=" width: 400px;">
										<div class="selectric-wrapper selectric-form-control">
											<div class="selectric-hide-select">
												<select id="setting_sel" class="form-control" tabindex="-1">
															<option value="0">Ya</option>
															<option value="1">Tidak</option>
												</select>
											</div>
										</div>
									</td>
							</tr>
						</table>
					</form>
				</div>
				<div class="modal-footer ">
					<a href="deleted_users.html" class="btn btn-danger">
						<span class="glyphicon glyphicon-ok-sign"></span> Yes
					</a>
					<a href="deleted_users.html"  class="btn btn-success" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>No</a>
				</div>
			</div>
		</div>
	</div>

	<!-- Select2 -->
	<script src="<?php echo $themes_url; ?>vendors/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/select2/js/select2.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/selectize/js/standalone/selectize.min.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/selectric/js/jquery.selectric.min.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>js/custom_js/custom_elements.js" type="text/javascript"></script>
	
	<!-- DataTable -->
	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/datatables_custom.js"></script>
	
