	<link href="<?php echo $themes_url; ?>vendors/daterangepicker/css/daterangepicker.css" rel="stylesheet" type="text/css"/>

    <link href="<?php echo $themes_url; ?>vendors/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>

    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/datedropper/datedropper.css">

    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/timedropper/css/timedropper.css">

    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/jquerydaterangepicker/css/daterangepicker.min.css">
    			<!--smoott-->   
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">
    <link href="<?php echo $themes_url; ?>vendors/hover/css/hover-min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $themes_url; ?>vendors/laddabootstrap/css/ladda-themeless.min.css">
    <link href="<?php echo $themes_url; ?>css/buttons_sass.css" rel="stylesheet">
    <link href="<?php echo $themes_url; ?>css/advbuttons.css" rel="stylesheet">

		<!-- Content Header (Page header) -->

		<section class="content-header">

			<h1>Pengajuan Izin</h1>

            <ol class="breadcrumb">

                <li>

                    <a href="<?php site_url('dashboard');?>">

                        <i class="fa fa-fw ti-home"></i> Dashboard

                    </a>

                </li>

                <li> <a href="<?php site_url('sakit');?>">Sakit & Izin</a></li>

				<li> <a href="<?php site_url('sakit/pengajuanizin');?>">Pengajuan Izin</a></li>

				<li> Tambah Pengajuan Izin Lainnya </li>

            </ol>

		</section>

		<!-- Main content -->

        <section class="content p-l-r-15">

            <div class="row">			

                <div class="col-md-12">

                    <div class="panel">

                        <div class="panel-heading">

							<h4 class="panel-title">

                                <i class="ti-support"></i> Tambah Pengajuan Izin Lainnya

                            </h4>	
                             <span class="pull-right">
	                                <i class="fa fa-fw ti-angle-up clickable"></i>
	                                <i class="fa  fa-fw ti-close removepanel clickable"></i>
                            </span>

                        </div>

						<?php

							if(sizeof($data_izin_id)>0){

								$id	 	= $data_izin_id[0]['id_referensi'];

								$nm_izin = $data_izin_id[0]['nama_ref'];

								$mak_hari = $data_izin_id[0]['value'];

								

							}

						?>

						<div class="panel-body">

							<div class="col-xs-12">

								<form class="form-horizontal" action="<?php echo base_url('pengaturan/edit_izin_pengaturan/'.$id);?>" method="post">

									<div class="form-group">

										<label for="" class="control-label col-md-2">Nama Jenis Izin</label>

										<div class="col-md-10">

											<input type='text' class='form-control' name='nama_jenis' id='nama_jenis'  value="<?php echo $nm_izin;?>" required="">

										</div>

									</div>

									<div class="form-group">

										<label for="" class="control-label col-md-2">Jenis Izin</label>

										<div class="col-md-10">

											<select name="status" id="status" class="form-control" required="">

												<option value="">Pilih Status</option>

												<option value="1">Aktif</option>

												<option value="0">Non Aktif</option>

											</select>

										</div>

									</div>

									<div class="form-group">

										<label for="" class="control-label col-md-2">Maksimal Hari</label>

										<div class="col-md-10">

											<input type='text' class='form-control' name='max_hari' id='max_hari'  value="<?php echo $mak_hari;?>" required="">

										</div>

									</div>

									<div class="row">

										<hr>

									</div>

									<div class="form-group">

										<div class="col-md-12">

											<div class="pull-right">

												<button name="submit" type="submit" class="btn btn-sm btn-labeled btn-primary" name="simpan" value="simpan">

													<span class="btn-label">

														<i class="ti-save"></i>

													</span> Simpan

												</button>

												<button type="button" class="btn btn-sm btn-labeled btn-danger"  onClick="document.location='<?php echo site_url('pengaturan/izin'); ?>'">

													<span class="btn-label">

														<span class="glyphicon glyphicon-remove"></span>

													</span> Batal

												</button>

											</div>

										</div>

									</div>

								</form>

							</div>

						</div>

						

					</div>

				</div>

			</div>

			<div class="background-overlay"></div>

        </section>

	<!-- airdatepicker -->

	<script src="<?php echo $themes_url; ?>vendors/moment/js/moment.min.js" type="text/javascript"></script>	

    <script src="<?php echo $themes_url; ?>vendors/datetime/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.min.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.en.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>js/custom_js/advanceddate_pickers.js"></script>

	

	<!-- Select2 -->

	<script src="<?php echo $themes_url; ?>vendors/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/select2/js/select2.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/selectize/js/standalone/selectize.min.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/selectric/js/jquery.selectric.min.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>js/custom_js/custom_elements.js" type="text/javascript"></script>

	

	<!-- bootstrap time picker -->

	<script src="<?php echo $themes_url; ?>vendors/clockpicker/js/bootstrap-clockpicker.min.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/datedropper/datedropper.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/timedropper/js/timedropper.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>js/custom_js/datepickers.js" type="text/javascript"></script> 