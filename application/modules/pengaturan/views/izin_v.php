				<!--smoott-->   
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">
    <link href="<?php echo $themes_url; ?>vendors/hover/css/hover-min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $themes_url; ?>vendors/laddabootstrap/css/ladda-themeless.min.css">
    <link href="<?php echo $themes_url; ?>css/buttons_sass.css" rel="stylesheet">
    <link href="<?php echo $themes_url; ?>css/advbuttons.css" rel="stylesheet">
		<!-- Content Header (Page header) -->

		<section class="content-header">

           <h1>Pengaturan Izin</h1>

            <ol class="breadcrumb">

                <li>

                    <a href="<?php base_url('dashboard');?>">

                        <i class="fa fa-fw ti-home"></i> Dashboard

                    </a>

                </li>

                <li> <a href="<?php base_url('pengaturan');?>">Pengaturan</a></li>

				 <li> <a href="<?php base_url('pengaturan/izin');?>">Izin</a></li>

             </ol>

		</section>

		<!-- Main content -->

        <section class="content p-l-r-15">

            <div class="row">			

                <div class="col-md-12">
                	<?php echo $this->session->flashdata('pesan'); ?>
                    <div class="panel">

                        <div class="panel-heading">

							<h4 class="panel-title">

                                <i class="ti-briefcase"></i> Pengaturan Izin

                            </h4>	
                             <span class="pull-right">
	                                <i class="fa fa-fw ti-angle-up clickable"></i>
	                                <i class="fa  fa-fw ti-close removepanel clickable"></i>
                            </span>

                        </div>

						<div class="panel-body">

							<div class="row">

								<div class="col-xs-12">

									<div class="row">

										<div class="col-md-10">

											<h4>1. Izin khusus apa saja yang berlaku di perusahaan Anda?</h4>

										</div>

										<div class="col-md-2">

											<div class="pull-right">

												<button type="button" id="myBtn1" class="btn btn-sm btn-default"  data-target="#info" data-toggle="modal"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>

											</div>

										</div>

									</div>

								</div>

							</div>

							

							<div class="row">

								<div class="col-xs-12">

									<div class="pull-right">

										<button class="btn btn-primary btn-sm" type="button"  data-toggle="modal" data-target="#tambahJenis" data-animate-modal="fadeIn" data-modalcolor="#34586e">

											<i class="fa ti-plus" aria-hidden="true"></i>Tambah Jenis Izin Lainnya

										</button>

									</div>

								</div>

							</div></br>

							

							<div class="row">

								<div class="col-xs-12">

									<div class="table-responsive">

										<table class="table table-striped" id="sample_1">

											<thead style="background:#F3F7F9">

												<tr class="filters">

													<th>No</th>

													<th style="width:20%">Jenis Izin Lainnya</th>

													<th style="width:20%">Maks Hari / Pengajuan</th>

													<th style="width:10%;text-align:center">Status</th>

													<th style="width:20%;text-align:center">Aksi</th>

												</tr>

											</thead>

											<tbody>

											<?php if(sizeof($data_izin)>0){

												for($i=0;$i<sizeof($data_izin);$i++){

													$id = $data_izin[$i]['id_referensi'];

													$kode_ref = $data_izin[$i]['kode_ref'];

													$nm_izin = $data_izin[$i]['nama_ref'];

													$value = $data_izin[$i]['value'];

													$status = $data_izin[$i]['status'];

													if($status == '1'){$st = 'checked';}elseif($status == '0'){$st = '';}

													echo "<tr><td style='width:5%;'>".$kode_ref."</td><td>".$nm_izin."</td><td>".$value."</td>

														<td align='center'>

															<input type='checkbox' id='toggle_real' name='my-checkbox' data-size='small' ".$st.">

														</td>

														<td align='center'>

															<a class='btn-xs btn-warning' href='edit_izin_pengaturan/".$id."'>

															<i class='fa fa-fw ti-pencil text-default  actions_icon'  data-toggle='tooltip' data-placement='top' data-placement='top' title data-original-title='Edit'>

															</i>

															</a>
															&nbsp;&nbsp;
															<a class='btn-xs btn-danger' href='deleteizin/".$id."'>

															<i class='fa fa-fw ti-trash text-default  actions_icon' data-placement='top' data-toggle='tooltip' data-placement='top'  data-original-title='Hapus'>

															</i>

															</a>

														</td>

														</tr>

													";

												}

											}?>

											</tbody>

										</table>

									</div>

								</div>

							</div></br>

							<!--modal add -->	

							<div id="tambahJenis" class="modal fade animated" role="dialog">

								<div class="modal-dialog modal-lg">

									<div class="modal-content">

										<div class="modal-header">

											<button type="button" class="close"data-dismiss="modal">&times;</button>

											<h4 class="modal-title">Tambah Jenis Izin Lainnya</h4>

										</div>

										<div class="modal-body">

											<div class="row">

												<div class="col-xs-12">

													<form class="form-horizontal" id="form_add_kalender" method="POST" action="<?php echo base_url('pengaturan/addperaturanizin');?>">
													

														
														<div class="form-group">

															<label for="" class="control-label col-md-3">Nama Jenis Izin</label>

															<div class="col-md-8">

																<input class="form-control" type="text" name="nama_jenis_izin" id="nama_jenis_izin" placeholder="Nama Jenis Izin" required="">

															</div>

														</div>

														<div class="form-group">

															<label for="" class="control-label col-md-3">Maksimal Pengajuan</label>

															<div class="col-md-8">

																<input class="form-control" type="text" name="maks_pengajuan" id="maks_pengajuan" placeholder="Jumlah Hari" required="">

															</div>

														</div>

														<div class="form-group">

															<label for="" class="control-label col-md-3">Status</label>

															<div class="col-md-8">

																<select class="form-control" name="status" id="">

																	<option value="">Silahkan Pilih</option>

																	<option value="1">Aktif</option>

																	<option value="0">Non Aktif</option>

																</select>

															</div>

														</div>

														<hr>

														<div class="row">

															<div class="col-md-11">

																<div class="pull-right">

																	<button type="submit" class="btn btn-sm btn-labeled btn-primary" >

																		<span class="btn-label">

																			<i class="ti-save"></i>

																		</span> Simpan

																	</button>

																	<button type="button" class="btn btn-sm btn-labeled btn-danger"  data-dismiss="modal">

																		<span class="btn-label">

																			<i class="glyphicon glyphicon-remove"></i>

																		</span> Batal

																	</button>

																</div>

															</div>

														</div>

													</form>

												</div>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

            <!-- row-->

        </section>

		   <div class="background-overlay"></div>

        <!-- /.content --> 
	<script> 
		$('#notifications').slideDown('slow').delay(3500).slideUp('slow');
	</script>
	<script>

	    $(document).ready(function() {

	        // Untuk sunting

	        $('#edit-data').on('show.bs.modal', function (event) {

	            var div = $(event.relatedTarget); // Tombol dimana modal di tampilkan

	            var modal          = $(this);

	            // Isi nilai pada field

	            modal.find('#id_izin').attr("value",div.data('id_referensi'));

				modal.find('#status').attr("value",div.data('status'));

	            modal.find('#maks_pengajuan').attr("value",div.data('value'));

				

	        });

	    });

	</script>



	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/sweetalert2/js/sweetalert2.min.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/sweetalert.js"></script>



	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/jquery.dataTables.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/dataTables.bootstrap.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/datatables_custom.js"></script>

	<!-- Sweet Alert -->

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/sweetalert2/js/sweetalert2.min.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/sweetalert.js"></script>