		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>Kalender</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php base_url('dashboard');?>">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li> <a href="<?php base_url('pengaturan');?>">Pengaturan</a></li>
				<li> Kalender </li>
            </ol>
		</section>
		
		<!-- Main content -->
        <section class="content p-l-r-15">
            <div class="row">			
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
							<h4 class="panel-title">
                                <i class="ti-calendar"></i> Kalender
                            </h4>	
                        </div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12">
									<div class="row">
										<div class="col-md-10">
											<h4><p>1. Silakan edit kalender kerja berikut sesuai dengan kebijakan perusahaan Anda. Pengaturan kalender ini akan mempengaruhi catatan kehadiran. Anda dapat mengubah pengaturan ini kapan saja.</p></h4>
										</div>
										<div class="col-md-2">
											<div class="pull-right">
												<button class="btn btn-sm btn-default" type="button" id="myBtn" class="btn btn-sm btn-info btn-informasi pull-right" data-target="#info" data-toggle="modal"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>
											</div>
										</div>
									</div>
								</div>
							</div></br></br>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="row">
										<div class="col-md-6 pull-left">
											<form class="form-horizontal" action="" method="get">
												<div class="form-group">
													<label for="input-text" class="col-md-2 control-label">Tahun</label>
													<div class="col-md-8">
														<select name="tahun" class="form-control">
															<option value="2017">2017</option>
															<option value="2018">2018</option>
														</select>
													</div>
												</div>
											</form>
										</div>
										
										<div class="col-md-6">
											<div class="pull-right">
												<a href="" class="ladda-button btn btn-info btn-sm button_normal"><i class="fa ti-plus" aria-hidden="true"></i> Tambah Event</a>
												<a href="<?php echo site_url('pengaturan/versikalender');?>" class="btn btn-default btn-sm"><i class="fa ti-calendar" aria-hidden="true"></i> Lihat Kalender</a>
											</div>
										</div>
									</div>
								</div>
							</div></br>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="table-responsive">
										<table class="table table-striped">
											<thead style="background:#e9f0d8">
												<tr class="filters">
													<th style="width:20%">Tanggal</th>
													<th>Nama Event</th>
													<th>Tipe</th>
													<th style="text-align:center">Aksi</th>
												</tr>
											</thead>
											<tbody style="height:150px">
												<tr>
													<td>1 Jan 2017</td>
													<td>Tahun Baru 2017 Masehi</td>
													<td>Libur Nasional</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>28 Jan 2017</td>
													<td>Tahun Baru Imlek 2568 Kongzili</td>
													<td>Libur Nasional</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>28 Mar 2017</td>
													<td>Hari Raya Nyepi Tahun Baru Saka 1939</td>
													<td>Libur Nasional</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>14 Apr 2017</td>
													<td>Wafat Isa Al Masih</td>
													<td>Libur Nasional</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>24 Apr 2017</td>
													<td>Isra Mi'Raj Nabi Muhammad SAW</td>
													<td>Libur Nasional</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>1 Mei 2017</td>
													<td>Hari Buruh Internasional</td>
													<td>Libur Nasional</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Edit"></i></a>&nbsp;&nbsp
														<a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>11 Mei 2017</td>
													<td>Hari Raya Waisak 2561</td>
													<td>Libur Nasional</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>25 Mei 2017</td>
													<td>Kenaikan Isa Al Masih</td>
													<td>Libur Nasional</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>1 Jun 2017</td>
													<td>Hari Lahir Pancasila</td>
													<td>Libur Nasional</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>25 Jun 2017</td>
													<td>Hari Raya Idul Fitri 1438 H</td>
													<td>Libur Nasional</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>26 Jun 2017</td>
													<td>Hari Raya Idul Fitri 1438 H</td>
													<td>Libur Nasional</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>17 Agt 2017</td>
													<td>Hari Kemerdekaan Republik Indonesia</td>
													<td>Libur Nasional</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>1 Sep 2017</td>
													<td>Hari Raya Idul Adha 1438 H</td>
													<td>Libur Nasional</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>21 Sep 2017</td>
													<td>Tahun Baru Islam 1439 H</td>
													<td>Libur Nasional</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>1 Des 2017</td>
													<td>Maulid Nabi Muhammad SAW</td>
													<td>Libur Nasional</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>25 Des 2017</td>
													<td>Hari Raya Natal</td>
													<td>Libur Nasional</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div></br>
							
							<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="Heading" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
											</button>
											<h4 class="modal-title custom_align" id="Heading">Delete User</h4>
										</div>
										<div class="modal-body">
											<div class="alert alert-warning">
												<span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to
												delete this Account?
											</div>
										</div>
										<div class="modal-footer ">
											<a href="deleted_users.html" class="btn btn-danger">
												<span class="glyphicon glyphicon-ok-sign"></span> Yes
											</a>
											<button type="button" class="btn btn-success" data-dismiss="modal">
												<span class="glyphicon glyphicon-remove"></span> No
											</button>
										</div>
									</div>
								</div>
							</div>
							
							<div id="info" class="modal fade animated" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>
											<p class="text-center">
												Secara default, kalender ini telah disesuaikan dengan kalender nasional. Silakan menghapus, menambah atau mengubah nama-nama event yang ada di dalam tabel ini sesuai dengan kebijakan perusahaan Anda.
											</p>
										</div>
									</div>
								</div>
							</div>
					
						</div>
					</div>
				</div>
			</div>
            <!-- row-->
        </section>
		   <div class="background-overlay"></div>
        <!-- /.content -->