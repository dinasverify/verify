	<link href="<?php echo $themes_url; ?>vendors/daterangepicker/css/daterangepicker.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $themes_url; ?>vendors/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/datedropper/datedropper.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/timedropper/css/timedropper.css">
    	<link href="<?php echo $themes_url; ?>css/buttons_sass.css" rel="stylesheet">
	<link href="<?php echo $themes_url; ?>css/advbuttons.css" rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/jquerydaterangepicker/css/daterangepicker.min.css"> -->
	<!--smoott-->   
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">
    
	
	    <!-- Content Header (Page header) -->
		<section class="content-header">
		<h1>Master Pangkat</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php site_url('dashboard');?>">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li> <a href="#">Pengaturan</a></li>
				<li> <a href="#">Master Pangkat</a></li>
			</ol>
			<hr>
		</section>
		<div class="col-md-12"><hr></div>
		<!-- Main content -->
        <section class="content p-l-r-15">
        	<div class="row">			
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
							<h4 class="panel-title">
                                <span class="ti-angle-double-down"></span> Master Pangkat
                            </h4>
                            <span class="pull-right">
								<i class="fa fa-fw clickable ti-angle-up"></i>
								<i class="fa  fa-fw ti-close removepanel clickable"></i>
                            </span>	
                        </div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12">
									<?php echo $this->session->flashdata('pesan'); ?>
								<form method="post" class="form-horizontal" enctype="multipart/form-data" role="form" action="<?php echo base_url('pengaturan/editpangkat?id='.$id);?>">
									<div class="form-group">
										<label for="" class="control-label col-md-2">Nama Pangkat</label>
										<div class="col-md-10">
											<input type="text" value="<?php echo $nama_p;?>" name="pangkat" class="form-control" required="" placeholder="Isikan nama pangkat yg akan digunakan.">
										</div>
									</div>
									<div class="form-group">
										<label for="" class="control-label col-md-2">Golongan</label>
										<div class="col-md-10">
											<input type="text" name="gol" 
											value="<?php echo $gol; ?>"  class="form-control" required="" placeholder="Isikan nama golongan.">
										</div>
									</div>
									<hr>
									<div class="form-group">
										<div class="col-md-12">
											<div class="pull-right">
												<button type="submit" class="btn btn-sm btn-labeled btn-primary" name="simpan" value="simpan">
													<span class="btn-label">
														<i class="ti-save"></i>
													</span> Update
												</button>
												<button type="button" class="btn btn-labeled btn-danger" onClick="document.location='<?php echo site_url('pengaturan/pangkat'); ?>'">

													<span class="btn-label">

														<i class="ti-close"></i>

													</span> Batal

												</button>
											</div>
										</div>
									</div>
								</form>
								</div>
							</div>
						</div>
				</div>
			</div>
			<div class="background-overlay"></div>
        </section>
	
	
	<script type="text/javascript"> 
		$('#notifications').slideDown('slow').delay(3500).slideUp('slow');
	</script>
	<!-- Select2 -->
	<script src="<?php echo $themes_url; ?>vendors/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/select2/js/select2.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/selectize/js/standalone/selectize.min.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/selectric/js/jquery.selectric.min.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>js/custom_js/custom_elements.js" type="text/javascript"></script>
	
	<!-- DATATABLE -->
	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/datatables_custom.js"></script>