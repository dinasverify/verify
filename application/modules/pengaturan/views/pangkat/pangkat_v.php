			<!--smoott-->   
	<link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">
	<link href="<?php echo $themes_url; ?>vendors/hover/css/hover-min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo $themes_url; ?>vendors/laddabootstrap/css/ladda-themeless.min.css">
	<link href="<?php echo $themes_url; ?>css/buttons_sass.css" rel="stylesheet">
	<link href="<?php echo $themes_url; ?>css/advbuttons.css" rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/jquerydaterangepicker/css/daterangepicker.min.css"> -->
	<!--smoott-->   
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">
    
	
	    <!-- Content Header (Page header) -->
		<section class="content-header">
		<h1>Master Pangkat</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php site_url('dashboard');?>">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li> <a href="#">Pengaturan</a></li>
				<li> <a href="#">Master Pangkat</a></li>
			</ol>
			<hr>
		</section>
		
		<!-- Main content -->
        <section class="content p-l-r-15">
        	<div class="row">			
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
							<h4 class="panel-title">
                                <span class="ti-angle-double-down"></span> Master Pangkat
                            </h4>
                            <span class="pull-right">
								<i class="fa fa-fw clickable ti-angle-up"></i>
								<i class="fa  fa-fw ti-close removepanel clickable"></i>
                            </span>	
                        </div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12">
									<?php echo $this->session->flashdata('pesan'); ?>
									
								<form method="post" class="form-horizontal" enctype="multipart/form-data" role="form" action="<?php echo base_url('pengaturan/insertpangkat');?>">
									<div class="form-group">
										<label for="" class="control-label col-md-2">Nama Pangkat</label>
										<div class="col-md-10">
											<input type="text" name="pangkat" class="form-control" required="" placeholder="Isikan nama pangkat yg akan digunakan.">
										</div>
									</div>
									<div class="form-group">
										<label for="" class="control-label col-md-2">Golongan</label>
										<div class="col-md-10">
											<input type="text" name="gol" class="form-control" required="" placeholder="Isikan nama golongan.">
										</div>
									</div>
									<hr>
									<div class="form-group">
										<div class="col-md-12">
											<div class="pull-right">
												<button type="submit" class="btn btn-sm btn-labeled btn-primary" name="simpan" value="simpan">
													<span class="btn-label">
														<i class="ti-save"></i>
													</span> Simpan
												</button>
											</div>
										</div>
									</div>
								</form>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									
									<div class="row">
										<div class="col-xs-12">
											<hr>		
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<div class="table-responsive">
												<table class="table table-striped" id="sample_1">
													<thead style="background:#F3F7F9">
														<tr style="text-align: center;" class="filters">
															<th>No</th>
															<th>Nama Pangkat</th>
															<th style="text-align: center;">Golongan/level</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
														<?php $no=1; foreach($pangkat as $data){?>
														<tr style="text-align: center;">
															<td><?php echo $no++; ?></td>
															<td><?php echo $data['nama_pangkat']?></td>
															<td><?php echo $data['golongan']?></td>
															<td><a href="<?php echo base_url('pengaturan/editpangkat?id='.$data['id_pangkat']);?>"><button name="edit" class="btn btn-sm btn-labeled btn-warning" type="button"><i class="ti-pencil"></i> Edit</button></a>
															<a href="<?php echo base_url('pengaturan/deletepangkat?id='.$data['id_pangkat']);?>"><button name="" class="btn btn-sm btn-labeled btn-danger" type="button"><span class="ti-trash"></span> Edit</button></a>
														</td>
														</tr>
														<?php } ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
					</div>
				</div>
			</div>
			<div class="background-overlay"></div>
        </section>
	
	
	<script type="text/javascript"> 
		$('#notifications').slideDown('slow').delay(3500).slideUp('slow');
		
		// $(document).ready(function(){
	 //        tampil_data_barang();   //pemanggilan fungsi tampil barang.
	                  
	 //        //fungsi tampil barang
	 //        function tampil_data_barang(){
	 //            $.ajax({
	 //                type  : 'ajax',
	 //                url   : '<?php echo base_url()?>pengaturan/data_pangkat',
	 //                async : false,
	 //                dataType : 'json',
	 //                success : function(result){
	 //                    var html = '';
	 //                    var i;
	 //                    for(i=0; i<result.length; i++){
	 //                        html += '<tr>'+
	 //                                '<td>'+result[i].id_pangkat+'</td>'+
	 //                                '<td>'+result[i].nama_pangkat+'</td>'+
	 //                                '<td>'+result[i].golongan+'</td><td>'+
	 //                                    '<a href="javascript:;" class="btn btn-info btn-xs item_edit" data="'+result[i].id_pangkat+'">Edit</a>'+' '+
	 //                                     '<a href="javascript:;" class="btn btn-danger btn-xs item_hapus" data="'+result[i].id_pangkat+'">Hapus</a>'+
	 //                                 '</td>'+
	 //                                '</tr>';
	 //                    }
	 //                    $('#show_data').html(html);
	 //                }
	 
	 //            });
	 //        }

	 //        $(document).on('click','#insert',function(){
	 //        	var namapangkat = $('#persyaratan').text();
	 //        	var gol 		= $('#golongan').text();
	        	
	 //        	console.log(gol);
	 //        });
	        //function insertData(){
	        	
	        	// $.ajax({
	        	// 	type : "POST",
	        	// 	data : "namaPangkat="+namapangkat+"&golongan="+$gol,
	        	// 	url  : '<?php echo base_url()?>pengaturan/insert_pangkat'
	        		

	        	// });
	        //}
	 
	        // //GET UPDATE
	        // $('#show_data').on('click','.item_edit',function(){
	        //     var id=$(this).attr('data');
	        //     $.ajax({
	        //         type : "GET",
	        //         url  : "<?php echo base_url('index.php/barang/get_barang')?>",
	        //         dataType : "JSON",
	        //         data : {id:id},
	        //         success: function(data){
	        //             $.each(data,function(barang_kode, barang_nama, barang_harga){
	        //                 $('#ModalaEdit').modal('show');
	        //                 $('[name="kobar_edit"]').val(data.barang_kode);
	        //                 $('[name="nabar_edit"]').val(data.barang_nama);
	        //                 $('[name="harga_edit"]').val(data.barang_harga);
	        //             });
	        //         }
	        //     });
	        //     return false;
	        // });
	 
	 
	        // //GET HAPUS
	        // $('#show_data').on('click','.item_hapus',function(){
	        //     var id=$(this).attr('data');
	        //     $('#ModalHapus').modal('show');
	        //     $('[name="kode"]').val(id);
	        // });
	 
	        //Simpan Barang
	        // $('#btn_simpan').on('click',function(){
	        //     var kobar=$('#kode_barang').val();
	        //     var nabar=$('#nama_barang').val();
	        //     var harga=$('#harga').val();
	        //     $.ajax({
	        //         type : "POST",
	        //         url  : "<?php echo base_url('index.php/barang/simpan_barang')?>",
	        //         dataType : "JSON",
	        //         data : {kobar:kobar , nabar:nabar, harga:harga},
	        //         success: function(data){
	        //             $('[name="kobar"]').val("");
	        //             $('[name="nabar"]').val("");
	        //             $('[name="harga"]').val("");
	        //             $('#ModalaAdd').modal('hide');
	        //             tampil_data_barang();
	        //         }
	        //     });
	        //     return false;
	        // });

	       
	 
	        // //Update Barang
	        // $('#btn_update').on('click',function(){
	        //     var kobar=$('#kode_barang2').val();
	        //     var nabar=$('#nama_barang2').val();
	        //     var harga=$('#harga2').val();
	        //     $.ajax({
	        //         type : "POST",
	        //         url  : "<?php echo base_url('index.php/barang/update_barang')?>",
	        //         dataType : "JSON",
	        //         data : {kobar:kobar , nabar:nabar, harga:harga},
	        //         success: function(data){
	        //             $('[name="kobar_edit"]').val("");
	        //             $('[name="nabar_edit"]').val("");
	        //             $('[name="harga_edit"]').val("");
	        //             $('#ModalaEdit').modal('hide');
	        //             tampil_data_barang();
	        //         }
	        //     });
	        //     return false;
	        // });
	 
	        // //Hapus Barang
	        // $('#btn_hapus').on('click',function(){
	        //     var kode=$('#textkode').val();
	        //     $.ajax({
	        //     type : "POST",
	        //     url  : "<?php echo base_url('index.php/barang/hapus_barang')?>",
	        //     dataType : "JSON",
	        //             data : {kode: kode},
	        //             success: function(data){
	        //                     $('#ModalHapus').modal('hide');
	        //                     tampil_data_barang();
	        //             }
	        //         });
	        //         return false;
	        //     });
	 
	    //});

	</script>
	
		<!-- Sweet Alert -->

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/sweetalert2/js/sweetalert2.min.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/sweetalert.js"></script>



	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/jquery.dataTables.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/dataTables.bootstrap.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/datatables_custom.js"></script>

	<!-- Sweet Alert -->

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/sweetalert2/js/sweetalert2.min.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/sweetalert.js"></script>