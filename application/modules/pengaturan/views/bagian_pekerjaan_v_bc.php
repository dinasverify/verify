	<!-- Sweet Alert -->
	<link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/sweetalert2/css/sweetalert2.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom_css/sweet_alert2.css">
	<!-- Content Header (Page header) -->
		<section class="content-header">
            <h1>Bagian & Pekerjaan</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url('dashboard');?>">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li> <a href="<?php echo site_url('pengaturan');?>">Pengaturan</a></li>
				<li> Bagian & Pekerjaan </li>
            </ol>
		</section>
		<!-- Main content -->
        <section class="content p-l-r-15">
            <div class="row">			
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
							<h4 class="panel-title">
                                <i class="ti-briefcase"></i> Bagian & Pekerjaan
                            </h4>	
                        </div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12">
									<div class="row">
										<div class="col-md-8">
											<h4>1. Apa saja bagian atau divisi yang ada di perusahaan Anda</h4>
										</div>
										<div class="col-md-4">
											<div class="pull-right">
												<button type="button" id="myBtn1" class="btn btn-sm btn-default pull-right" data-target="#info" data-toggle="modal"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="pull-right">
										<a href="<?php echo site_url('pengaturan/tambahbagian'); ?>" class="btn btn-info btn-sm" data-style="Tambah Bagian">
											<i class="fa ti-plus" aria-hidden="true"></i> Tambah Bagian
										</a>
									</div>
								</div>
							</div></br>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="table-responsive">
										<table class="table table-striped" id="table">
											<thead>
												<tr class="filters">
													<th style="width:45%">Nama Bagian</th>
													<th style="text-align:center">Status</th>
													<th style="text-align:center">Aksi</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Dewan Komisaris</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Dewan Direksi</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Business Development</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>HCGA & Finance</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>EBIS (Enterprise Business Solution)</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>ERP (Enterprise Resource Planning</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>EAI (Enterprise Application Integration</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Billing System</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>BIM (Business Intelegence And Marketplace</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div></br>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="row">
										<div class="col-md-8">
											<h4>2. Apa saja Job Title (Nama Jabatan atau Nama Pekerjaan) yang ada di perusahaan Anda?</h4>
										</div>
										<div class="col-md-4">
											<div class="pull-right">
												<button type="button" id="myBtn2" class="btn btn-sm btn-default pull-right" data-target="#info2" data-toggle="modal"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="pull-right">
										<a href="<?php echo site_url('pengaturan/tambahpekerjaan'); ?>" class="btn btn-info btn-sm" data-style="Tambah Pekerjaan">
											<i class="fa ti-plus" aria-hidden="true"></i> Tambah Pekerjaan
										</a>
									</div>
								</div>
							</div></br>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="table-responsive">
										<table class="table table-striped" id="table">
											<thead style="background:#e9f0d8">
												<tr class="filters">
													<th style="width:45%">Nama Pekerjaan</th>
													<th style="text-align:center">Status</th>
													<th style="text-align:center">Aksi</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Komisaris Utama</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Komisaris</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Direktur Utama</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Manager</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Marketing & Sales Manager</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Marketing Staff</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Finance Staff</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Billing System</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>HC Staff</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Office Boy</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Assistant Manager</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Developer</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>PMO</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>System Analyst</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Software Tester</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Technical Writer</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Jr PMO</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Project Admin</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div></br>
							
							<div id="info" class="modal fade animated" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>
											<p class="text-center">
												Tabel ini berisikan daftar nama bagian atau divisi yang mungkin ada di dalam perusahaan Anda. Silakan menghapus, menambah atau mengubah isi tabel ini sesuai struktur organisasi perusahaan Anda.
											</p>
										</div>
									</div>
								</div>
							</div>
							
							<div id="info2" class="modal fade animated" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>
											<p class="text-center">
												Tabel berikut berisikan daftar nama pekerjaan (job title) yang mungkin ada di dalam perusahaan Anda. Silakan menghapus, menambah atau mengubah isi tabel ini sesuai kebutuhan perusahaan.
											</p>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
            <div class="background-overlay"></div>
        </section> 
	<!-- Sweet Alert -->
	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/sweetalert2/js/sweetalert2.min.js"></script>
	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/sweetalert.js"></script>