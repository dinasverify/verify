	<!--smoott-->   
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">
    <link href="<?php echo $themes_url; ?>vendors/hover/css/hover-min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $themes_url; ?>vendors/laddabootstrap/css/ladda-themeless.min.css">
    <link href="<?php echo $themes_url; ?>css/buttons_sass.css" rel="stylesheet">
    <link href="<?php echo $themes_url; ?>css/advbuttons.css" rel="stylesheet">
	<link href="<?php echo $themes_url; ?>vendors/daterangepicker/css/daterangepicker.css" rel="stylesheet" type="text/css"/>

	<!-- Content Header (Page header) -->

		<section class="content-header">

            <h1>Pengaturan Kalender</h1>

            <ol class="breadcrumb">

                <li>

                    <a href="<?php base_url('dashboard');?>">

                        <i class="fa fa-fw ti-home"></i> Dashboard

                    </a>

                </li>

                <li> <a href="<?php base_url('pengaturan');?>">Pengaturan</a></li>

				 <li> <a href="<?php base_url('pengaturan/kalender');?>">Kalender</a></li>
				 <li> Edit Pengaturan Kalender </li>
             </ol>

		</section>

		

		<!-- Main content -->

        <section class="content p-l-r-15">

            <div class="row">			

                <div class="col-md-12">
                	<?php echo $this->session->flashdata('pesan'); ?>
                    <div class="panel">

                        <div class="panel-heading">

							<h4 class="panel-title">

                                <i class="ti-calendar"></i> Edit Pengaturan Kalender

                            </h4>	
                             <span class="pull-right">
	                                <i class="fa fa-fw ti-angle-up clickable"></i>
	                                <i class="fa  fa-fw ti-close removepanel clickable"></i>
                            </span>

                        </div>

						<div class="panel-body">
							<form class="form-horizontal" action="<?php echo base_url('pengaturan/editkalender?id='.$id);?>" method="post">
								<div class="row">
									<div class="col-md-12">

										<label class="control-label col-md-2" for="nm_event">Nama Event</label>

										<div class="col-md-9">

											<input type="text" value="<?php echo $event; ?>" placeholder="Nama hari Event" class="form-control" id="nama_event" name="nama_event">

										</div>

									</div>
								</div>
						
								<br>

								<div class="row">
									<div class="col-md-12">

										<label class="control-label col-sm-2" for="tgl">Tanggal</label>

										<div class="col-md-9">

											<div class="input-group">

												<div class="input-group-addon">

													<i class="fa fa-fw ti-calendar"></i>

												</div>

												<input type="text" value="<?php echo $tanggal; ?>" name="tanggal" class="form-control" id="fromdate" placeholder="Format Tanggal DD/MM/YYYY"/>

											</div>

										</div>

									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-md-12">

											<label class="control-label col-md-2" for="nm_event">Tipe</label>

											<div class="col-md-9">

												<select id="dynamic"  name="tipe" class="form-control" tabindex="-1" required="">

														<option value="">- Pilihan -</option>

														<option <?php if($tipe==1){ echo "selected";}?> value="1">Libur Nasional</option>

														<option <?php if($tipe==2){ echo "selected";}?> value="2">Cuti Bersama</option>

														<option <?php if($tipe==3){ echo "selected";}?> value="3">Event Non Libur</option>

													</select>

												</div>

										</div>
								</div>
								
							
							<hr>
							<div class="form-group">							

							<div class="row">

								<div class="col-md-11">

									<div class="pull-right">

										<button name="update" type="submit" class="btn btn-sm btn-primary"><span class="ti-save"></span> Update</button>

										<button class="btn btn-sm btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Batal</button>

									</div>

								</div>

							</div>

							</div>
						</form>

						</div>

					</div>

				</div>

			</div>

		</div>

            <!-- row-->

        </section>

		<div class="background-overlay"></div>

   	
	<script> 
		$('#notifications').slideDown('slow').delay(3500).slideUp('slow');
	</script>
	
	<!-- airdatepicker -->

	<script src="<?php echo $themes_url; ?>vendors/moment/js/moment.min.js" type="text/javascript"></script>	

    <script src="<?php echo $themes_url; ?>vendors/datetime/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.min.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.en.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>js/custom_js/advanceddate_pickers.js"></script>

		<!-- bootstrap time picker -->

	<script src="<?php echo $themes_url; ?>vendors/clockpicker/js/bootstrap-clockpicker.min.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/datedropper/datedropper.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/timedropper/js/timedropper.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>js/custom_js/datepickers.js" type="text/javascript"></script> 

