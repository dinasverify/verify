			<!--smoott-->   
	<link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">
	<link href="<?php echo $themes_url; ?>vendors/hover/css/hover-min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo $themes_url; ?>vendors/laddabootstrap/css/ladda-themeless.min.css">
	<link href="<?php echo $themes_url; ?>css/buttons_sass.css" rel="stylesheet">
	<link href="<?php echo $themes_url; ?>css/advbuttons.css" rel="stylesheet">
	<!-- Sweet Alert -->

	<link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/sweetalert2/css/sweetalert2.min.css"/>

    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom_css/sweet_alert2.css">

		<!-- Content Header (Page header) -->

		<section class="content-header">

           <h1>Setting Department</h1>

            <ol class="breadcrumb">

                <li>

                    <a href="<?php base_url('dashboard');?>">

                        <i class="fa fa-fw ti-home"></i> Dashboard

                    </a>

                </li>

                <li> <a href="<?php base_url('absensi');?>">Pengaturan</a></li>

				 <li> <a href="<?php base_url('absensi');?>">Department & Job</a></li>

             </ol>

		</section>

		<!-- Main content -->

        <section class="content p-l-r-15">

            <div class="row">			

                <div class="col-md-12">

                    <div class="panel">

                        <div class="panel-heading">

							<h4 class="panel-title">

                                <i class="menu-icon fa fa-cogs fa-lg fa-fw"></i> Setting Department

                            </h4>	
                            <span class="pull-right">
								<i class="fa fa-fw ti-angle-up clickable"></i>
								<i class="fa fa-fw ti-close removepanel clickable"></i>
							</span>
                        </div>

						<div class="panel-body">

							<div class="row">

								<div class="col-xs-12">

									<div class="row">

										<div class="col-md-8">

											<h4><p>1. Apa saja bagian atau divisi yang ada di perusahaan Anda</p></h4>

										</div>

										<div class="col-md-4">

											<div class="pull-right">

												<button type="button" id="myBtn" class="btn btn-sm btn-default" style="border-radius:10px" data-target="#info" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>

											</div>

										</div>

									</div>

								</div>

							</div>

							

							<div class="row">

								<div class="col-xs-12">

									<div class="pull-right">

										<a href="adddepartment"><button class="btn btn-primary btn-sm "  data-toggle="modal">

											<i class="fa ti-plus" aria-hidden="true"></i> Tambah Department

										</button></a>

									</div>

									<!--<div class="panel-body">

										<button type="button" class="btn btn-primary btn-lg center-block"

													data-toggle="modal" data-target="#myModal"> 

												Click to open form in modal

											</button>-->

											<!-- Modal

									</div> -->

								</div>

							</div></br>

						

							<div class="row">

								<div class="col-xs-12">

									<div class="table-responsive">

										<table class="table table-striped" id="sample_2">

											<thead>

												<tr class="filters">

													<th style="width:5%">Id Department</th>

													<th style="width:10%">Kode Department</th>

													<th style="width:30%">Nama Department</th>

													<th style="text-align:center;width:30%">Deskripsi</th>

													<th style="text-align:center; width:10%">Status</th>

													<th style="text-align:center; width:10%">Aksi</th>

												</tr>

											</thead>

											<tbody>

												<?php 

													if(sizeof($data_bagian)>0){											

														for($i=0;$i<sizeof($data_bagian);$i++){

															$id 		= $data_bagian[$i]['id_departemen'];

															$kode_dep 		= $data_bagian[$i]['kode_dep'];

															$nama_dep		= $data_bagian[$i]['nama_dep'];

															$description	= $data_bagian[$i]['description'];

															$status			= $data_bagian[$i]['status'];

															if($status==1){$st="Aktif";}else{$st="Tidak Aktif";}

															echo "<tr>

																	<td>".$id."</td>

																	<td>".$kode_dep."</td>

																	<td>".$nama_dep."</td>

																	<td>".$description."</td>

																	<td style='text-align:center'>

																		".$st."

																		<!--<input type='checkbox' id='toggle_real' name='my-checkbox' data-size='small' checked>-->

																	</td>

																	<td style='text-align:center'>


																	<a href='".site_url('pengaturan/update_dept?id='.$id)."' data-toggle='tooltip' class='btn btn-warning btn-xs' data-original-title='Edit Department'>
																	<i class='ti-pencil'></i></a>&nbsp;&nbsp;

																	<a href='#' data-toggle='tooltip' class='btn btn-danger btn-xs' data-original-title='Delete Department'>
																	<i class='ti-trash'></i></a>&nbsp;&nbsp;
																	</td>

																</tr>";

														}

													}

											?>		

											</tbody>

										</table>

									</div>

								</div>

							</div></br>

							<hr>

							<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="add" aria-hidden="true">

								<div class="modal-dialog">

									<div class="modal-content">

										<div class="modal-header">

											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

											<h4 class="modal-title custom_align" id="Heading">Tambah Bagian</h4>

										</div>

										<div class="modal-body">

											<div class="form-group">

												<input class="form-control" name="kodebagian" type="text" placeholder="Kode Bagian">

											</div>

										</div>

										<div class="modal-body">

											<div class="form-group">

												<input class="form-control" name="bagian" type="text" placeholder="Bagian">

											</div>

										</div>

										<div class="modal-body">

											<div class="form-group">

												<input class="form-control" name="status" type="text" placeholder="Status">

											</div>

										</div>

										<div class="modal-body">

											<div class="form-group">

												<input class="form-control" name="desc" type="text" placeholder="Description">

											</div>

										</div>

										<div class="modal-footer ">

											<button type="button" class="btn btn-sm btn-primary"  data-dismiss="modal">

												<span class="ti-save"></span> Simpan

											</button>

										</div>

									</div>

								</div>

							</div>

							

							<div class="modal fade" id="add_job" tabindex="-1" role="dialog" aria-labelledby="add_job" aria-hidden="true">

								<div class="modal-dialog">

									<div class="modal-content">

										<div class="modal-header">

											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

											<h4 class="modal-title custom_align" id="Heading">Tambah Job</h4>

										</div>

										<div class="modal-body">

											<div class="form-group">

												<input class="form-control" name="kodejob" type="text" placeholder="Kode Pekerjaan">

											</div>

										</div>

										<div class="modal-body">

											<div class="form-group">

												<input class="form-control" name="namajob" type="text" placeholder="Nama Pekerjaan">

											</div>

										</div>

										<div class="modal-body">

											<div class="form-group">

												<input class="form-control" name="desc" type="text" placeholder="Description">

											</div>

										</div>

										<div class="modal-footer ">

											<button type="button" class="btn btn-sm btn-primary"  data-dismiss="modal">

												<span class="ti-save"></span> Simpan

											</button>

										</div>

									</div>

								</div>

							</div>

							

							<div class="row">

								<div class="col-xs-12">

									<div class="row">

										<div class="col-md-8">

											<h4><p>2. Apa saja Job Title (Nama Jabatan atau Nama Pekerjaan) yang ada di perusahaan Anda?</p></h4>

										</div>

										<div class="col-md-4">

											<div class="pull-right">

												<button type="button" id="myBtn" class="btn btn-sm btn-default"  data-target="#infojob" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>

											</div>

										</div>

									</div>

									<div class="row">

										<div class="col-xs-12">

											<div class="pull-right">

												<a href="addjob"><button class="btn btn-primary btn-sm">

													<i class="fa ti-plus"  aria-hidden="true"></i> Tambah Job

												</button></a>

											</div>

										</div>

									</div>

								</div>

							</div></br>

							

							<div class='modal fade' id='edit_job' tabindex='-1' role='dialog' aria-labelledby='edit' aria-hidden='true'>

								<div class='modal-dialog'>

									<div class='modal-content'>

										<div class='modal-header'>

											<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>

											<h4 class='modal-title custom_align' id='Heading'>Edit Job</h4>

										</div>

										<div class='modal-body'>

											<div class='form-group'>

												<input class='form-control ' type='text' placeholder='Nama Pekerjaan'>

											</div>

										</div>

										<div class='modal-footer '>

											<button type='button' class='btn btn-sm btn-success' style="border-radius:10px" data-dismiss='modal'>

												<span class='glyphicon glyphicon-ok-sign'></span> Update

											</button>

										</div>

									</div>

									<!-- /.modal-content -->

								</div>

							</div>

							

							<div class="row">

								<div class="col-xs-12">

									<div class="table-responsive">

										<table class="table table-striped" id="sample_1">

											<thead style="background:#F3F7F9">

												<tr class="filters">

													<th style="width:5%">No Job</th>

													<th style="width:10%">Kode Job</th>

													<th style="width:30%">Nama Pekerjaan</th>

													<th style="text-align:center;width:30%">Description</th>

													<th style="width:10%">Status</th>

													<th style="text-align:center">Aksi</th>

												</tr>

											</thead>

											<tbody>

												<?php 

													if(sizeof($data_job)>0){											$no = 1;

														for($i=0;$i<sizeof($data_job);$i++){

															$id 		= $data_job[$i]['id_job_t'];

															$kode 		= $data_job[$i]['kode_job_t'];

															$nama_job		= $data_job[$i]['nama_job'];

															$description	= $data_job[$i]['description'];

															$status			= $data_job[$i]['status_job'];

															if($status == 1){$st= "Aktif";}else{$st= "Non Aktif";}

															echo "<tr>

																	<td>".$no++."</td>

																	<td>".$kode."</td>

																	<td>".$nama_job."</td>

																	<td>".$description."</td>

																	<td>".$st."</td>

																	<td style='text-align:center'>

																		<a href='".site_url('pengaturan/update_job?id='.$id)."' data-toggle='tooltip' class='btn btn-warning btn-xs' data-original-title='Edit Job'>
																	<i class='ti-pencil'></i></a>&nbsp;&nbsp;

																	<a href='#' data-toggle='tooltip' class='btn btn-danger btn-xs' data-original-title='Delete Job'>
																	<i class='ti-trash'></i></a>&nbsp;&nbsp;

																	

																	</td>

																</tr>";

														}

													}

												?>

											</tbody>

										</table>

									</div>

								</div>

							</div></br>

							

							<div class='modal fade' id='edit' tabindex='-1' role='dialog' aria-labelledby='edit' aria-hidden='true'>

								<div class='modal-dialog'>

									<div class='modal-content'>

										<div class='modal-header'>

											<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>

											<h4 class='modal-title custom_align' id='Heading'>Edit Bagian</h4>

										</div>

										<div class='modal-body'>

											<div class='form-group'>

												<input class='form-control' name='dept' type='text' value='<?php echo $nama_dep; ?>' placeholder='Nama Department'>

											</div>

											<select name='statusdept' id='statusdept' class='form-control' required='' data-validation-required-message='<i></i> Tolong isi kotak di atas' aria-invalid='false'>

												<option value=''>-- Status Bagian --</option>

												<option value='1'>Aktif</option>

												<option value='2'>Tidak Aktif</option>

											</select>

											<p class='help-block text-right'></p>

										</div>

										<div class='modal-footer'>

											<a href='pengaturan/create'>

												<button name="tombol" class="btn btn-success" type="submit" style="border-radiius:10px">

													<span class="ti-save">

												</span> Update</button>

											</a>

											

											<!--<i class='fa fa-fw ti-pencil text-default actions_icon' title='Edit Bagian' ></i></a>

												<button href='deleted_users.html' type='button' class='btn btn-success' data-dismiss='modal'>

												<span class='glyphicon glyphicon-ok-sign'>

												</span> Update	

												</button>-->

										</div>

									</div>

									<!-- /.modal-content -->

								</div>

								<!-- /.modal-dialog -->

							</div>

							

							<div id="info" class="modal fade animated" role="dialog">

								<div class="modal-dialog">

									<div class="modal-content">

										<div class="modal-body">

											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>

											<p class="text-center">

												Pengaturan ini untuk informasi bagian departemen perusahaan.

											</p>

										</div>

									</div>

								</div>

							</div>

							

							<div id="infojob" class="modal fade animated" role="dialog">

								<div class="modal-dialog">

									<div class="modal-content">

										<div class="modal-body">

											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>

											<p class="text-center">

												Pengaturan ini untuk informasi bagian unit karyawan perusahaan.

											</p>

										</div>

									</div>

								</div>

							</div>

					

						</div>

					</div>

				</div>

			</div>

            <!-- row-->

        </section>

		   <div class="background-overlay"></div>

        <!-- /.content --> 

	<!-- Sweet Alert -->

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/sweetalert2/js/sweetalert2.min.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/sweetalert.js"></script>



	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/jquery.dataTables.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/dataTables.bootstrap.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/datatables_custom.js"></script>

	<!-- Sweet Alert -->

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/sweetalert2/js/sweetalert2.min.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/sweetalert.js"></script>