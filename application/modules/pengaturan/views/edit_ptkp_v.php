				<!--smoott-->   
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">
    <link href="<?php echo $themes_url; ?>vendors/hover/css/hover-min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $themes_url; ?>vendors/laddabootstrap/css/ladda-themeless.min.css">
    <link href="<?php echo $themes_url; ?>css/buttons_sass.css" rel="stylesheet">
    <link href="<?php echo $themes_url; ?>css/advbuttons.css" rel="stylesheet">
	<!-- Content Header (Page header) -->

		<section class="content-header">

            <h1>Edit Pengaturan PPh 21</h1>

            <ol class="breadcrumb">

                <li>

                    <a href="<?php base_url('dashboard');?>">

                        <i class="fa fa-fw ti-home"></i> Dashboard

                    </a>

                </li>

                <li> <a href="<?php base_url('pengaturan');?>">Pengaturan</a></li>

				 <li> <a href="#">Pengaturan PPh 21</a></li>
				 <li> Edit Pengaturan PPh 21 </li>
             </ol>

		</section>

		

		<!-- Main content -->

        <section class="content p-l-r-15">

            <div class="row">			

                <div class="col-md-12">
                	<?php echo $this->session->flashdata('pesan'); ?>
                    <div class="panel">

                        <div class="panel-heading">

							<h4 class="panel-title">

                                <i class="ti-calendar"></i> Edit Pengaturan PPh 21

                            </h4>	
                             <span class="pull-right">
	                                <i class="fa fa-fw ti-angle-up clickable"></i>
	                                <i class="fa  fa-fw ti-close removepanel clickable"></i>
                            </span>

                        </div>

						<div class="panel-body">
							<form class="form-horizontal" action="<?php echo base_url('pengaturan/editptkp?id='.$id);?>" method="post">
								<div class="row">
									<div class="col-md-12">

										<label class="control-label col-md-2" for="nm_event">Nama Uraian Pajak</label>

										<div class="col-md-10">

											<input type="text" value="<?php echo $uraian; ?>" placeholder="Nama hari Event" class="form-control" id="nama_event" name="uraian">

										</div>

									</div>
								</div>
						
								<br>

								<div class="row">
									<div class="col-md-12">

										<label class="control-label col-sm-2" for="tgl">Status Pajak</label>

										<div class="col-md-10">

											<input type="text" value="<?php echo $statuspajak; ?>" placeholder="Nama hari Event" class="form-control" id="nama_event" name="statuspajak">

										</div>

									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-md-12">

										<label class="control-label col-sm-2" for="tgl">PTKP</label>

										<div class="col-md-10">

											<input type="text" value="<?php echo $PTKP; ?>" placeholder="Nama hari Event" class="form-control" id="nama_event" name="ptkp">

										</div>

									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-md-12">									
										<label class="control-label col-sm-2" for="tgl">Status</label>

										<div class="col-md-10">

											<select class="form-control" name="status" id="">

												<option value="">Silahkan Pilih</option>

												<option <?php if($status==1){ echo "selected"; }?> value="1">Aktif</option>

												<option <?php if($status==0){ echo "selected"; }?> value="0">Non Aktif</option>

											</select>

										</div>
									</div>
								</div>
							<hr>
							
								<div class="row">

									<div class="col-md-12">

										<div class="pull-right">

											<button type="submit" name="update" class="btn btn-labeled btn-primary" >

													<span class="btn-label">

														<i class="ti-save"></i>

													</span> Update

												</button>

											<button type="button" class="btn btn-labeled btn-danger" onclick="document.location='<?php echo site_url('pengaturan/pph21'); ?>'">

													<span class="btn-label">

														<span class="glyphicon glyphicon-remove"></span>

													</span> Batal

												</button>

										</div>

									</div>

								</div>

							

						</form>

						</div>

					</div>

				</div>

			</div>

		</div>

            <!-- row-->

        </section>

		<div class="background-overlay"></div>

   	
	<script> 
		$('#notifications').slideDown('slow').delay(3500).slideUp('slow');
	</script>
	
	<!-- airdatepicker -->

	<script src="<?php echo $themes_url; ?>vendors/moment/js/moment.min.js" type="text/javascript"></script>	

    <script src="<?php echo $themes_url; ?>vendors/datetime/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.min.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.en.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>js/custom_js/advanceddate_pickers.js"></script>

		<!-- bootstrap time picker -->

	<script src="<?php echo $themes_url; ?>vendors/clockpicker/js/bootstrap-clockpicker.min.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/datedropper/datedropper.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/timedropper/js/timedropper.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>js/custom_js/datepickers.js" type="text/javascript"></script> 

