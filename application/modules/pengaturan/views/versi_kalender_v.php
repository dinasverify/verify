	<!-- Calendar -->

	<link href="<?php echo $themes_url; ?>vendors/fullcalendar/css/fullcalendar.css" rel="stylesheet" type="text/css"/>

    <link href="<?php echo $themes_url; ?>vendors/fullcalendar/css/fullcalendar.print.css" rel="stylesheet" media='print' type="text/css">

    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">

    <link href="<?php echo $themes_url; ?>css/calendar_custom.css" rel="stylesheet" type="text/css"/>

		<!-- Content Header (Page header) -->

		<section class="content-header">

           <h1>Pengaturan Kalender</h1>

            <ol class="breadcrumb">

                <li>

                    <a href="<?php base_url('dashboard');?>">

                        <i class="fa fa-fw ti-home"></i> Dashboard

                    </a>

                </li>

                <li> <a href="<?php base_url('pengaturan');?>">Pengaturan</a></li>

				 <li> <a href="<?php base_url('pengaturan/versikalender');?>">Versi Kalender</a></li>

             </ol>

		</section>

		<!-- Main content -->

        <section class="content p-l-r-15">

            <div class="row">			

                <div class="col-md-12">

                    <div class="panel">

                        <div class="panel-heading">

							<h4 class="panel-title">

                                <i class="ti-calendar"></i> Kalender Perusahaan

                            </h4>	

                        </div>

						<div class="panel-body">

							<div class="row">

								<div class="col-md-3">

									<div class="box">

										<div class="box-title">

											<h3>Drag Events</h3>

											<div class="pull-right box-toolbar">

												<a href="#" class="btn btn-link btn-xs" data-toggle="modal" data-target="#myModal">

													<i class="fa ti-plus"></i>

												</a>

											</div>

										</div>

										<div class="box-body">

											<div id='external-events'>

												<div class='external-event palette-warning'>Team Out</div>

												<div class='external-event palette-primary'>Product Seminar</div>

												<div class='external-event palette-danger'>Client Meeting</div>

												<div class='external-event palette-info'>Repeating Event</div>

												<div class='external-event palette-success'>Anniversary Celebrations</div>

												<p class="well no-border no-radius">

													<input type='checkbox' class="custom_icheck" id='drop-remove'/>

													<label for='drop-remove'>remove after drop</label>

												</p>

											</div>

										</div>

										<div class="box-footer">

											<a href="#" class="btn btn-sm btn-success btn-block" style="border-radius:10px" data-toggle="modal" data-target="#myModal">Tambah Event</a>

										</div>

									</div>

									<!-- /.box -->

								</div>

								<div class="col-md-9">

									<div class="box">

										<div class="box-body">

											<div id="calendar"></div>

										</div>

									</div>

								</div>

							</div>

						</div>

						

						<!-- Modal -->

						<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"

							 aria-hidden="true">

							<div class="modal-dialog">

								<div class="modal-content">

									<div class="modal-header">

										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

										<h4 class="modal-title" id="myModalLabel"> Tambah Event

										</h4>

									</div>

									<div class="modal-body">

										<div class="input-group">

											<input type="text" id="event" class="form-control" placeholder="Nama Event">

											<div class="input-group-btn">

												<button type="button" id="color-chooser-btn" class="btn btn-info dropdown-toggle"

													data-toggle="dropdown"> Select <span class="caret"></span>

												</button>

												<ul class="dropdown-menu pull-right" id="color-chooser">

													<li>

														<a class="palette-primary" href="#">Primary</a>

													</li>

													<li>

														<a class="palette-success" href="#">Success</a>

													</li>

													<li>

														<a class="palette-info" href="#">Info</a>

													</li>

													<li>

														<a class="palette-warning" href="#">warning</a>

													</li>

													<li>

														<a class="palette-danger" href="#">Danger</a>

													</li>

													<li>

														<a class="palette-default" href="#">Default</a>

													</li>

												</ul>

											</div>

											<!-- /btn-group -->

										</div>

										<!-- /input-group -->

									</div>

									<div class="modal-footer">

										<button type="button" class="btn btn-sm btn-danger pull-right" style="border-radius:10px" id="close_calendar_event"

											data-dismiss="modal"> <span class="glyphicon glyphicon-remove"></span> Batal

										</button>

										<button type="button" class="btn btn-sm btn-success pull-left" style="border-radius:10px" id="add-new-event"

											data-dismiss="modal"> <span class="glyphicon glyphicon-plus"></span> Tambah

										</button>

									</div>

								</div>

							</div>

						</div>



					</div>

				</div>

			</div>

            <!-- row-->

        </section>

		<div class="background-overlay"></div>

        <!-- /.content --> 

	<!-- Calendar -->
	<script src="<?php echo $themes_url; ?>js/app.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/moment/js/moment.min.js" type="text/javascript"></script>	

	<script src="<?php echo $themes_url; ?>vendors/fullcalendar/js/fullcalendar.min.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>js/custom_js/calendar_custom.js" type="text/javascript"></script>


	<script src="<?php echo $themes_url; ?>vendors/moment/js/moment.min.js" type="text/javascript"></script>
