			<!--smoott-->   
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">
    <link href="<?php echo $themes_url; ?>vendors/hover/css/hover-min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $themes_url; ?>vendors/laddabootstrap/css/ladda-themeless.min.css">
    <link href="<?php echo $themes_url; ?>css/buttons_sass.css" rel="stylesheet">
    <link href="<?php echo $themes_url; ?>css/advbuttons.css" rel="stylesheet">
	<!-- Sweet Alert -->

	<link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/sweetalert2/css/sweetalert2.min.css"/>

    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom_css/sweet_alert2.css">

		<!-- Content Header (Page header) -->

		<section class="content-header">

            <h1>PPh 21</h1>

            <ol class="breadcrumb">

                <li>

                    <a href="<?php echo site_url('dashboard');?>">

                        <i class="fa fa-fw ti-home"></i> Dashboard

                    </a>

                </li>

                <li> <a href="<?php echo site_url('pengaturan');?>">Pengaturan</a></li>

				<li> PPh 21 </li>

            </ol>

		</section>

		<!-- Main content -->

        <section class="content p-l-r-15">

            <div class="row">			

                <div class="col-md-12">

                    <div class="panel">

                        <div class="panel-heading">

							<h4 class="panel-title">

                                <i class="ti-zip"></i> PPh 21

                            </h4>	
                             <span class="pull-right">
	                                <i class="fa fa-fw ti-angle-up clickable"></i>
	                                <i class="fa  fa-fw ti-close removepanel clickable"></i>
                            </span>

                        </div>

						<div class="panel-body">

							<div class="row">

								<div class="col-xs-12">

									<div class="row">

										<div class="col-md-10">

											<h4><p>1. Apakah perusahaan Anda menerapkan perhitungan Pajak PPh 21?

											<a  data-toggle='modal' data-target='#editaturanpph21' ><button type="button" id="myBtn6" class="btn btn-sm btn-primary" ><i class="fa ti-pencil"  data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></button></a></p></h4>

										</div>

									</div>

								</div>

							</div>

							

							<div class="row">

								<div class="col-xs-12">

									<div class="table col-md-9">

										<table>

											<tr>

												<td style="width:20px"><i class="fa fa-check"></i></td>

												<td style="width:20px"><?php if($statuspajak == 1){echo "Ya";}else{echo "Tidak";}?></td>

											</tr>

										</table>

									</div>

								</div>

							</div>

							<hr>

							<div class="row">

								<div class="col-xs-12">

									<div class="row">

										<div class="col-md-10">

											<h4><p>2. Input Nama Pemotong dan NPWP Pemotong

											<a data-toggle='modal' data-target='#editpemotong'><button type="button" id="myBtn3" class="btn btn-sm btn-primary" ><i class="fa ti-pencil" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></button></a></p></h4>

										</div>

										<div class="col-md-2">

											<div class="pull-right">

												<button type="button" id="myBtn4" class="btn btn-sm btn-default pull-right"  data-target="#infoPemotong" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>

											</div>

										</div>

									</div>

								</div>

							</div>

							

							<div class="row">

								<div class="col-xs-12">

									<div class="table-responsive">

										<table class="table table-bordered">

											<thead>

												<tr>

													<td style="width:200px">Nama Pemotong</td>

													<td><?php echo $nama_perusahaan;?></td>

												</tr>

												<tr>

													<td style="width:200px">NPWP Pemotong</td>

													<td><?php echo $npwp;?></td>

												</tr>

												<tr>

													<td style="width:200px">Status</td>

													<td><?php echo $status;?></td>

												</tr>

											</thead>

										</table>

									</div>

								</div>

							</div>

							<hr>

							<div class="row">

								<div class="col-xs-12">

									<div class="row">

										<div class="col-md-10">

											<h4><p>3. Metode apa yang digunakan perusahaan Anda dalam menghitung PPh 21?

											<a data-toggle='modal' data-target='#editmethod'><button type="button" id="myBtn5" class="btn btn-sm btn-primary" ><i class="fa ti-pencil" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></button></a></p></h4>

										</div>

										<div class="col-md-2">

											<div class="pull-right">

												<button type="button" id="myBtn6" class="btn btn-sm btn-default pull-right"  data-target="#infoMetode" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>

											</div>

										</div>

									</div>

								</div>

							</div>

							<div class="row">

								<div class="col-xs-12">

									<div class="table-responsive">

										<table class="table table-bordered">

											<thead>

												<tr>

													<td style="width:200px">Methode PPh21</td>

													<td><?php echo $method;?></td>

												</tr>

											</thead>

										</table>

									</div>

								</div>

							</div>

							<br>

							<hr>

							<div class="row">

								<div class="col-xs-12">

									<h3>Informasi PTKP</h3>

									<div class="row" >

										<div class="col-xs-12">

											<div class="table-responsive">

												<table class="table table-striped" id="sample_2">

													<thead style="background:#e9f0d8">

														<tr class="filters">

															<th style="width:2%">No</th>

															<th>Id</th>

															<th style="width:25%">Uraian</th>

															<th style="width:20%;text-align:center">Status Pajak</th>

															<th style="width:20%;text-align:center">PTKP</th>

															<th style="width:10%;text-align:center">Status</th>

															<th style="width:20%;text-align:center">Aksi</th>

														</tr>

													</thead>

													<tbody>

														<?php $no =1; foreach($status_ptkp as $data) { ?>

														<tr>

															<td><?php echo $no;?></td>

															<td><?php echo $data['id']?></td>

															<td><?php echo $data['uraian']?></td>

															<td style='text-align:center'><?php echo $data['statuspajak']?></td>

															<td style='text-align:center'><?php echo number_format($data['PTKP'])?>

															<td><?php if($data['status'] == 1){echo "Active";}else{ echo "Non Active";}?></td>
															

															</td>

															<td style='text-align:center'>

																<a class="btn-xs btn-warning" href="<?php echo site_url('pengaturan/editptkp?id='.$data['id']); ?>"><i class='fa fa-fw ti-pencil text-default actions_icon' data-toggle='tooltip' data-placement='top' title data-original-title='Edit'></i></a>&nbsp;&nbsp;

															</td>

														</tr>

														<?php } ?>

													</tbody>

												</table>

											</div>

										</div>

									</div>

										</div>

									</div></br>

							

							<div class="row">

								<div class="col-xs-12">

									<div class="col-md-10 col-md-offset-1 noteInfo">

										<div class="table-responsive">

											<table class="table table-bordered">

												<thead style="background:#0088cc;color:white"><!--style="background:#34586e;color:white"-->

													<tr>

														<th>

															Catatan: </br>

															Data PPh 21 mulai awal tahun berjalan hingga menggunakan Gadjian dapat diisi sesudah Anda melengkapi Data Personalia.

														</th>

													</tr>

												</thead>

											</table>

										</div>

									</div>

								</div>

							</div>

							<!--Update Point-->

							<div class='modal fade' id='editaturanpph21' tabindex='-1' role='dialog' aria-labelledby='edit' aria-hidden='true'>

								<div class='modal-dialog'>

									<div class='modal-content'>

										<div class='modal-header'>

											<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>

											<h4 class='modal-title custom_align' id='Heading'>Edit Status Pajak</h4>

										</div>

										<div class='modal-body'>

											<form class="form-horizontal" id="form_edit_kalender" method="POST" action="<?php echo base_url('pengaturan/editpph21');?>">

											<div class='modal-header'>

											<div class='form-group'>

												<h4>Apakah perusahaan Anda menerapkan pajak PPh 21?</h4>

											</div>

											<div class='form-group'>

												<?php

														if($statuspajak == 1){

															echo "<input type='radio' name='radios3' value='1' class='square-blue' checked>

													<label name='Yes' for='Test3_1'>Yes</label>

													<input type='radio' name='radios3' value='0' class='square-blue'>

													<label name='no' for='Test3_1'>No</label>";

														}else{

															echo "<input type='radio' name='radios3' value='1' class='square-blue'>

													<label name='Yes' for='Test3_1'>Yes</label>

													<input type='radio' name='radios3' value='0' class='square-blue'checked>

													<label name='no' for='Test3_1'>No</label>";}

													?>

											</div>

										</div>

										<div class='modal-footer'>

											<button type='submit' class='btn btn-sm btn-primary'>

												<span class='glyphicon glyphicon-ok-sign'></span> Update

											</button>

										</div>

									</form>

									</div>

									<!-- /.modal-content -->

								</div>

							</div>

							</div>

							<div class='modal fade' id='editpemotong' tabindex='-1' role='dialog' aria-labelledby='edit' aria-hidden='true'>

								<div class='modal-dialog'>

									<div class='modal-content'>

										<form class="form-horizontal" id="form_edit_kalender" method="POST" action="<?php echo base_url('pengaturan/editpemotong');?>">

											<div class='modal-header'>

												<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>

												<h4 class='modal-title custom_align' id='Heading'>Edit Pemotong Pajak</h4>

											</div>

											<div class='modal-body'>

												<input class='form-control' name='nama_pemotong' type='text' value='<?php echo $nama_perusahaan; ?>' placeholder='Nama Pemotong'><br>

												<input class='form-control' name='npwp' type='text' value='<?php echo $npwp; ?>' placeholder='No NPWP'>

												<br>

												<select name='status' id='status' class='form-control' required='' data-validation-required-message='<i></i> Tolong isi kotak di atas' aria-invalid='false'>

													<option value=''>Pilih Status</option>

													<option value='1' <?php if($status =="Sudah Diterapkan"){echo "selected";} ?> >Diterapkan</option>

													<option value='0' <?php if($status =="Tidak Diterapkan"){echo "selected";} ?> >Tidak Diterapkan</option>

												</select>

												<p class='help-block text-right'></p>

											</div>

											<div class='modal-footer'>

												<a href='pengaturan/create'>

													<button name="tombol" class="btn btn-success" type="submit" style="border-radiius:10px">

														<span class="ti-save">

													</span> Update</button>

												</a>

											</div>

										</form>

									</div>

									<!-- /.modal-content -->

								</div>

								<!-- /.modal-dialog -->

							</div>



							<div class='modal fade' id='editpemotongx' tabindex='-1' role='dialog' aria-labelledby='edit' aria-hidden='true'>

								<div class='modal-dialog'>

									<div class='modal-content'>

										<form class="form-horizontal" id="form_edit_kalender" method="POST" action="<?php echo base_url('pengaturan/editpph21');?>">

										<div class='modal-header'>

											<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>

											<h4 class='modal-title custom_align' id='Heading'>Edit Pemotong Pajak</h4>

										</div>

										

											<div class='modal-body'>

												<div>

													<div class="form-group">

														<input class="form-control" name="kodejob" type="text" placeholder="Kode Pekerjaan">

													</div>

												</div>												

											</div>

											<div class="modal-footer ">

												<button type="button" class="btn btn-sm btn-primary"  data-dismiss="modal">

													<span class="ti-save"></span> Update

												</button>

											</div>

										

									<!-- /.modal-content -->

									</form>

								</div>



							</div>

							</div>

							<div class='modal fade' id='editmethod' tabindex='-1' role='dialog' aria-labelledby='edit' aria-hidden='true'>

								<div class='modal-dialog'>

									<div class='modal-content'>

										<div class='modal-header'>

											<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>

											<h4 class='modal-title custom_align' id='Heading'>Edit Methode PPh21</h4>

										</div>

										<div class='modal-body'>

											<form class="form-horizontal" id="form_edit_kalender" method="POST" action="<?php echo base_url('pengaturan/editmethod');?>">

											<div class='modal-header'>

											<div class='form-group'>

												<h4>Pilih metode apa yang digunakan perusahaan Anda dalam menghitung PPh 21?</h4>

											</div>

											<div class='form-group'>

												<select class="form-control" name="method" required="">

													<option value="">Pilih Method PPh 21</option>

													<option <?php if($method == 'Net Method'){ echo "selected";}?> value="Net Method">Net Method</option>

													<option <?php if($method == 'Gross Up Method'){ echo "selected";}?> value="Gross Up Method">Gross Up Method</option>

													<option <?php if($method == 'Gross Method'){ echo "selected";}?> value="Gross Method">Gross Method</option>

												</select>

											</div>

										</div>

										<div class='modal-footer'>

											<button type='submit' class='btn btn-sm btn-primary'>

												<span class='glyphicon glyphicon-ok-sign'></span> Update

											</button>

										</div>

									</form>

									</div>

									<!-- /.modal-content -->

								</div>

							</div>

							</div>

							<!--Update Point-->

							<!--info-->

							<div id="info" class="modal fade animated" role="dialog">

								<div class="modal-dialog">

									<div class="modal-content">

										<div class="modal-body">

											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>

											<p class="text-center">

												Pengaturan ini memberi tahu sistem mengenai penerapan perhitungan PPh 21 dalam perusahaan.

											</p>

										</div>

									</div>

								</div>

							</div>

							<div id="infoPemotong" class="modal fade animated" role="dialog">

								<div class="modal-dialog">

									<div class="modal-content">

										<div class="modal-body">

											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>

											<p class="text-center">

												Nama Pemotong adalah pihak yang menandatangani SPT, yaitu Pemotong / Pimpinan atau kuasa. NPWP Pemotong adalah NPWP yang menandatangani SPT sebagaimana dimaksud pada Nama Pemotong.

											</p>

										</div>

									</div>

								</div>

							</div>

							<div id="infoMetode" class="modal fade animated" role="dialog">

								<div class="modal-dialog">

									<div class="modal-content">

										<div class="modal-body">

											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>

											<p class="text-center">

												Metode Gross-up yaitu PPh 21 dibayar perusahaan dan dimasukkan sebagai tunjangan penghasilan untuk karyawan. Metode Gross yaitu karyawan membayar sendiri PPh 21-nya. Metode Nett yaitu PPh 21 dibayar perusahaan, tapi tidak dimasukkan sebagai unsur penghasilan karyawan.

											</p>

										</div>

									</div>

								</div>

							</div>

							<!--info-->

						</div>

					</div>

				</div>

			</div>

            <!-- row-->

        </section>

		<div class="background-overlay"></div>

	<!-- Sweet Alert -->

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/sweetalert2/js/sweetalert2.min.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/sweetalert.js"></script>



	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/jquery.dataTables.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/dataTables.bootstrap.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/datatables_custom.js"></script>

