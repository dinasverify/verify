		<!-- Content Header (Page header) -->
		<section class="content-header">
            <h1>Configurasi</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url('dashboard');?>">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li> <a href="<?php echo site_url('pengaturan');?>">Pengaturan</a></li>
				<li> Configurasi </li>
            </ol>
		</section>
		<!-- Main content -->
        <section class="content p-l-r-15">
            <div class="row">			
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
							<h4 class="panel-title">
                                <i class="ti-zip"></i> Configurasi
                            </h4>	
                        </div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12">
									<div class="col-md-12">
										<div class="table-responsive">
											<table id="user" class="table table-striped">
				                                <tbody>
													<tr>
					                                   <td style="background:#34586e;color:white;width: 90%">
														</td>
					                                    <td style="background:#34586e;color:white"><a href="<?php echo site_url('pengaturan/editsetperusahaan');?>"><button class="btn btn-sm btn-primary">
																<i class="fa fa-edit" aria-hidden="true"></i> Perubahan Settingan
															</button></a></td>
					                                </tr>
					                                <tr>
					                                    <td class="table_simple"><h4>Nama Perusahaan :</h4></td>
					                                    <td><button  type="button" id="myBtn4" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button> </td>
					                                </tr>
					                                <tr>
					                                    <td  colspan='2'><h4><?php echo $namaperusanaan;?></h4></td>
													</tr>
					                                <tr>
														<td class="table_simple"><h4>Bidang :</h4></td>
					                                    <td><button  type="button" id="myBtn4" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button> </td>
					                                </tr>

					                                <tr>
					                                    <td  colspan='2'><h4><?php echo $bidang;?></h4></td>
													</tr>
					                                <tr>
					                                    <td><h4>Alamat :</h4></td>
														<td><button  type="button" id="myBtn4" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button> </td>
					                                </tr>
					                                <tr>
														<td colspan="2"><h4><?php echo $alamat; ?></h4></td>
													</tr>
					                                <tr>
					                                    <td><h4>Registrasi : </h4></td>
														<td><button  type="button" id="myBtn4" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button> </td>
													</tr>
													<tr>
														<td colspan="2"><h4><?php echo date('d F Y',strtotime($tgl_reg)); ?></h4></td>
													</tr>
													<tr>
					                                    <td><h4>Berakhir : </h4></td>
														<td><button  type="button" id="myBtn4" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button> </td>
													</tr>
													<tr>
														<td colspan="2"><h4><?php echo date('d F Y',strtotime($end_reg)); ?></h4></td>
													</tr>
													<tr>
					                                    <td><h4>Logo Perusahaan : </h4></td>
														<td><button  type="button" id="myBtn4" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button> </td>
													</tr>
													<tr>
														<td colspan="2"><h4><?php echo "Ok"; ?></h4></td>
													</tr>
												</tbody>
			                            </table>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
            <!-- row-->
        </section>
		<div class="background-overlay"></div>
        <!-- /.content -->