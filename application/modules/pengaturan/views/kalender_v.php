	<!--smoott-->   
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">
    <link href="<?php echo $themes_url; ?>vendors/hover/css/hover-min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $themes_url; ?>vendors/laddabootstrap/css/ladda-themeless.min.css">
    <link href="<?php echo $themes_url; ?>css/buttons_sass.css" rel="stylesheet">
    <link href="<?php echo $themes_url; ?>css/advbuttons.css" rel="stylesheet">
	<link href="<?php echo $themes_url; ?>vendors/daterangepicker/css/daterangepicker.css" rel="stylesheet" type="text/css"/>


	<link href="<?php echo $themes_url; ?>vendors/fullcalendar/css/fullcalendar.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $themes_url; ?>vendors/fullcalendar/css/fullcalendar.print.css" rel="stylesheet" media='print' type="text/css">
    <link href="<?php echo $themes_url; ?>vendors/iCheck/css/all.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">
    <link rel="stylesheet" href="<?php echo $themes_url; ?>css/custom_css/skins/skin-default.css" type="text/css" id="skin"/>
    <link href="c<?php echo $themes_url; ?>ss/calendar_custom.css" rel="stylesheet" type="text/css"/>

	<!-- Content Header (Page header) -->

		<section class="content-header">

           <h1>Pengaturan Kalender</h1>

            <ol class="breadcrumb">

                <li>

                    <a href="<?php base_url('dashboard');?>">

                        <i class="fa fa-fw ti-home"></i> Dashboard

                    </a>

                </li>

                <li> <a href="<?php base_url('pengaturan');?>">Pengaturan</a></li>

				 <li> <a href="<?php base_url('pengaturan/kalender');?>">Kalender</a></li>

             </ol>

		</section>

		

		<!-- Main content -->

        <section class="content p-l-r-15">

            <div class="row">			

                <div class="col-md-12">
                	<?php echo $this->session->flashdata('pesan'); ?>
                    <div class="panel">

                        <div class="panel-heading">

							<h4 class="panel-title">

                                <i class="ti-calendar"></i> Pengaturan Kalender

                            </h4>	
                             <span class="pull-right">
	                                <i class="fa fa-fw ti-angle-up clickable"></i>
	                                <i class="fa  fa-fw ti-close removepanel clickable"></i>
                            </span>

                        </div>

						<div class="panel-body">

							<div class="row">

								<div class="col-xs-12">

									<div class="row">

										<div class="col-md-10">

											<h4><p>1. Silakan edit kalender kerja berikut sesuai dengan kebijakan perusahaan Anda. Pengaturan kalender ini akan mempengaruhi catatan kehadiran. Anda dapat mengubah pengaturan ini kapan saja.</p></h4>

										</div>

										<div class="col-md-2">

											<div class="pull-right">

												<button type="button" id="myBtn" class="btn btn-sm btn-primary"  data-target="#info" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>

											</div>

										</div>

									</div>

								</div>

							</div></br>

							

							<div class="row">

								<div class="col-xs-12">

									<div class="row">

										<div class="col-md-6">

											<form class="form-inline">

												<div class="form-group">

													<label for="input-text" class="col-md-4 control-label">Tahun</label>

													<div class="col-md-8">

														<select name="tahun" class="form-control" style="width:300px">

															<option value="2017">2017</option>

															<option value="2018">2018</option>

														</select>

													</div>

												</div>

											</form>

										</div>

										

										<div class="row">

											<div class="col-md-6">

												<div class="pull-right">

													<form class="form-inline">

														<div class="col-md-12">

															<a href="" class="btn btn-sm btn-primary"  data-toggle="modal" data-target="#add_event"><i class="fa ti-plus" aria-hidden="true"></i> Tambah Event</a>

															<a href="<?php echo site_url('pengaturan/versikalender');?>" class="ladda-button btn btn-primary btn-sm button_normal"><i class="fa ti-calendar" aria-hidden="true"></i> Lihat Kalender</a>

														</div>

													</form>

												</div>

											</div>

										</div>

									</div>

								</div>

							</div></br>

							

							<div class="row">

								<div class="col-xs-12">

									<div class="table-responsive">

										<table class="table table-striped">

											<thead style="background:#F3F7F9">

												<tr class="filters">

													<th style="width:20%">Tanggal</th>

													<th>Nama Event</th>

													<th>Tipe</th>

													<th style="text-align:center">Aksi</th>

												</tr>

											</thead>

											<tbody style="height:150px">

											<?php 

											if(sizeof($data_kal)>0){

												for($i=0;$i<sizeof($data_kal);$i++){

													$id=$data_kal[$i]["id_event"];

													$tipe=$data_kal[$i]["tipe_event"];

													$tgl=$data_kal[$i]["tanggal"];

													$nm=$data_kal[$i]["nama_event"];

													if($tipe==1){$tp="Libur Nasional";}elseif($tipe==2){$tp="Cuti Bersama";}else{$tp="Event Non Libur";}

													echo "<tr>

															<td>".$tgl."</td>

															<td>".$nm."</td>

															<td>".$tp."</td>

															<td style='text-align:center'>

																<a href='".base_url('pengaturan/editkalender?id='.$id)."' class='btn-xs btn-warning'><i class='fa fa-fw ti-pencil text-default actions_icon ' title='Edit'></i></a>&nbsp;&nbsp;

																<a href='".base_url('pengaturan/deletekalender?id='.$id)."' class='btn-xs btn-danger'><i class='fa fa-fw ti-trash text-default actions_icon' data-toggle='tooltip' data-placement='top' title data-original-title='Delete'></i></a>

																</td>

															</tr>";

												}

											}

											

											?>

											</tbody>

										</table>

									</div>

								</div>

							</div></br>

							

							<div id="add_event" class="modal fade" role="dialog">

								<div class="modal-dialog modal-lg">

									<!-- Modal content-->

									<div class="modal-content">

										<div class="modal-header">

											<button type="button" class="close" data-dismiss="modal">&times;</button>

											<h4 class="modal-title">Tambah Event</h4>

										</div>

										<div class="modal-body">

											<form class="form-horizontal" id="form_add_kalender" method="POST" action="<?php echo base_url('pengaturan/add_kalender');?>">

												<div class="form-group">

													<label class="control-label col-sm-2" for="tgl">Tanggal</label>

													<div class="col-md-9">

														<div class="input-group">

															<div class="input-group-addon">

																<i class="fa fa-fw ti-calendar"></i>

															</div>

															<input type="text" class="form-control" id="fromdate" placeholder="Format Tanggal DD/MM/YYYY"/>

														</div>

													</div>

												</div>

												<div class="form-group">

													<label class="control-label col-md-2" for="nm_event">Nama Event</label>

													<div class="col-md-9">

														<input type="text" placeholder="Nama hari Event" class="form-control" id="nama_event" name="nama_event">

													</div>

												</div>

												<div class="form-group">

													<label class="control-label col-md-2" for="nm_event">Tipe</label>

													<div class="col-md-9">

														<select id="dynamic"  name="tipe" class="form-control" tabindex="-1" required="">

																<option value="">- Pilihan -</option>

																<option value="1">Libur Nasional</option>

																<option value="2">Cuti Bersama</option>

																<option value="3">Event Non Libur</option>

															</select>

														</div>

												</div>

												<hr>

												<div class="row">

													<div class="col-md-11">

														<div class="pull-right">

															<button type="submit" class="btn btn-sm btn-primary"><span class="ti-save"></span> Submit</button>

															<button class="btn btn-sm btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Batal</button>

														</div>

													</div>

												</div>

											</form>

										</div>

									</div>

								</div>

							</div>


							<div id="info" class="modal fade animated" role="dialog">

								<div class="modal-dialog">

									<div class="modal-content">

										<div class="modal-body">

											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>

											<p class="text-center">

												Pengaturan ini untuk informasi, kalender ini akan mempengaruhi catatan kehadiran. Anda dapat mengubah pengaturan ini kapan saja.

											</p>

										</div>

									</div>

								</div>

							</div>

					

						</div>

					</div>

				</div>

			</div>

            <!-- row-->

        </section>

		<div class="background-overlay"></div>

   	
	<script> 
		$('#notifications').slideDown('slow').delay(3500).slideUp('slow');
	</script>

<!-- Sweet Alert -->

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/sweetalert2/js/sweetalert2.min.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/sweetalert.js"></script>



	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/jquery.dataTables.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/dataTables.bootstrap.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/datatables_custom.js"></script>

	<!-- Sweet Alert -->

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/sweetalert2/js/sweetalert2.min.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/sweetalert.js"></script>
