	<!-- Sweet Alert -->
	<link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/sweetalert2/css/sweetalert2.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom_css/sweet_alert2.css">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>Penggajian</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url('dashboard');?>">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li> <a href="<?php echo site_url('pengaturan');?>">Pengaturan</a></li>
				<li> <a href="<?php echo site_url('pengaturan/penggajian');?>">Penggajian</a></li>
				<li> Tambah Penggajian </li>
            </ol>
		</section>
		<!-- Main content -->
        <section class="content p-l-r-15">
            <div class="row">			
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
							<h4 class="panel-title">
                                <i class="ti-briefcase"></i> Tambah Penggajian
                            </h4>	
                        </div>
						<div class="panel-body" >
							<div class="row">
								<div class="col-xs-12">
									<h4>Form Tambah Pengaturan Penggajian</h4>
								</div></br>
								<div class="col-xs-12">
									<div class="panel">
										<div class="panel-body" style="background:#f5f5f5">
											<div class="row">
												<div class="col-xs-12">
													<div class="row">
														<div class="col-md-10">
															<h4>1. Masukkan informasi umum mengenai Slip Gaji ini:</h4>
														</div>
														<div class="col-md-2">
															<div class="pull-right">
																<button type="button" id="myBtn1" class="btn btn-sm btn-default pull-right" data-target="#info" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>
															</div>
														</div>
													</div>
												</div>
											</div></br>
											
											<div class="row">
												<div class="col-xs-12">
													<form class="form-horizontal" action="#" method="post">
														<div class="form-group">
															<label for="" class="control-label col-md-3">Nama Slip Gaji</label>
															<div class="col-md-9">
																<input class="form-control" type="text" name="nama_slip_gaji" id="nama_slip_gaji" placeholder="Nama Slip Gaji" required="">
															</div>
														</div>
														<div class="form-group">
															<label for="" class="control-label col-md-3">Periode</label>
															<div class="col-md-9">
																<select class="form-control" name="Periode" id="Periode" required="">
																	<option selected="selected" value="">Tetap (Gaji Bulanan, Mingguan, N Harian</option>
																	<option value="">Tidak Tetap (THR atau bonus lebih dari 31 hari</option>
																</select>
															</div>
														</div>
														<div class="form-group">
															<label for="" class="control-label col-md-3">Tanggal Slip Sebelumnya</label>
															<div class="col-md-9">
																<div class="input-group">
																	<input type="text" class="form-control pull-right" data-language='en' id="my-element" />
																	<div class="input-group-addon">
																		<i class="fa fa-fw ti-calendar"></i>
																	</div>
																</div>
															</div>
														</div>
														<div class="form-group">
															<label for="" class="control-label col-md-3">Untuk komponen pendapatan tergantung kehadiran, tentukan hari absensi terakhir yang masuk hitungan gaji:</label>
															<div class="col-md-9">
																<select class="form-control" name="Periode" id="Periode" required="">
																	<option value=""></option>
																	<option value="">0 Hari sebelum akhir periode</option>
																	<option value="">1 Hari sebelum akhir periode</option>
																	<option value="">2 Hari sebelum akhir periode</option>
																	<option value="">3 Hari sebelum akhir periode</option>
																	<option value="">4 Hari sebelum akhir periode</option>
																	<option value="">5 Hari sebelum akhir periode</option>
																	<option value="">6 Hari sebelum akhir periode</option>
																	<option value="">7 Hari sebelum akhir periode</option>
																	<option value="">8 Hari sebelum akhir periode</option>
																	<option value="">9 Hari sebelum akhir periode</option>
																	<option value="">10 Hari sebelum akhir periode</option>
																</select>
															</div>
														</div>
														<div class="form-group">
															<div class="col-md-12">
																<div class="pull-right">
																	<button type="submit" class="btn btn-labeled btn-info">
																		<span class="btn-label">
																			<i class="ti-save"></i>
																		</span> Simpan
																	</button>
																	<button type="button" class="btn btn-labeled btn-danger" onclick="document.location='<?php echo site_url('pengaturan/penggajian'); ?>'">
																		<span class="btn-label">
																			<i class="ti-close"></i>
																		</span> Batal
																	</button>
																</div>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div></br>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="row">
										<div class="col-md-10">
											<h4>2. Komponen pendapatan apa saja yang terdapat dalam Slip Gaji ini? (selain tunjangan PPh 21 dan BPJS)</h4>
										</div>
										<div class="col-md-2">
											<div class="pull-right">
												<button type="button" id="myBtn2" class="btn btn-sm btn-default pull-right" data-target="#info2" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="pull-right">
										<a href="<?php echo site_url('pengaturan/tambahkomponen_pend'); ?>" class="btn btn-info btn-sm" data-style="Komponen Pendapatan">
											<i class="fa ti-plus" aria-hidden="true"></i> Komponen Pendapatan
										</a>
									</div>
								</div>
							</div></br>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="table-responsive">
										<table class="table table-striped" id="table">
											<thead style="background:#e9f0d8">
												<tr class="filters">
													<th style="width:40%">Nama Pendapatan</th>
													<th style="width:40%">Tipe</th>
													<th style="width:20%;text-align:center">Aksi</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td></td>
													<td></td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-file text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Detail"></i></a>&nbsp;&nbsp;
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Delete"></i></a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div></br>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="row">
										<div class="col-md-10">
											<h4>3. Komponen potongan apa saja yang terdapat dalam Slip Gaji ini? (selain tunjangan PPh 21 dan BPJS)</h4>
										</div>
										<div class="col-md-2">
											<div class="pull-right">
												<button type="button" id="myBtn3" class="btn btn-sm btn-default pull-right" data-target="#info3" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="pull-right">
										<a href="<?php echo site_url('pengaturan/tambahkomponen_pot'); ?>" class="btn btn-info btn-sm" data-style="Komponen Potongan">
											<i class="fa ti-plus" aria-hidden="true"></i> Komponen Potongan
										</a>
									</div>
								</div>
							</div></br>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="table-responsive">
										<table class="table table-striped" id="table">
											<thead style="background:#e9f0d8">
												<tr class="filters">
													<th style="width:40%">Nama Potongan</th>
													<th style="width:40%">Tipe</th>
													<th style="width:20%;text-align:center">Aksi</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td></td>
													<td></td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-file text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Detail"></i></a>&nbsp;&nbsp;
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Delete"></i></a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div></br>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="center">
										<a href="<?php echo site_url('pengaturan/penggajian'); ?>" type="button" class="btn btn-labeled btn-danger">
												<span class="btn-label">
												<i class="glyphicon glyphicon-chevron-left"></i>
											</span> Kembali ke Daftar Slip Gaji
										</a>
									</div>
								</div>
							</div></br>
							
							<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="Heading" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
											</button>
											<h4 class="modal-title custom_align" id="Heading">Delete User</h4>
										</div>
										<div class="modal-body">
											<div class="alert alert-warning">
												<span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to
												delete this Account?
											</div>
										</div>
										<div class="modal-footer ">
											<button onClick="document.location='<?php echo site_url('pengaturan'); ?>'" type="submit" class="btn btn-danger">
												<span class="glyphicon glyphicon-ok-sign"></span> Yes
											</button>
											<button type="button" class="btn btn-success" data-dismiss="modal">
												<span class="glyphicon glyphicon-remove"></span> No
											</button>
										</div>
									</div>
								</div>
							</div>
							
							<div id="info" class="modal fade animated" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>
											<p class="text-center">
												Sebagian perusahaan hanya mengeluarkan satu slip gaji, sementara yang lain mengeluarkan beberapa jenis slip gaji. Silakan membuat jenis-jenis Slip Gaji yang tersedia di perusahaan Anda.
											</p>
										</div>
									</div>
								</div>
							</div>
							
							<div id="info2" class="modal fade animated" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>
											<p class="text-center">
												Pengaturan ini untuk menentukan komponen pendapatan/gaji yang terbaca di Slip Gaji.
											</p>
										</div>
									</div>
								</div>
							</div>
							
							<div id="info3" class="modal fade animated" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>
											<p class="text-center">
												Pengaturan ini untuk menentukan komponen pengurang gaji yang terbaca di Slip Gaji.
											</p>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
            <div class="background-overlay"></div>
        </section>
	<!-- airdatepicker -->
	<script src="<?php echo $themes_url; ?>vendors/moment/js/moment.min.js" type="text/javascript"></script>	
    <script src="<?php echo $themes_url; ?>vendors/datetime/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.min.js" type="text/javascript"></script>
    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.en.js" type="text/javascript"></script>
    <script src="<?php echo $themes_url; ?>js/custom_js/advanceddate_pickers.js"></script>
	
	<!-- Sweet Alert -->
	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/sweetalert2/js/sweetalert2.min.js"></script>
	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/sweetalert.js"></script>