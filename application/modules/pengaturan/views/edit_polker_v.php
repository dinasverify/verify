		<!--smoott-->   
		<link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">
		<link href="<?php echo $themes_url; ?>vendors/hover/css/hover-min.css" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo $themes_url; ?>vendors/laddabootstrap/css/ladda-themeless.min.css">
		<link href="<?php echo $themes_url; ?>css/buttons_sass.css" rel="stylesheet">
		<link href="<?php echo $themes_url; ?>css/advbuttons.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/datedropper/datedropper.css">

		<link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/timedropper/css/timedropper.css">

		<link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/jquerydaterangepicker/css/daterangepicker.min.css">
		
		<section class="content-header">

            <h1>Pola dan Jadwal Kerja Karyawan</h1>

            <ol class="breadcrumb">

                <li>

                    <a href="<?php echo('dashboard');?>">

                        <i class="fa fa-fw ti-home"></i> Dashboard

                    </a>

                </li>

                <li> <a href="<?php base_url('pengaturan');?>">Pengaturan</a></li>

				<li> <a href="<?php base_url('pengaturan/polker');?>">Pola dan Jadwal Kerja Karyawan</a></li>

				<li> Tambah Pola Kerja Karyawan </li>

            </ol>

		</section>

		<!-- Main content -->

        <section class="content p-l-r-15">

            <div class="row">			

                <div class="col-md-12">

                    <div class="panel">

                        <div class="panel-heading">

							<h4 class="panel-title">

                                <i class="ti-calendar"></i> Tambah Pola dan Jadwal Kerja Karyawan

                            </h4>	

                        </div>

						<div class="panel-body">

							<div class="col-md-12">

								<form class="form-horizontal" action="<?php echo base_url('pengaturan/updatepolker?id='.$id_pola)?>" method="post">

									<div class="form-group">

										<label for="" class="control-label col-md-2">Nama Pola Kerja</label>

										<div class="col-md-8">

											<input type="text" class="form-control" name="nama_polker" id="nama_polker" value="<?php echo $pola;?>"style="width:50%" required="">

										</div>

									</div>

									<div class="form-group">

										<label for="" class="control-label col-md-2">Jumlah hari Pola Kerja</label>
										<div class="col-md-6">
											<div class="input-group">
												<input type="number" min="0" class="form-control pull-right" data-language="en" name="jumlahharipola" value="<?php echo $jumlah_hari_kerja;?>" required="" placeholder="0">
												<div class="input-group-addon">
													<i class=""> Hari</i>
												</div>
											</div>
										</div>
										

									</div>

									<div class="form-group">

										<label for="" class="control-label col-md-2">Jumlah Libur</label>


										<div class="col-md-6">
											<div class="input-group">
												<input type="number" value="<?php echo $jumlah_libur;?>" min="0" class="form-control pull-right" data-language="en" name="jumlahlibur" required="" placeholder="0">
												<div class="input-group-addon">
													<i class=""> Hari</i>
												</div>
											</div>
										</div>

									</div>

									<div class="form-group">

										<label for="" class="control-label col-md-2">Lama Pola</label>


										<div class="col-md-6">
											<div class="input-group">
												<input type="number" value="<?php echo $lama_pola;?>" min="0" class="form-control pull-right" data-language="en" name="lamapola" required="" placeholder="0">
												<div class="input-group-addon">
													<i class=""> Hari</i>
												</div>
											</div>
										</div>

									

									</div>

									<div class="form-group">

										<label for="" class="control-label col-md-2">Jadwal Masuk</label>

										<div class="col-md-6">
											<input type="text" id="start_time" value="<?php echo $jadwal_masuk;?>" class="form-control pull-right" data-language="en" name="jadwalmasuk" required="" placeholder="HH:MM">
																				
										</div>
									</div>

									<div class="form-group">

										<label for="" class="control-label col-md-2">Jadwal Pulang</label>

										<div class="col-md-6">
												<i class="fa fa-fw ti-time"> </i>
											<div class="input-group">
												<input type="text" class="form-control pull-right" id="end_time" value="<?php echo $jadwal_keluar; ?>" name="jadwalpulang"  required="" placeholder="HH:MM">
											</div>
											
										</div>
										
									</div>

									<div class="form-group">

										<label for="" class="control-label col-md-2">Toleransi Terlambat</label>

										<div class="col-md-6">
											<div class="input-group">
												<input type="number" class="form-control pull-right" name="toleransitelat" id="waktu_tol" value="<?php echo $toleransi_late;?>"   required="" placeholder="0">
												<div class="input-group-addon">
													<i class=""> Menit</i>
												</div>
											</div>
										</div>

									</div>

									<hr>

									<div class="form-group">

										<div class="col-md-12">

											<div class="pull-right">

												<button type="submit" name="update" class="btn btn-labeled btn-primary" >

													<span class="btn-label">

														<i class="ti-save"></i>

													</span> Update

												</button>

												<button type="button" class="btn btn-labeled btn-danger" onclick="document.location='<?php echo site_url('pengaturan/polker'); ?>'">

													<span class="btn-label">

														<span class="glyphicon glyphicon-remove"></span>

													</span> Batal

												</button>

											</div>

										</div>

									</div>

								</form>

							</div>

							

							

						</div>

					</div>

				</div>

			</div>

            <div class="background-overlay"></div>

        </section>

	<!-- airdatepicker -->

	<script src="<?php echo $themes_url; ?>vendors/moment/js/moment.min.js" type="text/javascript"></script>	

    <script src="<?php echo $themes_url; ?>vendors/datetime/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.min.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.en.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>js/custom_js/advanceddate_pickers.js"></script>

		<!-- bootstrap time picker -->

	<script src="<?php echo $themes_url; ?>vendors/clockpicker/js/bootstrap-clockpicker.min.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/datedropper/datedropper.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/timedropper/js/timedropper.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>js/custom_js/datepickers.js" type="text/javascript"></script> 

