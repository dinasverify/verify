				<!--smoott-->   
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">
    <link href="<?php echo $themes_url; ?>vendors/hover/css/hover-min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $themes_url; ?>vendors/laddabootstrap/css/ladda-themeless.min.css">
    <link href="<?php echo $themes_url; ?>css/buttons_sass.css" rel="stylesheet">
    <link href="<?php echo $themes_url; ?>css/advbuttons.css" rel="stylesheet">
	<!-- DataTable css -->
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/datatables/css/dataTables.bootstrap.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom_css/datatables_custom.css">
		
		<!-- Content Header (Page header) -->
		<section class="content-header">
           <h1>Pengaturan Penggajian</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php base_url('dashboard');?>">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li> <a href="<?php base_url('pengaturan');?>">Pengaturan</a></li>
				 <li> <a href="<?php base_url('pengaturan/bagian');?>">Penggajian</a></li>
             </ol>
		</section>
		<!-- Main content -->
        <section class="content p-l-r-15">
            <div class="row">			
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
							<h4 class="panel-title">
                                <i class="ti-briefcase"></i> Pengaturan Penggajian
                            </h4>		
                             <span class="pull-right">
	                                <i class="fa fa-fw ti-angle-up clickable"></i>
	                                <i class="fa  fa-fw ti-close removepanel clickable"></i>
                            </span>
                        </div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12">
									<?php echo $this->session->flashdata('pesan'); ?>
									<div class="col-md-10">
										<h4>1. Komponen pendapatan apa saja yang tersedia di perusahaan Anda (selain tunjangan PPh 21 dan BPJS)?</h4>
									</div>
									<div class="col-md-2">
										<div class="pull-right">
											<button type="button" id="myBtn1" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button>
										</div>
									</div>
								</div>
							</div></br>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="pull-right">
										<a href="<?php echo site_url('pengaturan/tambahkomponen_pend');?>"><button class="btn btn-sm btn-primary" data-style="Nama Pendapatan" >
											<i class="fa ti-plus" aria-hidden="true"></i> Tambah Pendapatan
										</button></a>
									</div>
								</div>
							</div></br>
							<div class="row">
								<div class="col-xs-12">
									<div class="table-responsive">
										<table class="table table-striped" id="table_karyawan">
											<thead style="background:#e9f0d8">
												<tr class="filters">
													<th style="width:3%">No</th>
													<th style="width:20%">Tunjangan</th>
													<th>Tipe Pendapatan</th>
													<th>Nilai</th>
													<th>Kena PPh 21</th>
													<th>Keterangan</th>
													<th>Status</th>
													<th style="text-align:center">Aksi</th>
												</tr>
											</thead>
											<tbody>
												<?php
												if(sizeof($data_set_sal)>0){
													$no = 1;
													for($i=0;$i<sizeof($data_set_sal);$i++){
														$id_pen 	= $data_set_sal[$i]['id_kom_pend'];
														$nama_pen 	= $data_set_sal[$i]['nama_pendapatan'];
														$tp 		= $data_set_sal[$i]['tipe_pendapatan'];
														if($tp == 0){$tipe ="Tidak Ada";}elseif($tp == 7){$tipe = "Gaji Pokok";}elseif($tp == 10){$tipe="Tergantung Kehadiran";}
														$nilai 		= $data_set_sal[$i]['nilai'];
														$pph 		= $data_set_sal[$i]['is_pph21'];
														if($pph==1){$pph21="Ya";}else{$pph21="Tidak";}
														$st 		= $data_set_sal[$i]['status'];
														if($st==1){$cek="checked";}else{$cek="";}
														//$setting = $data_set_sal[$i]['setting'];
														//if($setting==1){$set="<a href='#' class='btn btn-default btn-sm'>Default</a>";}else{$set="<a href='#'><i class='fa fa-fw ti-pencil text-default actions_icon' data-toggle='tooltip' data-placement='top' title data-original-title='Edit'></i></a>&nbsp;&nbsp;
														//<a href='#' data-toggle='modal' data-target='#delete'><i class='fa fa-fw ti-trash text-default actions_icon' data-toggle='tooltip' data-placement='top' title data-original-title='Delete'></i></a>";}
														$tipea1 	= $data_set_sal[$i]['tipe_a1'];
														echo "<tr>
																<td>".$no++."</td>
																<td>".$nama_pen."</td>
																<td>".$tipe."</td><td>".number_format($nilai)."</td><td>".$pph21."</td>
																<td>".$tipea1."</td><td style='text-align:center'>
																	<input type='checkbox' id='toggle_real' name='my-checkbox' data-size='small' ".$cek." readonly>
																</td>
																<td>
																	<a class='btn-xs btn-warning' href='".site_url('pengaturan/edittunjangan/'.$id_pen)."'><i class='fa fa-fw ti-pencil actions_icon' data-toggle='tooltip' data-placement='top' title data-original-title='Edit'></i></a>
																	<a class='btn-xs btn-danger' href='".site_url('pengaturan/hapustunjangan/'.$id_pen)."'><i class='fa fa-fw ti-trash actions_icon' data-toggle='tooltip' data-placement='top' title data-original-title='Hapus'></i></a>
																</td>
															</tr>";
													}
												}
											?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							</br>
							<div class="row">
								<div class="col-xs-12">
									<div class="col-md-10 col-md-offset-1 noteInfo">
										<div class="table-responsive">
											<table class="table table-bordered">
												<thead style="background:#34586e;color:white">
													<tr>
														<th>
															Catatan : Bagi personalia yang baru masuk atau berhenti bekerja di tengah periode penggajian, perhitungan gaji pokok dan tunjangan tetap proposional dihitung berdasarkan jumlah hari kalender yang dilalui personalia tersebut dibagi dengan total hari kalender dalam suatu periode penggajian.
														</th>
													</tr>
												</thead>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<?php echo $this->session->flashdata('pesan1'); ?>
									<br>
									<div class="col-md-10">
										<h4>2. Golongan Tunjangan sesuai dengan Jabatan</h4> <!-- (selain potongan PPh 21, Premi BPJS dan Unpaid Leave)-->
									</div>
									<div class="col-md-2">
										<div class="pull-right">
											<button type="button" id="myBtn2" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button>
										</div>
									</div>
								</div>
							</div></br>
							<div class="row">
								<div class="col-xs-12">
									<div class="pull-right">
										<a href="<?php echo site_url('pengaturan/tambahitempendapatan');?>"><button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modaladd_pot" data-style="Nama Potongan">
											<i class="fa ti-plus" aria-hidden="true"></i> Tambah Grup Gaji Pendapatan
										</button></a>
									</div>
									<div class="pull-left">
										<div class="box-title">
										<form method="post" action="<?php echo site_url("pengaturan/penggajian"); ?>">
											&nbsp; &nbsp;<h4> Pilih Level Jabatan : </h4> 
											<select class="form-control" name="idgrup" onchange="this.form.submit();">
												<option value="">Pilih level Jabatan</option>
												<?php $level = 1;
														echo '<option value="0">Semua Level Jabatan</option>';
													foreach ($grupgaji as $data) {
														echo "<option value=".$data['id_grup'].">".$level++." ".$data['nama_grup']."</option>";
													}
												?>	
											</select>
										</form>	
										</div>
									</div>
								</div>
							</div></br>
							<div class="row">
								<div class="col-xs-12">
									<div class="table-responsive">
										<table class="table table-striped" id="sample_3">
											<thead style="background:#e9f0d8">
												<tr class="filters">
													<th style="width:3%">No</th>
													<th style="width:20%">Nama Jabatan</th>
													<th style="width:2%">ID Tunjangan</th>
													<th style="width:20%">Nama Tunjangan Pendapatan</th>
													<th style="width:15%">Nilai</th>
													<th style="width:10%">Keterangan</th>
													<th style='text-align:center;width:10%'>Aksi</th>
												</tr>
											</thead>
											<tbody>
												<?php
												if(sizeof($itempendapatan)>0){
													$no = 1;
													for($i=0;$i<sizeof($itempendapatan);$i++){
														$id		 	= $itempendapatan[$i]['id_item_penggajian'];
														$id_tun 	= $itempendapatan[$i]['id_grup'];
														$nama_grup 	= $itempendapatan[$i]['nama_grup'];
														$id_kom_pend= $itempendapatan[$i]['id_kom_pend'];
														$nama_pend 	= $itempendapatan[$i]['nama_pendapatan'];
														$status 	= $itempendapatan[$i]['status'];
														$nilai 		= $itempendapatan[$i]['nilai'];
														if($status == 1){
															$info 	= "";
															$uang 	= $nilai;
														}else{
															$info 	= "<h5><span style='width:5%' class='label label-sm label-danger'>Tidak Ada</span></h5>";
															$uang 	= 0;
														}
														echo "<tr>
																<td>".$no++."</td>
																<td>".$nama_grup."</td>
																<td>".$id_kom_pend."</td>
																<td>".$nama_pend."</td>
																<td>".number_format($uang)."</td>
																<td>".$info."</td>
																<td style='text-align:center'>
																	<a class='btn-xs btn-warning' href=".site_url('pengaturan/editpendapatanperjabatan?id='.$id)."><i class='fa fa-fw ti-pencil text-default actions_icon' data-toggle='tooltip' data-placement='top' title data-original-title='Edit'></i></a>&nbsp;&nbsp;
																	<a class='btn-xs btn-danger' href=".site_url('pengaturan/deleteitempenggajian/'.$id)."><i class='fa fa-fw ti-trash text-default actions_icon' data-toggle='tooltip' data-placement='top' title data-original-title='Delete'></i></a>
																</td>
															</tr>";
													}
												}
											?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							</br></br>
							
							<table id="user" class="table table-striped">
                                <tbody>
								<tr>
                                    <td class="table_simple"></td>
                                    <td><a href="<?php echo site_url('pengaturan/editaturangaji');?>"><button class="btn btn-sm btn-primary">
											<i class="fa fa-edit" aria-hidden="true"></i> Perubahan Aturan
										</button></a></td>
                                </tr>
                                <tr>
                                    <td class="table_simple"><h4>5. Apakah perusahaan Anda menerapkan penghitungan lembur? </h4></td>
                                    <td><button  type="button" id="myBtn4" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button> </td>
                                </tr>
                                <tr>
                                    <td  colspan='2'><i class="fa fa-check"></i><?php echo " ".$status;?></td>
								</tr>
                                <tr>
									<td style="background:#34586e;color:white">
										Catatan : </br>Lompati pertanyaan no. 4 jika Anda memilih 'Tidak' pada pertanyaan no. 3.
									</td><td></td>
                                </tr>
                                <tr>
                                    <td><h4>6. Pilih formula yang Anda gunakan untuk menghitung uang lembur:</h4></td>
									<td><button  type="button" id="myBtn4" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button> </td>
                                </tr>
                                <tr>
									<td colspan="2"><i class="fa fa-check"> Nominal Jumlah Rupiah Per Jam: <?php echo $nominal;?></i></td>
								</tr>
                                <tr>
                                    <td><h4>7. Agar menerima Tunjangan Hari Raya (THR), berapa lama seorang personalia harus melalui masa kerja perusahaan Anda? </h4></td>
									<td><button  type="button" id="myBtn4" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button> </td>
								</tr>
								<tr>
									<td colspan="2"><i class="fa fa-check"></i><?php echo " ".$mindapetthr; ?> Bulan</td>
								</tr>
                                <tr>
									<td><h4>8. Berapa besar THR yang Anda bayarkan untuk tiap personalia di perusahaan Anda? </h4></td>
									<td><button  type="button" id="myBtn4" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button> </td>
								</tr>  
								<tr>
									<td colspan="2"><i class="fa fa-check"></i><?php 
										if($besaranthr == 26){
											echo " Gaji Pokok";
										}elseif($besaranthr == 27){
											echo " Gaji Pokok + Tunjangan Tetap";
										}
									?></td>
								</tr>								
								<tr>
									<td><h4>9. Untuk personalia yang bekerja kurang dari satu tahun, bagaimanakah cara Anda menentukan besaran THR untuknya?</h4></td>
									<td><button  type="button" id="myBtn4" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button> </td>
								</tr>
								<tr>
									<td colspan="2"><i class="fa fa-check"></i>
										<?php 
										if($thrminkurangthn == 29)
										{
											echo "Berdasarkan Jumlah Hari Kalender: Aantara Tanggal Ia Mulai Bekerja Hingga Tanggal Pembuatan THR Dibagi 365 Hari";
										}elseif($thrminkurangthn == 30){
											echo "Berdasarkan jumlah bulan: antara bulan ia mulai bekerja dengan bulan pembuatan";
										}
									?>
									</td>
								</tr>
								<tr>
									<td><h4>10. Bagaimanakah kebijakan perusahaan untuk menentukan besaran gaji bagi personalia yang mengalami perubahan karir (contoh: promosi) di tengah periode penggajian?</h4></td>
									<td><button  type="button" id="myBtn4" class="btn btn-md btn-default pull-right" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"><i class="fa fa-question"></i></button> </td>
								</tr>
								<tr>
                                   <td colspan="2"><i class="fa fa-check"></i>
								   <?php 
										if($rubahkarir == 32){
											echo "Proporsional. Menggabungkan perhitungan pro rata gaji lama dan gaji baru";
										}elseif($rubahkarir == 33){
											echo "Hanya nilai gaji baru saja yang berlaku";
										}
									?>
								   </td>
                                </tr>
                                </tbody>
                            </table>	
							<div id="info" class="modal fade animated" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>
											<p class="text-center">
												Klik tombol detail untuk menyetujui atau menolak pengajuan.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="background-overlay"></div>
        </section>
	<script> 
		$('#notifications').slideDown('slow').delay(3500).slideUp('slow');
	</script>
<!-- Sweet Alert -->

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/sweetalert2/js/sweetalert2.min.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/sweetalert.js"></script>



	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/jquery.dataTables.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/dataTables.bootstrap.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/datatables_custom.js"></script>

