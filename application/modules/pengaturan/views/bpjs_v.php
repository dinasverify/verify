	<!--smoott-->   
	<link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">
	<link href="<?php echo $themes_url; ?>vendors/hover/css/hover-min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo $themes_url; ?>vendors/laddabootstrap/css/ladda-themeless.min.css">
	<link href="<?php echo $themes_url; ?>css/buttons_sass.css" rel="stylesheet">
	<link href="<?php echo $themes_url; ?>css/advbuttons.css" rel="stylesheet">
	<!-- Sweet Alert -->

	<link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/sweetalert2/css/sweetalert2.min.css"/>

    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom_css/sweet_alert2.css">

	

	<!-- Calendar -->

	<link href="<?php echo $themes_url; ?>vendors/fullcalendar/css/fullcalendar.css" rel="stylesheet" type="text/css"/>

    <link href="<?php echo $themes_url; ?>vendors/fullcalendar/css/fullcalendar.print.css" rel="stylesheet" media='print' type="text/css">

    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">

    <link href="<?php echo $themes_url; ?>css/calendar_custom.css" rel="stylesheet" type="text/css"/>

	

	<!-- Daterange Picker -->

    <link href="<?php echo $themes_url; ?>vendors/daterangepicker/css/daterangepicker.css" rel="stylesheet" type="text/css"/>

    <link href="<?php echo $themes_url; ?>vendors/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>

    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/datedropper/datedropper.css">

    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/jquerydaterangepicker/css/daterangepicker.min.css">

		<!-- Content Header (Page header) -->

		<section class="content-header">

            <h1>BPJS</h1>

            <ol class="breadcrumb">

                <li>

                    <a href="<?php echo site_url('dashboard');?>">

                        <i class="fa fa-fw ti-home"></i> Dashboard

                    </a>

                </li>

                <li> <a href="<?php echo site_url('pengaturan');?>">Pengaturan</a></li>

				<li> BPJS </li>

            </ol>

		</section>

		<!-- Main content -->

        <section class="content p-l-r-15">

            <div class="row">			

                <div class="col-md-12">

                    <div class="panel">

                        <div class="panel-heading">

							<h4 class="panel-title">

                                <i class="ti-support"></i> BPJS

                            </h4>	
                            <span class="pull-right">
	                                <i class="fa fa-fw ti-angle-up clickable"></i>
	                                <i class="fa  fa-fw ti-close removepanel clickable"></i>
                            </span>

                        </div>

						<div class="panel-body">

							<div class="row">

								<div class="col-xs-12">
									<?php echo $this->session->flashdata('pesan'); ?>
									<div class="row">

										<div class="col-md-10">

											<h4><p>1. Berapakah Upah Minimum Provinsi (UMP) yang berlaku di perusahaan Anda ?</p></h4>

										</div>

										<div class="col-md-2">

											<div class="pull-right">

												<button type="button" id="myBtn1" class="btn btn-sm btn-default"  data-target="#infoUMP" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>

											</div>

										</div>

									</div>

								</div>

							</div>

							

							<div class="row">

								<div class="col-xs-12">

									<div class="pull-right">
									<button type="button" class="btn btn-labeled btn-primary" onclick="document.location='<?php echo site_url('pengaturan/addump'); ?>'">

											<span class="btn-label">

												<i class="fa ti-plus" aria-hidden="true"></i>

											</span> Tambah UMP

										</button>
										
									</div>

								</div>

							</div></br>

							

							<div class="row">

								<div class="col-xs-12">

									<div class="table-responsive">

										<table class="table table-striped" id="sample_1">

											<thead>

												<tr class="filters">

													<th style="width:20%">No</th>

													<th style="width:20%">Nama UMP</th>

													<th style="width:30%">Nilai</th>

													<th style="width:20%;text-align:center">Aksi</th>

												</tr>

											</thead>

											<tbody>
												<?php $no = 1; foreach($ump as $data){?>
												<tr>	

													<td><?php echo $no++ ?></td>
													
													<td><?php echo $data['nama_ump']; ?></td>

													<td><?php echo number_format($data['nilai']); ?></td>

													<td style="text-align:center">

														<a class="btn-xs btn-warning" 
														href="<?php echo base_url('pengaturan/editump?id='.$data['id_ump']);?>" ><i class="fa fa-fw ti-pencil text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></a>&nbsp;&nbsp;

														<a class="btn-xs btn-danger" href="<?php echo base_url('pengaturan/deleteump/'.$data['id_ump']);?>" class=""><i class="fa fa-fw ti-trash text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Delete"></i></a>

													</td>


												</tr>
												<?php } ?>
											</tbody>

										</table>

									</div>

								</div>

							</div>

							<hr>

							<div class="row">

								<div class="col-xs-12">

									<div class="row">

										<div class="col-md-10">

											<h4><p>2. Apakah perusahaan Anda menerapkan BPJS Ketenagakerjaan?

											<a  data-toggle='modal' data-target='#edit_bpjs_tenaga' ><button type="button" id="myBtn6" class="btn btn-sm btn-primary" ><i class="fa ti-pencil"  data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></button></a></p></h4>

										</div>

										<div class="col-md-2">

											<div class="pull-right">

												

											</div>

										</div>

									</div>

								</div>

							</div>

							<div class="row">

								<div class="col-xs-12">

									<div class="row">

										<div class="col-md-10">

											<h4><p>BPJS Ketenagakerjaan

											</p></h4>

										</div>

										<div class="col-md-2">

											<div class="pull-right">

												<button type="button" id="myBtn5" class="btn btn-sm btn-default"  data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>

											</div>

										</div>

									</div>

								</div>

							</div>

							<div class="row">

								<div class="col-xs-12">

									<div class="table col-md-9">

										<table>

											<tr>

												<td style="width:20px"><i class="fa fa-check"></i></td>

												<td style="width:20px"><?php if($bpjstng == 1){ echo "Ya";}else{ echo"Tidak";}?></td>

											</tr>

										</table>

									</div>

								</div>

							</div>

							<hr>

							<div class="row">

								<div class="col-xs-12">

									<div class="row">

										<div class="col-md-10">

											<h4><p>3. Apakah perusahaan Anda menerapkan BPJS Kesehatan?

											<a  data-toggle='modal' data-target='#edit_bpjs_kesehatan' ><button type="button" id="myBtn6" class="btn btn-sm btn-primary" ><i class="fa ti-pencil"  data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></button></a></p></h4>

										</div>

										<div class="col-md-2">

											<div class="pull-right">

												

											</div>

										</div>

									</div>

								</div>

							</div>

							<div class="row">

								<div class="col-xs-12">

									<div class="row">

										<div class="col-md-10">

											<h4><p>BPJS Kesehatan

											</p></h4>

										</div>

										<div class="col-md-2">

											<div class="pull-right">

												<button type="button" id="myBtn9" class="btn btn-sm btn-default pull-right"  data-target="#infoBPJS" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>

											</div>

										</div>

									</div>

								</div>

							</div>

							<div class="row">

								<div class="col-xs-12">

									<div class="table col-md-9">

										<table>

											<tr>

												<td style="width:20px"><i class="fa fa-check"></i></td>

												<td style="width:20px"><?php if($bpjskes == 1){ echo "Ya";}else{echo"Tidak";}?></td>

											</tr>

										</table>

									</div>

								</div>

							</div>

							<hr>

							<div class="row" style="<?php echo $show; ?>">

								<div class="col-xs-12">

									<div class="table-responsive">

										<table class="table table-striped" id="sample_2">

											<thead style="background:#e9f0d8">

												<tr class="filters">

													<th style="width:2%">No</th>

													<th style="width:25%">Nama Potongan</th>

													<th style="width:20%;text-align:center">Tipe</th>

													<th style="width:15%;text-align:center">Nilai</th>

													<th style="width:10%;text-align:center">Status</th>

													<th style="width:25%;text-align:left">Keterangan</th>

													<th style="width:20%;text-align:center">Aksi</th>

												</tr>

											</thead>

											<tbody>

												<?php

												if(sizeof($data_kom_pot)>0){

													$no = 1;

													for($i=0;$i<sizeof($data_kom_pot);$i++){

														$id_kom 	= $data_kom_pot[$i]['id_kom_pot'];

														$nama_pot 	= $data_kom_pot[$i]['nama_potongan'];

														$tipe_pot 	= $data_kom_pot[$i]['tipe_potongan'];

														if($tipe_pot ==1){ $tp ="BPJS Ketenagakerjaan";}elseif($tipe_pot ==2){

															$tp = "BPJS Kesehatan";

														}else{$tp = "Peraturan Pemerintah";}

														$nilai 		= $data_kom_pot[$i]['nilai'];

														$sta_pot 	= $data_kom_pot[$i]['status'];

														$ket 		= $data_kom_pot[$i]['keterangan'];

														if($id_kom == 9 && $sta_pot == 0 || $id_kom == 10 && $sta_pot == 0){

															$cek = "Tidak";

														}else{

															$cek = "Ya";

														}

														



														echo "<tr>

																<td>".$no++."</td>

																<td>".$nama_pot."</td>

																<td>".$tp."</td>

																<td style='text-align:center'>".number_format($nilai,2)."</td>

																<td style='text-align:center'>

																	 ".$cek."

																</td>

																<td>

																	 ".$ket."

																</td>

																<td style='text-align:center'>

																	<a href=".site_url('pengaturan/editpotongan/'.$id_kom)."><i class='fa fa-fw ti-pencil text-default actions_icon' data-toggle='tooltip' data-placement='top' title data-original-title='Edit'></i></a>&nbsp;&nbsp;

																	<a href=".site_url('pengaturan/hapuspotongan/'.$id_kom)."><i class='fa fa-fw ti-trash text-default actions_icon' data-toggle='tooltip' data-placement='top' title data-original-title='Delete'></i></a>

																</td>

															</tr>";

													}

												}

											?>

											</tbody>

										</table>

									</div>

								</div>

							</div>

							<hr>

							<!-- <div class="row">

								<div class="col-xs-12">

									<div class="col-md-10 col-md-offset-1 noteInfo">

										<div class="table-responsive">

											<table class="table table-bordered">

												<thead style="background:#0088cc;color:white"> --><!--style="background:#34586e;color:white"-->

													<!-- <tr>

														<th>

															Catatan: </br>

															Basis pengali yang digunakan Payroll ini untuk perhitungan BPJS adalah nilai yang lebih besar antara UMP dan Gaji Pokok + Tunjangan Tetap

														</th>

													</tr>

												</thead>

											</table>

										</div>

									</div>

								</div>

							</div>
 -->
							<!--info UMP-->

							<div id="infoUMP" class="modal fade animated" role="dialog">

								<div class="modal-dialog">

									<div class="modal-content">

										<div class="modal-body">

											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>

											<p class="text-center">

												Besar UMP akan mempengaruhi perhitungan BPJS. Jika Gaji Pokok+Tunjangan Tetap kurang dari UMP, maka basis pengali perhitungan iuran BPJS akan menggunakan nilai UMP.

											</p>

										</div>

									</div>

								</div>

							</div>

							<!--info BPJS-->

							<div id="infoBPJS" class="modal fade animated" role="dialog">

								<div class="modal-dialog">

									<div class="modal-content">

										<div class="modal-body">

											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>

											<p class="text-center">

												Pengaturan ini akan mempengaruhi penghitungan gaji.

											</p>

										</div>

									</div>

								</div>

							</div>

							<!--info Metode-->

							<div id="infoMetode" class="modal fade animated" role="dialog">

								<div class="modal-dialog">

									<div class="modal-content">

										<div class="modal-body">

											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>

											<p class="text-center">

												Metode Gross-up yaitu PPh 21 dibayar perusahaan dan dimasukkan sebagai tunjangan penghasilan untuk karyawan. Metode Gross yaitu karyawan membayar sendiri PPh 21-nya. Metode Nett yaitu PPh 21 dibayar perusahaan, tapi tidak dimasukkan sebagai unsur penghasilan karyawan.

											</p>

										</div>

									</div>

								</div>

							</div>

							<div class='modal fade' id='edit_bpjs_tenaga' tabindex='-1' role='dialog' aria-labelledby='edit' aria-hidden='true'>

								<div class='modal-dialog'>

									<div class='modal-content'>

										<div class="modal-body">

										<form class="form-horizontal" id="form_edit_kalender" method="POST" action="<?php echo base_url('pengaturan/editbpjstenaga');?>">

											<div class='modal-header'>

												<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>

												<h4 class='modal-title custom_align' id='Heading'>Edit BPJS Ketenagakerjaan</h4>

											</div>

											<div class='modal-body'>

												<div class='form-group'>

													<h4>Apakah perusahaan Anda menerapkan BPJS Ketenagakerjaan?</h4>

												</div>

												<div class='form-group'>

													<?php

														if($bpjstng == 1){

															echo "<input type='radio' name='radios3' value='1' class='square-blue' checked>

													<label name='Yes' for='Test3_1'>Yes</label>

													<input type='radio' name='radios3' value='0' class='square-blue'>

													<label name='no' for='Test3_1'>No</label>";

														}else{

															echo "<input type='radio' name='radios3' value='1' class='square-blue'>

													<label name='Yes' for='Test3_1'>Yes</label>

													<input type='radio' name='radios3' value='0' class='square-blue'checked>

													<label name='no' for='Test3_1'>No</label>";														}

													?>

													

												</div>

											</div>

											<div class='modal-footer '>

												<button type='submit' name="submitbpjstng" class='btn btn-sm btn-primary'>

													<span class='glyphicon glyphicon-ok-sign'></span> Update

												</button>

											</div>

										</form>

									</div>

									</div>

									<!-- /.modal-content -->

								</div>

							</div>

							<div class='modal fade' id='edit_bpjs_kesehatan' tabindex='-1' role='dialog' aria-labelledby='edit' aria-hidden='true'>

								<div class='modal-dialog'>

									<div class='modal-content'>

										<div class='modal-header'>

											<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>

											<h4 class='modal-title custom_align' id='Heading'>Edit BPJS Kesehatan</h4>

										</div>

										<div class='modal-body'>

											<form class="form-horizontal" id="form_edit_kalender" method="POST" action="<?php echo base_url('pengaturan/editbpjskesehatan');?>">

											<div class='modal-header'>

											<div class='form-group'>

												<h4>Apakah perusahaan Anda menerapkan BPJS Kesehatan?</h4>

											</div>

											<div class='form-group'>

												<?php

														if($bpjskes == 1){

															echo "<input type='radio' name='radios3' value='1' class='square-blue' checked>

													<label name='Yes' for='Test3_1'>Yes</label>

													<input type='radio' name='radios3' value='0' class='square-blue'>

													<label name='no' for='Test3_1'>No</label>";

														}else{

															echo "<input type='radio' name='radios3' value='1' class='square-blue'>

													<label name='Yes' for='Test3_1'>Yes</label>

													<input type='radio' name='radios3' value='0' class='square-blue'checked>

													<label name='no' for='Test3_1'>No</label>";}

													?>

											</div>

										</div>

										<div class='modal-footer'>

											<button type='submit' class='btn btn-sm btn-primary'>

												<span class='glyphicon glyphicon-ok-sign'></span> Update

											</button>

										</div>

									</form>

									</div>

									<!-- /.modal-content -->

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

            <div class="background-overlay"></div>

        </section>

        	<script> 
		$('#notifications').slideDown('slow').delay(3500).slideUp('slow');
	</script>

	<!-- Sweet Alert -->

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/sweetalert2/js/sweetalert2.min.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/sweetalert.js"></script>



	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/jquery.dataTables.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/dataTables.bootstrap.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/datatables_custom.js"></script>



	<!-- airdatepicker -->

	<script src="<?php echo $themes_url; ?>vendors/moment/js/moment.min.js" type="text/javascript"></script>	

    <script src="<?php echo $themes_url; ?>vendors/datetime/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.min.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.en.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>js/custom_js/advanceddate_pickers.js"></script>



	<!-- bootstrap time picker -->

	<script src="<?php echo $themes_url; ?>vendors/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/clockpicker/js/bootstrap-clockpicker.min.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/jquerydaterangepicker/js/jquery.daterangepicker.min.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/datedropper/datedropper.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>js/custom_js/datepickers.js" type="text/javascript"></script>