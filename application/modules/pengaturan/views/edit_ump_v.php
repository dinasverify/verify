				<!--smoott-->   
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">
    <link href="<?php echo $themes_url; ?>vendors/hover/css/hover-min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $themes_url; ?>vendors/laddabootstrap/css/ladda-themeless.min.css">
    <link href="<?php echo $themes_url; ?>css/buttons_sass.css" rel="stylesheet">
    <link href="<?php echo $themes_url; ?>css/advbuttons.css" rel="stylesheet">
	<!-- Content Header (Page header) -->

		<section class="content-header">

            <h1>Tambah UMP Daerah</h1>

            <ol class="breadcrumb">

                <li>

                    <a href="<?php base_url('dashboard');?>">

                        <i class="fa fa-fw ti-home"></i> Dashboard

                    </a>

                </li>

                <li> <a href="<?php base_url('pengaturan');?>">Pengaturan</a></li>

				 <li> <a href="#">Pengaturan BPJS</a></li>
				 <li> Tambah UMP Daerah</li>
             </ol>

		</section>
		<!-- Main content -->

        <section class="content p-l-r-15">

            <div class="row">			

                <div class="col-md-12">
                	<?php echo $this->session->flashdata('pesan'); ?>
                    <div class="panel">

                        <div class="panel-heading">

							<h4 class="panel-title">

                                <i class="ti-calendar"></i> Tambah UMP Daerah

                            </h4>	
                             <span class="pull-right">
	                                <i class="fa fa-fw ti-angle-up clickable"></i>
	                                <i class="fa  fa-fw ti-close removepanel clickable"></i>
                            </span>

                        </div>

						<div class="panel-body">
							<form class="form-horizontal" action="<?php echo base_url('pengaturan/editump?id='.$id);?>" method="post">
								<div class="row">
									<div class="col-md-12">

										<label class="control-label col-md-2" for="nm_event">Nama Daerah UMP</label>

										<div class="col-md-10">

											<input type="text" value="<?php echo $nama_ump; ?>" placeholder="Nama Tempat Daerah" class="form-control" id="nama_event" name="daerah">

										</div>

									</div>
								</div>
						
								<br>

								<div class="row">
									<div class="col-md-12">

										<label class="control-label col-sm-2" for="tgl">Nilai</label>

										<div class="col-md-10">
											<div class="input-group">
												<input type="number" min=0 class="form-control" value="<?php echo $nilai; ?>" name="jmlh_uang" id="jmlh_uang" required="" placeholder="0">

												<div class="input-group-addon">
													<i class=""> Rupiah</i>
												</div>
											</div>
										</div>

									</div>
								</div>
								<hr>
							
								<div class="row">

									<div class="col-md-12">

										<div class="pull-right">

											<button type="submit" name="simpan" class="btn btn-labeled btn-primary" >

													<span class="btn-label">

														<i class="ti-save"></i>

													</span> Update

												</button>

											<button type="button" class="btn btn-labeled btn-danger" onclick="document.location='<?php echo site_url('pengaturan/bpjs'); ?>'">

													<span class="btn-label">

														<span class="glyphicon glyphicon-remove"></span>

													</span> Batal

												</button>

										</div>

									</div>

								</div>

							

						</form>

						</div>

					</div>

				</div>

			</div>

		</div>

            <!-- row-->

        </section>

		<div class="background-overlay"></div>

   	
	<script> 
		$('#notifications').slideDown('slow').delay(3500).slideUp('slow');
	</script>
	
	<!-- airdatepicker -->

	<script src="<?php echo $themes_url; ?>vendors/moment/js/moment.min.js" type="text/javascript"></script>	

    <script src="<?php echo $themes_url; ?>vendors/datetime/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.min.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.en.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>js/custom_js/advanceddate_pickers.js"></script>

		<!-- bootstrap time picker -->

	<script src="<?php echo $themes_url; ?>vendors/clockpicker/js/bootstrap-clockpicker.min.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/datedropper/datedropper.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/timedropper/js/timedropper.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>js/custom_js/datepickers.js" type="text/javascript"></script> 

