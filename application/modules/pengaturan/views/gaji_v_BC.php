	<!-- Sweet Alert -->
	<link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/sweetalert2/css/sweetalert2.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom_css/sweet_alert2.css">
		<!-- Content Header (Page header) -->
		<section class="content-header">
            <h1>Gaji & LTHR</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url('dashboard');?>">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li> <a href="<?php echo site_url('pengaturan');?>">Pengaturan</a></li>
				<li> Gaji & LTHR </li>
            </ol>
		</section>
		<!-- Main content -->
        <section class="content p-l-r-15">
            <div class="row">			
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
							<h4 class="panel-title">
                                <i class="ti-money"></i> Gaji & LTHR
                            </h4>	
                        </div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12">
									<div class="row">
										<div class="col-md-10">
											<h4>1. Komponen pendapatan apa saja yang tersedia di perusahaan Anda (selain tunjangan PPh 21 dan BPJS)?</h4>
										</div>
										<div class="col-md-2">
											<div class="pull-right">
												<button type="button" id="myBtn1" class="btn btn-sm btn-default pull-right" data-target="#info" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="pull-right">
										<a href="<?php echo site_url('pengaturan/gaji'); ?>" class="btn btn-info btn-sm" data-style="Nama Pendapatan">
											<i class="fa ti-plus" aria-hidden="true"></i> Nama Pendapatan
										</a>
									</div>
								</div>
							</div></br>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="table-responsive">
										<table class="table table-striped" id="table">
											<thead style="background:#e9f0d8">
												<tr class="filters">
													<th style="width:20%">Nama Pendapatan</th>
													<th style="width:10%">Tipe</th>
													<th style="width:10%">PPh 21</th>
													<th style="width:10%">Tipe A1</th>
													<th style="width:30%;text-align:center">Status</th>
													<th style="width:20%;text-align:center">Aksi</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Gaji Pokok (Basic Salary)</td>
													<td>Gaji Pokok</td>
													<td>Ya</td>
													<td></td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#" class="btn btn-default btn-sm">Default</a>
													</td>
												</tr>
												<tr>
													<td>Tunjangan Hari Raya (THR)</td>
													<td>THR</td>
													<td>Ya</td>
													<td>Tantiem,Bonus,</br>Gratifikasi,Jasa,</br>Produksi dan THR</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#" class="btn btn-default btn-sm">Default</a>
													</td>
												</tr>
												<tr>
													<td>Uang Lembur (overtime)</td>
													<td>Uang Lembur</td>
													<td>Ya</td>
													<td>Tunjungan Lainnya,Uang</br>Lembur,dsb.</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#" class="btn btn-default btn-sm">Default</a>
													</td>
												</tr>
												<tr>
													<td>Tunjangan Makan</td>
													<td>Tergantung Kehadiran</td>
													<td>Ya</td>
													<td>Tunjungan Lainnya,Uang</br>Lembur,dsb.</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Tunjangan Transportasi</td>
													<td>Tergantung Kehadiran</td>
													<td>Ya</td>
													<td>Tunjungan Lainnya,Uang</br>Lembur,dsb.</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Tunjangan Kehadiran</td>
													<td>Tergantung Kehadiran</td>
													<td>Tidak</td>
													<td></td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Tunjangan Fungsional</td>
													<td>Jumlah Tetap</td>
													<td>Ya</td>
													<td>Tunjungan Lainnya,Uang</br>Lembur,dsb.</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Tunjangan Istri</td>
													<td>Jumlah Tetap</td>
													<td>Tidak</td>
													<td></td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Tunjangan Anak</td>
													<td>Jumlah Tetap</td>
													<td>Tidak</td>
													<td></td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Tunjangan Komunikasi</td>
													<td>Jumlah Tetap</td>
													<td>Tidak</td>
													<td></td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Tunjangan Perjalanan Dinas</td>
													<td>Tergantung Output</td>
													<td>Tidak</td>
													<td></td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Tunjangan Jalan Jalan</td>
													<td>Manual</td>
													<td>Ya</td>
													<td>Tunjungan Lainnya,Uang</br>Lembur,dsb.</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Delete"></i></a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							
							<hr>
							<div class="row">
								<div class="col-xs-12">
									<div class="col-md-10 col-md-offset-1 noteInfo">
										<div class="table-responsive">
											<table class="table table-bordered">
												<thead style="background:#34586e;color:white">
													<tr>
														<th>
															Catatan : Bagi personalia yang baru masuk atau berhenti bekerja di tengah periode penggajian, perhitungan gaji pokok dan tunjangan tetap proposional dihitung berdasarkan jumlah hari kalender yang dilalui personalia tersebut dibagi dengan total hari kalender dalam suatu periode penggajian.
														</th>
													</tr>
												</thead>
											</table>
										</div>
									</div>
								</div>
							</div>
							
							<hr>
							<div class="row">
								<div class="col-xs-12">
									<div class="row">
										<div class="col-md-10">
											<h4>2. Komponen potongan apa saja yang ada di perusahaan Anda (selain potongan PPh 21, Premi BPJS dan Unpaid Leave)?</h4>
										</div>
										<div class="col-md-2">
											<div class="pull-right">
												<button type="button" id="myBtn2" class="btn btn-sm btn-default pull-right" data-target="#info2" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="pull-right">
										<a href="<?php echo site_url(''); ?>" class="btn btn-info btn-sm" data-style="Nama Potongan">
											<i class="fa ti-plus" aria-hidden="true"></i> Nama Potongan
										</a>
									</div>
								</div>
							</div></br>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="table-responsive">
										<table class="table table-striped" id="table">
											<thead style="background:#e9f0d8">
												<tr class="filters">
													<th style="width:25%">Nama Potongan</th>
													<th>Tipe</th>
													<th style="width:25%;text-align:center">Status</th>
													<th style="width:20%;text-align:center">Aksi</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Potongan Lainnya</td>
													<td>Manual</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" title="Lihat Karyawan"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" title="Delete"></i></a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							
							<hr>
							<div class="row">
								<div class="col-xs-12">
									<div class="row">
										<div class="col-md-10">
											<h4>3. Apakah perusahaan Anda menerapkan penghitungan lembur? 
											<button type="button" id="myBtn3" class="btn btn-sm btn-info"><i class="fa ti-pencil" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></button></h4>
										</div>
										<div class="col-md-2">
											<div class="pull-right">
												<button type="button" id="myBtn4" class="btn btn-sm btn-default pull-right" data-target="#info3" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="table col-md-9">
										<table>
											<tr>
												<td style="width:20px"><i class="fa fa-check"></i></td>
												<td style="width:20px">Ya</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
							
							<hr>
							<div class="row">
								<div class="col-xs-12">
									<div class="col-md-10 col-md-offset-1 noteInfo">
										<div class="table-responsive">
											<table class="table table-bordered">
												<thead style="background:#34586e;color:white">
													<tr>
														<th>
															Catatan : </br>Lompati pertanyaan no. 4 jika Anda memilih 'Tidak' pada pertanyaan no. 3.
														</th>
													</tr>
												</thead>
											</table>
										</div>
									</div>
								</div>
							</div>
							
							<hr>
							<div class="row">
								<div class="col-xs-12">
									<div class="row">
										<div class="col-md-10">
											<h4>4. Pilih formula yang Anda gunakan untuk menghitung uang lembur:
											<button type="button" id="myBtn5" class="btn btn-sm btn-info"><i class="fa ti-pencil" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></button></h4>
										</div>
										<div class="col-md-2">
											<div class="pull-right">
												<button type="button" id="myBtn6" class="btn btn-sm btn-default pull-right" data-target="#info4" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="table col-md-9">
										<table>
											<tr>
												<td style="width:20px"><i class="fa fa-check"></i></td>
												<td style="width:300px">Nominal Jumlah Rupiah Per Jam: 100</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
							
							<hr>
							<div class="row">
								<div class="col-xs-12">
									<div class="row">
										<div class="col-md-10">
											<h4>5. Agar menerima Tunjangan Hari Raya (THR), berapa lama seorang personalia harus melalui masa kerja perusahaan Anda? 
											<button type="button" id="myBtn7" class="btn btn-sm btn-info"><i class="fa ti-pencil" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></button></h4>
										</div>
										<div class="col-md-2">
											<div class="pull-right">
												<button type="button" id="myBtn8" class="btn btn-sm btn-default pull-right" data-target="#info5" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="table col-md-9">
										<table>
											<tr>
												<td style="width:20px">1</td>
												<td style="width:50px">Bulan</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
							
							<hr>
							<div class="row">
								<div class="col-xs-12">
									<div class="row">
										<div class="col-md-10">
											<h4>6. Berapa besar THR yang Anda bayarkan untuk tiap personalia di perusahaan Anda?
											<button type="button" id="myBtn9" class="btn btn-sm btn-info"><i class="fa ti-pencil" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></button></h4>
										</div>
										<div class="col-md-2">
											<div class="pull-right">
												<button type="button" id="myBtn10" class="btn btn-sm btn-default pull-right" data-target="#info6" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="table col-md-9">
										<table>
											<tr>
												<td style="width:20px">1</td>
												<td style="width:50px">Kali</td>
												<td style="width:300px">Gaji Pokok + Tunjangan Tetap</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
							
							<hr>
							<div class="row">
								<div class="col-xs-12">
									<div class="row">
										<div class="col-md-11">
											<h4>7. Untuk personalia yang bekerja kurang dari satu tahun, bagaimanakah cara Anda menentukan besaran THR untuknya?
											<button type="button" id="myBtn11" class="btn btn-sm btn-info"><i class="fa ti-pencil" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></button></h4>
										</div>
										<div class="col-md-1">
											<div class="pull-right">
												<button type="button" id="myBtn12" class="btn btn-sm btn-default pull-right" data-target="#info7" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="table col-md-9">
										<table>
											<tr>
												<td style="width:20px"><i class="fa fa-check"></i></td>
												<td style="width:900px">Berdasarkan jumlah bulan: antara bulan ia mulai bekerja dengan bulan pembuatan THR pertama, dibagi 12 (pembulatan ke bawah)</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
							
							<hr>
							<div class="row">
								<div class="col-xs-12">
									<div class="row">
										<div class="col-md-10">
											<h4>8. Bagaimanakah kebijakan perusahaan untuk menentukan besaran gaji bagi personalia yang mengalami perubahan karir (contoh: promosi) di tengah periode penggajian?
											<button type="button" id="myBtn13" class="btn btn-sm btn-info"><i class="fa ti-pencil" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></button></h4>
										</div>
										<div class="col-md-2">
											<div class="pull-right">
												<button type="button" id="myBtn14" class="btn btn-sm btn-default pull-right" data-target="#info8" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="table col-md-9">
										<table>
											<tr>
												<td style="width:20px"><i class="fa fa-check"></i></td>
												<td style="width:500px">Proporsional. Menggabungkan perhitungan pro rata gaji lama dan gaji baru</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
							<hr>
							
							<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="Heading" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
											</button>
											<h4 class="modal-title custom_align" id="Heading">Delete User</h4>
										</div>
										<div class="modal-body">
											<div class="alert alert-warning">
												<span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to
												delete this Account?
											</div>
										</div>
										<div class="modal-footer ">
											<a href="deleted_users.html" class="btn btn-danger">
												<span class="glyphicon glyphicon-ok-sign"></span> Yes
											</a>
											<button type="button" class="btn btn-success" data-dismiss="modal">
												<span class="glyphicon glyphicon-remove"></span> No
											</button>
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
							</div>
							
							<div id="info" class="modal fade animated" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>
											<p class="text-center">
												Gaji pokok, THR dan uang lembur secara default sudah tercantum pada tabel pendapatan ini. Ketiga komponen ini tidak dapat diedit atau dihapus. Silakan menghapus, menambah atau mengubah komponen pendapatan lain.
											</p>
										</div>
									</div>
								</div>
							</div>
							
							<div id="info2" class="modal fade animated" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>
											<p class="text-center">
												Tabel potongan pendapatan ini dapat direvisi sesuai dengan kebijakan perusahaan.
											</p>
										</div>
									</div>
								</div>
							</div>
							
							<div id="info3" class="modal fade animated" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>
											<p class="text-center">
												Anda dapat memilih perhitungan lembur menurut peraturan Pemerintah atau menentukan jumlah uang lembur tetap per jam.
											</p>
										</div>
									</div>
								</div>
							</div>
							
							<div id="info4" class="modal fade animated" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>
											<p class="text-center">
												Anda dapat memilih perhitungan lembur menurut peraturan Pemerintah atau menentukan jumlah uang lembur tetap per jam.
											</p>
										</div>
									</div>
								</div>
							</div>
							
							<div id="info5" class="modal fade animated" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>
											<p class="text-center">
												Menurut Peraturan Menteri Ketenagakerjaan No. 6/2016 tentang Tunjangan Hari Raya (THR) Keagamaan, personalia dengan masa kerja minimal 1 bulan berhak mendapatkan THR.
											</p>
										</div>
									</div>
								</div>
							</div>
							
							<div id="info6" class="modal fade animated" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>
											<p class="text-center">
												Umumnya, perusahaan Indonesia membayar THR sebesar 1 kali gaji pokok. Silakan mengubah pengaturan ini sesuai kebijakan perusahaan Anda.
											</p>
										</div>
									</div>
								</div>
							</div>
							
							<div id="info7" class="modal fade animated" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>
											<p class="text-center">
												Karena THR biasanya sangat ditunggu-tunggu, pastikan Anda menjelaskan penghitungan THR secara transparan kepada personalia.
											</p>
										</div>
									</div>
								</div>
							</div>
							
							<div id="info8" class="modal fade animated" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>
											<p class="text-center">
												Contoh Proporsional: Gaji Karyawan A: Rp. 3.100.000/bulan. Pada tgl 15 bulan April gaji A= Rp 3.600.000/bulan. Gaji prorata bulan April karyawan A: (3.100.000x15/30) + 3.600.000x15/30)= 1.550.000 + 1.800.000 = Rp 3.350.000
											</p>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<div class="background-overlay"></div>
        </section>		
	<!-- Sweet Alert -->
	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/sweetalert2/js/sweetalert2.min.js"></script>
	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/sweetalert.js"></script>