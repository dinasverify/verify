	<link href="<?php echo $themes_url; ?>vendors/daterangepicker/css/daterangepicker.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $themes_url; ?>vendors/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/datedropper/datedropper.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/timedropper/css/timedropper.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/jquerydaterangepicker/css/daterangepicker.min.css">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>Configurasi</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php site_url('dashboard');?>">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li> <a href="<?php site_url('pengaturan');?>">Pengaturan</a></li>
				<li> <a href="<?php site_url('pengaturan/gaji');?>">Configurasi</a></li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content p-l-r-15">
            <div class="row">			
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
							<h4 class="panel-title">
                                <i class="menu-icon fa fa-cogs fa-lg fa-fw"></i> Configurasi
                            </h4>	
                        </div>
						<div class="panel-body">
							<div class="col-xs-12">
								<form method="post" class="form-horizontal" enctype="multipart/form-data" role="form" action="<?php echo base_url('pengaturan/editsetperusahaan');?>">
									<div class="form-group">
										<div class="col-md-12">
											<label for="" class="control-label">Nama Perusahaan :</label>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-7">
											<input class="form-control col-md-7" name="id_perusahaan" placeholder="Isi Nama Perusahaan"  type="hidden" value="<?php  echo $id_perusahaan; ?>">
											<input class="form-control col-md-7" name="namaperusanaan" placeholder="Isi Nama Perusahaan"  type="text" value="<?php  echo $namaperusanaan; ?>">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-7">
											<label for="" class="control-label">Bidang :</label>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-7">
											<input class="form-control col-md-7" name="bidang" placeholder="Bidang Perusahaan"  type="text" value="<?php  echo $bidang; ?>">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											<label for="" class="control-label">Alamat Perusahaan :</label>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-7">
											<input class="form-control col-md-7" name="alamat" placeholder="Isi Alamat Perusahaan" type="text" value="<?php echo $alamat;?>">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											<label for="" class="control-label">Upload Logo Depan :</label>
										</div>
									</div>
									<div class="form-group">
											<div class="col-md-7">
												<input id="input-22" type="file" name="file_front" class="file" accept="image/JPG,image/jpg,image/jpeg,image/png,image/PNG" data-preview-file-type="text" data-preview-class="bg-default">
											</div>
											<h5><p class="text-danger">* ukuran foto 40x300</p></h5>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											<label for="" class="control-label">Upload Logo Belakang :</label>
										</div>
									</div>
									<div class="form-group">
											<div class="col-md-7">
												<input id="input-21" type="file" name="file_back" class="file" accept="image/JPG,image/jpg,image/jpeg,image/png,image/PNG" data-preview-file-type="text" data-preview-class="bg-default">
											</div>
											<h5><p class="text-danger">* ukuran foto 150x40</p></h5>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											<label for="" class="control-label">Upload Icon :</label>
										</div>
									</div>
									<div class="form-group">
											<div class="col-md-7">
												<input id="input-20" type="file" name="file_icon" class="file" accept="image/JPG,image/jpg,image/jpeg,image/png,image/PNG,image/ICO" data-preview-file-type="text" data-preview-class="bg-default">
											</div>
											<h5><p class="text-danger">* ukuran foto 30x30</p></h5>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											<div class="pull-right">
												<button name="simpanfile" type="submit" class="btn btn-labeled btn-primary"  value="simpanfile">
													<span class="btn-label">
														<i class="ti-save"></i>
													</span> Simpan
												</button>
												<button type="button" class="btn btn-labeled btn-danger" onClick="document.location='<?php echo site_url('pengaturan/configurasi'); ?>'">
													<span class="btn-label">
														<i class="ti-close"></i>
													</span> Batal
												</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<div class="background-overlay"></div>
        </section>
	<!-- airdatepicker -->
	<script src="<?php echo $themes_url; ?>vendors/moment/js/moment.min.js" type="text/javascript"></script>	
    <script src="<?php echo $themes_url; ?>vendors/datetime/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.min.js" type="text/javascript"></script>
    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.en.js" type="text/javascript"></script>
    <script src="<?php echo $themes_url; ?>js/custom_js/advanceddate_pickers.js"></script>
	
	<!-- Select2 -->
	<script src="<?php echo $themes_url; ?>vendors/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/select2/js/select2.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/selectize/js/standalone/selectize.min.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/selectric/js/jquery.selectric.min.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>js/custom_js/custom_elements.js" type="text/javascript"></script>
	
	<!-- bootstrap time picker -->
	<script src="<?php echo $themes_url; ?>vendors/clockpicker/js/bootstrap-clockpicker.min.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/datedropper/datedropper.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/timedropper/js/timedropper.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>js/custom_js/datepickers.js" type="text/javascript"></script> 