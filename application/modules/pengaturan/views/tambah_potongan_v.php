	<link href="<?php echo $themes_url; ?>vendors/daterangepicker/css/daterangepicker.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $themes_url; ?>vendors/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/datedropper/datedropper.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/timedropper/css/timedropper.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/jquerydaterangepicker/css/daterangepicker.min.css">		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>Tambah Potongan</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php site_url('dashboard');?>">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li> <a href="<?php site_url('pengaturan');?>">Pengaturan</a></li>
				<li> <a href="<?php site_url('pengaturan/gaji');?>">Gaji & LTHR</a></li>
				<li> Tambah Potongan </li>
            </ol>
		</section>
		<!-- Main content -->
        <section class="content p-l-r-15">
            <div class="row">			
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
							<h4 class="panel-title">
                                <i class="ti-support"></i> Tambah Potongan
                            </h4>	
                        </div>
						<div class="panel-body">
							<div class="col-xs-12">
								<form class="form-horizontal" action="<?php echo base_url('pengaturan/tambahkomponen_pot');?>" method="post">
									
									<div class="form-group">
										<label for="" class="control-label col-md-3">Nama Potongan</label>
										<div class="col-md-7">
											<input name="nm_pot" class="form-control" placeholder="Isikan Nama Potongan">
										</div>
									</div>
									<div class="form-group">
										<label for="" class="control-label col-md-3">Tipe</label>
										<div class="col-md-7">
											<select name="tipe" class="form-control" style="width:100%" required="">
														<option value="" selected="select">Pilih Tipe</option>
														<option value="AUTO">Auto</option>
														<option value="MANUAL">Manual</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label for="" class="control-label col-md-3">Status</label>
										<div class="col-md-7">
											<select name="status" class="form-control" style="width:100%" required="">
														<option value="" selected="select">Pilih Status</option>
														<option value="0">Tidak Aktif</option>
														<option value="1">Aktif</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label for="" class="control-label col-md-3">Nilai</label>
										<div class="col-md-7">
												<input type="number" class="form-control" name="nilai" placeholder="Masukan Angka/Number" min=0 />
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-7 pull-right">
												<button name="submit" type="submit" class="btn btn-labeled btn-info" name="simpan" value="simpan">
													<span class="btn-label">
														<i class="ti-save"></i>
													</span> Simpan
												</button>
												<button type="button" class="btn btn-labeled btn-danger" onClick="document.location='<?php echo site_url('pengaturan/penggajian'); ?>'">
													<span class="btn-label">
														<i class="ti-close"></i>
													</span> Batal
												</button>
										</div>
									</div>
								
								</form>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<div class="background-overlay"></div>
        </section>
	<!-- airdatepicker -->
	<script src="<?php echo $themes_url; ?>vendors/moment/js/moment.min.js" type="text/javascript"></script>	
    <script src="<?php echo $themes_url; ?>vendors/datetime/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.min.js" type="text/javascript"></script>
    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.en.js" type="text/javascript"></script>
    <script src="<?php echo $themes_url; ?>js/custom_js/advanceddate_pickers.js"></script>
	
	<!-- Select2 -->
	<script src="<?php echo $themes_url; ?>vendors/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/select2/js/select2.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/selectize/js/standalone/selectize.min.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/selectric/js/jquery.selectric.min.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>js/custom_js/custom_elements.js" type="text/javascript"></script>
	
	<!-- bootstrap time picker -->
	<script src="<?php echo $themes_url; ?>vendors/clockpicker/js/bootstrap-clockpicker.min.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/datedropper/datedropper.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/timedropper/js/timedropper.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>js/custom_js/datepickers.js" type="text/javascript"></script> 