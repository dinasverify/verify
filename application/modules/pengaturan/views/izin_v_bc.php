	<!-- Sweet Alert -->
	<link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/sweetalert2/css/sweetalert2.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom_css/sweet_alert2.css">		<!-- Content Header (Page header) -->
		<section class="content-header">
            <h1>Izin</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url('dashboard');?>">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li> <a href="<?php echo site_url('pengaturan');?>">Pengaturan</a></li>
				<li> Izin </li>
            </ol>
		</section>
		<!-- Main content -->
        <section class="content p-l-r-15">
            <div class="row">			
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
							<h4 class="panel-title">
                                <i class="ti-support"></i> Izin
                            </h4>	
                        </div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12">
									<div class="row">
										<div class="col-md-10">
											<h4>1. Izin khusus apa saja yang berlaku di perusahaan Anda?</h4>
										</div>
										<div class="col-md-2">
											<div class="pull-right">
												<button type="button" id="myBtn1" class="btn btn-sm btn-default pull-right" data-target="#info" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="pull-right">
										<button class="btn btn-info btn-sm modalcolor" type="button" data-target="#tambahJenis" data-toggle="modal" data-animate-modal="fadeIn" data-modalcolor="#34586e">
											<i class="fa ti-plus" aria-hidden="true"></i> Jenis Izin Lainnya
										</button>
									</div>
								</div>
							</div></br>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="table-responsive">
										<table class="table table-striped" id="table">
											<thead style="background:#e9f0d8">
												<tr class="filters">
													<th style="width:20%">Jenis Izin Lainnya</th>
													<th style="width:20%">Maks Hari / Pengajuan</th>
													<th style="width:10%;text-align:center">Status</th>
													<th style="width:20%;text-align:center">Aksi</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Izin Menikah</td>
													<td>3</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Izin Menikahkan Anak</td>
													<td>2</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Izin Mengkhitankan Anak</td>
													<td>2</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Izin Mengkhitankan Anak</td>
													<td>2</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Izin Melahirkan</td>
													<td>92</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Izin Istri Melahirkan</td>
													<td>2</td>
													<td style="text-align:center">
														<input type="checkbox" id="toggle_real" name="my-checkbox" data-size="small" checked>
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Izin Anak Sakit</td>
													<td>2</td>
													<td style="text-align:center">
														<input type="checkbox" name="my-checkbox" data-size="small" checked id="onofftext" id="statusOnOff">
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Delete"></i></a>
													</td>
												</tr>
												<tr>
													<td>Izin Duka</td>
													<td>2</td>
													<td style="text-align:center">
														<input type="checkbox" name="my-checkbox" data-size="small" checked id="status" id="status">
													</td>
													<td style="text-align:center">
														<a href="#"><i class="fa fa-fw ti-pencil text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></a>&nbsp;&nbsp;
														<a href="#" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Delete"></i></a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							
							<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="Heading" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
											</button>
											<h4 class="modal-title custom_align" id="Heading">Delete User</h4>
										</div>
										<div class="modal-body">
											<div class="alert alert-warning">
												<span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to
												delete this Account?
											</div>
										</div>
										<div class="modal-footer ">
											<button onClick="document.location='<?php echo site_url('pengaturan'); ?>'" type="submit" class="btn btn-danger">
												<span class="glyphicon glyphicon-ok-sign"></span> Yes
											</button>
											<button type="button" class="btn btn-success" data-dismiss="modal">
												<span class="glyphicon glyphicon-remove"></span> No
											</button>
										</div>
									</div>
								</div>
							</div>
							
							<div id="tambahJenis" class="modal fade animated" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close"data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Tambah Jenis Izin Lainnya</h4>
										</div>
										<div class="modal-body">
											<div class="row">
												<div class="col-xs-12">
													<form class="form-horizontal" action="" method="post">
														<div class="form-group">
															<label for="" class="control-label col-md-5">Nama Jenis Izin</label>
															<div class="col-md-5">
																<input class="form-control" type="text" name="nama_jenis_izin" id="nama_jenis_izin" placeholder="Nama Jenis Izin" required="">
															</div>
														</div>
														<div class="form-group">
															<label for="" class="control-label col-md-5">Maksimal Pengajuan</label>
															<div class="col-md-5">
																<input class="form-control" type="text" name="maks_pengajuan" id="maks_pengajuan" placeholder="Jumlah Hari" required="">
															</div>
														</div>
														<div class="form-group">
															<label for="" class="control-label col-md-5">Status</label>
															<div class="col-md-5">
																<select class="form-control" name="" id="">
																	<option value="">Silahkan Pilih</option>
																	<option value="1">Aktif</option>
																	<option value="0">Non Aktif</option>
																</select>
															</div>
														</div>
														<div class="form-group">
															<div class="col-md-5 col-md-offset-6">
																<button type="submit" class="btn btn-labeled btn-info" data-dismiss="modal">
																		<span class="btn-label">
																		<i class="ti-save"></i>
																	</span> Simpan
																</button>
																<button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
																		<span class="btn-label">
																		<i class="ti-close"></i>
																	</span> Batal
																</button>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div id="info" class="modal fade animated" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<img src="<?php echo base_url('upload/user/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>
											<p class="text-center">
												Di tabel ini, Anda dapat menentukan jenis dan jumlah hari izin maksimal per pengajuan.
											</p>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
            <div class="background-overlay"></div>
        </section>
	<!-- Sweet Alert -->
	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/sweetalert2/js/sweetalert2.min.js"></script>
	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/sweetalert.js"></script>
	<script src="<?php echo $themes_url; ?>js/custom_js/pickers.js" type="text/javascript"></script>
