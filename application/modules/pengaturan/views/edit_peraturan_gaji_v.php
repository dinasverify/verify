	<link href="<?php echo $themes_url; ?>vendors/daterangepicker/css/daterangepicker.css" rel="stylesheet" type="text/css"/>

    <link href="<?php echo $themes_url; ?>vendors/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>

    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/datedropper/datedropper.css">

    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/timedropper/css/timedropper.css">

    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/jquerydaterangepicker/css/daterangepicker.min.css">

			<!--smoott-->   
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">
    <link href="<?php echo $themes_url; ?>vendors/hover/css/hover-min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $themes_url; ?>vendors/laddabootstrap/css/ladda-themeless.min.css">
    <link href="<?php echo $themes_url; ?>css/buttons_sass.css" rel="stylesheet">
    <link href="<?php echo $themes_url; ?>css/advbuttons.css" rel="stylesheet">


		<!-- Content Header (Page header) -->

		<section class="content-header">

			<h1>Perubahan Aturan Gaji</h1>

            <ol class="breadcrumb">

                <li>

                    <a href="<?php site_url('dashboard');?>">

                        <i class="fa fa-fw ti-home"></i> Dashboard

                    </a>

                </li>

                <li> <a href="<?php site_url('pengaturan');?>">Pengaturan</a></li>

				<li> <a href="<?php site_url('pengaturan/gaji');?>">Gaji & LTHR</a></li>

				<li> Perubahan Aturan Gaji</li>

            </ol>

		</section>

		<!-- Main content -->

		<?php 

		if(sizeof($get_aturangaji)>0){

			

			for($i=0;$i<sizeof($get_aturangaji);$i++){

				$id_ref 		= $get_aturangaji[$i]['id_referensi'];

				$nm_ref 		= $get_aturangaji[$i]['nama_ref'];

				$parentcode	 	= $get_aturangaji[$i]['parent_code'];

				$value		 	= $get_aturangaji[$i]['value'];

				$status		 	= $get_aturangaji[$i]['status'];

				

				$nominal 		= $get_aturangaji[8]['value'];

				$mindapetthr	= $get_aturangaji[2]['value'];

				$besaranthr		= $get_aturangaji[3]['value'];

				$thrminkurangthn = $get_aturangaji[4]['value'];

				$rubahkarir		 = $get_aturangaji[5]['value'];

			}

		}

		?>

        <section class="content p-l-r-15">

            <div class="row">			

                <div class="col-md-12">

                    <div class="panel">

                        <div class="panel-heading">

							<h4 class="panel-title">

                                <i class="ti-support"></i> Perubahan Aturan Gaji

                            </h4>	
                             <span class="pull-right">
	                                <i class="fa fa-fw ti-angle-up clickable"></i>
	                                <i class="fa  fa-fw ti-close removepanel clickable"></i>
                            </span>
                        </div>

						<div class="panel-body">

							<div class="col-xs-12">

								<form class="form-horizontal" action="<?php echo base_url('pengaturan/editaturangaji');?>" method="post">

									<div class="form-group">

										<div class="col-md-12">

											<label for="" class="control-label">3. Apakah perusahaan Anda menerapkan penghitungan lembur?</label>

										</div>

									</div>

									<div class="form-group">

										<div class="col-md-12">

										<?php 

											$setlembur = $get_aturangaji[0]['status'];

											if($setlembur == 1){

												echo "<input type='radio' name='radios3' value='1' class='square-blue' checked>

													<label name='no' for='Test3_1'>Yes</label>

													<input type='radio' name='radios3' value='0' class='square-blue'>

													<label name='no' for='Test3_1'>No</label>";

											}else{

												echo "<input type='radio' name='radios3' value='1' class='square-blue'>

													<label name='no' for='Test3_1'>Yes</label>

													<input type='radio' name='radios3' value='0' class='square-blue' checked>

													<label name='no' for='Test3_1'>No</label>";

											}

										?>

											

										</div>

									</div>

									<div class="form-group">

										<div class="col-md-12">

											<label for="" class="control-label">4. Pilih formula yang Anda gunakan untuk menghitung uang lembur:</label>

										</div>

									</div>

									<div class="form-group">

										<div class="col-md-7">

											<input class="form-control col-md-7" name="nilaiuanglembur" placeholder="Masukan Nominal Angka Lembur" min=0 type="number" value="<?php  echo $nominal; ?>">

										</div>

									</div>

									<div class="form-group">

										<div class="col-md-12">

											<label for="" class="control-label">5. Agar menerima Tunjangan Hari Raya (THR), berapa lama seorang personalia harus melalui masa kerja perusahaan Anda?</label>

										</div>

									</div>

									<div class="form-group">

										<div class="col-md-7">

											<input class="form-control col-md-7" name="nilai" placeholder="Masukan Angka Untuk Berapa Bulan" type="text" value="<?php echo $mindapetthr." Bulan";?>">

										</div>

									</div>

									<div class="form-group">

										<div class="col-md-12">

											<label for="" class="control-label">6. Berapa besar THR yang Anda bayarkan untuk tiap personalia di perusahaan Anda?</label>

										</div>

									</div>

									<div class="form-group">

									<?php 

										if($besaranthr == 26){

											echo "<div class='col-md-12'>

													<input type='radio' name='radios6' value='26' class='square-blue' checked>

													<label name='no' for='Test3_1'>Gaji Pokok</label>

													<div class='clearfix prettyradio labelright margin-right blue'>

													<input type='radio' name='radios6' value='27' class='square-blue' >

													<label name='no' for='Test3_1'>Gaji Pokok + Tunjangan Tetap</label></div>

												</div>";

										}elseif($besaranthr == 27){

											echo "<div class='col-md-12'>

													<input type='radio' name='radios6' value='26' class='square-blue' >

													<label name='no' for='Test3_1'>Gaji Pokok</label>

													<div class='clearfix prettyradio labelright margin-right blue'>

													<input type='radio' name='radios6' value='27' class='square-blue'checked>

													<label name='no' for='Test3_1'>Gaji Pokok + Tunjangan Tetap</label></div>

												</div>";

										}

									

									?>

										

									</div>

									<div class="form-group">

										<div class="col-md-12">

											<label for="" class="control-label">7. Untuk personalia yang bekerja kurang dari satu tahun, bagaimanakah cara Anda menentukan besaran THR untuknya?</label>

										</div>

									</div>

									<div class="form-group">

									<?php 

										if($thrminkurangthn == 29)

										{

											echo "<div class='col-md-12'>

													<input type='radio' name='radios7' value='30' class='square-blue'>

													<label name='no' for='Test3_1'>Berdasarkan jumlah bulan: antara bulan ia mulai bekerja dengan bulan pembuatan</label>

													<label name='no' for='Test3_1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;THR pertama, dibagi 12 (pembulatan ke bawah)</label>

													<div class='clearfix prettyradio labelright margin-right blue'>

													<input type='radio' name='radios7' value='29' class='square-blue'  checked>

													<label name='no' for='Test3_1'>Berdasarkan Jumlah Hari Kalender: Aantara Tanggal Ia Mulai Bekerja Hingga Tanggal Pembuatan THR Dibagi 365 Hari</label></div>

												</div>";

										}elseif($thrminkurangthn == 30){

											echo "<div class='col-md-12'>

													<input type='radio' name='radios7' value='30' class='square-blue'  checked>

													<label name='no' for='Test3_1'>Berdasarkan jumlah bulan: antara bulan ia mulai bekerja dengan bulan pembuatan</label>

													<label name='no' for='Test3_1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;THR pertama, dibagi 12 (pembulatan ke bawah)</label>

													<div class='clearfix prettyradio labelright margin-right blue'>

													<input type='radio' name='radios7' value='29' class='square-blue' >

													<label name='no' for='Test3_1'>Berdasarkan Jumlah Hari Kalender: Aantara Tanggal Ia Mulai Bekerja Hingga Tanggal Pembuatan THR Dibagi 365 Hari</label></div>

												</div>";

										}

									

									?>

									

										

									</div>

									<div class="form-group">

										<div class="col-md-12">

											<label for="" class="control-label">8. Bagaimanakah kebijakan perusahaan untuk menentukan besaran gaji bagi personalia yang mengalami</label>

											<label for="" class="control-label">&nbsp; &nbsp; perubahan karir (contoh: promosi) di tengah periode penggajian?</label>

										</div>

									</div>

									<div class="form-group">

									<?php 

										if($rubahkarir == 32){

											echo "<div class='col-md-12'>

													<input type='radio' name='radios8' value='32' class='square-blue' checked>

													<label name='no' for='Test3_1'>Proporsional. Menggabungkan perhitungan pro rata gaji lama dan gaji baru</label>

													<div class='clearfix prettyradio labelright margin-right blue'>

													<input type='radio' name='radios8' value='33' class='square-blue' >

													<label name='no' for='Test3_1'>Hanya nilai gaji baru saja yang berlaku</label></div>

												</div>";

										}elseif($rubahkarir == 33){

											echo "<div class='col-md-12'>

													<input type='radio' name='radios8' value='32' class='square-blue'>

													<label name='no' for='Test3_1'>Proporsional. Menggabungkan perhitungan pro rata gaji lama dan gaji baru</label>

													<div class='clearfix prettyradio labelright margin-right blue'>

													<input type='radio' name='radios8' value='33' class='square-blue' checked>

													<label name='no' for='Test3_1'>Hanya nilai gaji baru saja yang berlaku</label></div>

												</div>";

										}

									?>

										

									</div>

									<div class="form-group">

										<div class="col-md-12">

											<div class="pull-right">

												<button name="submit" type="submit" class="btn btn-labeled btn-primary"  value="simpan">

													<span class="btn-label">

														<i class="ti-save"></i>

													</span> Simpan

												</button>

												<button type="button" class="btn btn-labeled btn-danger" onClick="document.location='<?php echo site_url('pengaturan/penggajian'); ?>'">

													<span class="btn-label">

														<i class="ti-close"></i>

													</span> Batal

												</button>

											</div>

										</div>

									</div>

								</form>

							</div>

						</div>

						

					</div>

				</div>

			</div>

			<div class="background-overlay"></div>

        </section>

	<!-- airdatepicker -->

	<script src="<?php echo $themes_url; ?>vendors/moment/js/moment.min.js" type="text/javascript"></script>	

    <script src="<?php echo $themes_url; ?>vendors/datetime/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.min.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.en.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>js/custom_js/advanceddate_pickers.js"></script>

	

	<!-- Select2 -->

	<script src="<?php echo $themes_url; ?>vendors/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/select2/js/select2.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/selectize/js/standalone/selectize.min.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/selectric/js/jquery.selectric.min.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>js/custom_js/custom_elements.js" type="text/javascript"></script>

	

	<!-- bootstrap time picker -->

	<script src="<?php echo $themes_url; ?>vendors/clockpicker/js/bootstrap-clockpicker.min.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/datedropper/datedropper.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/timedropper/js/timedropper.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>js/custom_js/datepickers.js" type="text/javascript"></script> 