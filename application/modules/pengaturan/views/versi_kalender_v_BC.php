	<!-- Kalendar -->
	<link href="<?php echo $themes_url; ?>vendors/fullcalendar/css/fullcalendar.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $themes_url; ?>vendors/fullcalendar/css/fullcalendar.print.css" rel="stylesheet" media='print' type="text/css">
	<link href="<?php echo $themes_url; ?>css/calendar_custom.css" rel="stylesheet" type="text/css"/>
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>Kalender Perusahaan</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php base_url('dashboard');?>">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li> <a href="<?php base_url('pengaturan');?>">Pengaturan</a></li>
                <li> <a href="<?php base_url('pengaturan/kalender');?>">Kalender</a></li>
				<li> Versi Kalender </li>
            </ol>
		</section>
		<!-- Main content -->
        <section class="content p-l-r-15">
            <div class="row">			
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
							<h4 class="panel-title">
                                <i class="ti-calendar"></i> Kalender Perusahaan
                            </h4>	
                        </div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12">
									<div class="col-md-3">
										<div class="box">
											<div class="box-title">
												<h3>Draggable Events</h3>
												<div class="pull-right box-toolbar">
													<a href="#" class="btn btn-link btn-xs" data-toggle="modal" data-target="#myModal">
														<i class="fa ti-plus"></i>
													</a>
												</div>
											</div>
											<div class="box-body">
												<div id='external-events'>
													<div class='external-event palette-warning'>Team Out</div>
													<div class='external-event palette-primary'>Product Seminar</div>
													<div class='external-event palette-danger'>Client Meeting</div>
													<div class='external-event palette-info'>Repeating Event</div>
													<div class='external-event palette-success'>Anniversary Celebrations</div>
													<p class="well no-border no-radius">
														<input type='checkbox' class="custom_icheck" id='drop-remove'/>
														<label for='drop-remove'>remove after drop</label>
													</p>
												</div>
											</div>
											<div class="box-footer">
												<a href="#" class="btn btn-success btn-block" data-toggle="modal" data-target="#myModal">Create
													event</a>
											</div>
										</div>
									</div>
									<div class="col-md-9">
										<div class="box">
											<div class="box-body">
												<div id="calendar"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
					
						</div>
					</div>
				</div>
			</div>
			<div class="background-overlay"></div>
        </section>
	<!-- Kalender -->
	<script src="<?php echo $themes_url; ?>vendors/moment/js/moment.min.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/fullcalendar/js/fullcalendar.min.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/iCheck/js/icheck.js"></script>
	<script src="<?php echo $themes_url; ?>js/custom_js/calendar_custom.js" type="text/javascript"></script>
