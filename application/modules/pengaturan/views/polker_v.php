		<!--smoott-->   
	<link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">
	<link href="<?php echo $themes_url; ?>vendors/hover/css/hover-min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo $themes_url; ?>vendors/laddabootstrap/css/ladda-themeless.min.css">
	<link href="<?php echo $themes_url; ?>css/buttons_sass.css" rel="stylesheet">
	<link href="<?php echo $themes_url; ?>css/advbuttons.css" rel="stylesheet">
	<!-- Sweet Alert -->


		<!-- Content Header (Page header) -->

		<section class="content-header">

            <h1>Pola dan Jadwal Kerja</h1>

            <ol class="breadcrumb">

                <li>

                    <a href="<?php echo site_url('dashboard');?>">

                        <i class="fa fa-fw ti-home"></i> Dashboard

                    </a>

                </li>

                <li> <a href="<?php echo site_url('pengaturan');?>">Pengaturan</a></li>

				<li> Pola dan Jadwal Kerja </li>

            </ol>

		</section>

		<!-- Main content -->

        <section class="content p-l-r-15">

            <div class="row">			

                <div class="col-md-12">
                	<?php echo $this->session->flashdata('pesan'); ?>
                    <div class="panel">

                        <div class="panel-heading">

							<h4 class="panel-title">

                                <i class="ti-calendar"></i> Pola dan Jadwal Kerja

                            </h4>	
                              <span class="pull-right">
	                                <i class="fa fa-fw ti-angle-up clickable"></i>
	                                <i class="fa  fa-fw ti-close removepanel clickable"></i>
                            </span>

                        </div>

						<div class="panel-body">

							<div class="row">

								<div class="col-xs-12">

									<div class="pull-right">

										<button type="button" id="myBtn1" class="btn btn-sm btn-default" style="border-radius:10px" data-target="#info" data-toggle="modal" data-animate-modal="fadeIn"><i class="fa fa-question" data-toggle="tooltip" data-placement="top" title data-original-title="Klik Disini Untuk Membaca Petunjuk"></i></button>

									</div>

								</div>

							</div></br>

							

							<div class="row">

								<div class="col-xs-12">

									<div class="pull-right">

										<a href="<?php echo site_url('pengaturan/tambahpolker'); ?>" class="btn btn-primary btn-sm" data-style="Tambah Pola Kerja">

											<i class="fa ti-plus" aria-hidden="true"></i> Tambah Pola Kerja

										</a>

									</div>

								</div>

							</div></br>

							

							<div class="row">

								<div class="col-xs-12">

									<div class="table-responsive">

										<table class="table table-striped" id="sample_1">

											<thead style="background:#F3F7F9">

												<tr class="filters">

													<th style="width:5%">No</th>

													<th style="width:10%">Nama Pola Kerja</th>

													<th style="width:10%">Jumlah Hari Kerja</th>

													<th style="width:7%">Toleransi Kerja</th>

													<th style="width:10%">Jumlah Libur</th>

													<th style="width:7%">Lama Pola</th>

													<th style="width:13%">Jadwal Masuk</th>

													<th style="width:13%">Jadwal Pulang</th>

													<th style="width:15%;text-align:center">Aksi</th>

												</tr>

											</thead>

											<tbody>

										<?php $no=1; foreach($polakerja as $polker){ ?>

												<tr>

													<td><?php echo $no++;?></td>

													<td><?php echo $polker['nama_pola'];?></td>

													<td><?php echo $polker['jumlah_hari_kerja'];?></td>

													<td><?php echo $polker['toleransi_late'];?></td>

													<td><?php echo $polker['jumlah_libur'];?></td>

													<td><?php echo $polker['lama_pola'];?></td>

													<td><?php echo $polker['jadwal_masuk'];?></td>

													<td><?php echo $polker['jadwal_keluar'];?></td>

													<td style="text-align:center">

														<a class="btn-xs btn-warning" href="<?php echo site_url("pengaturan/updatepolker?id=".$polker['id_pola'])?>"><i class="fa fa-fw ti-pencil text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i></a>&nbsp;&nbsp;

														<a  class="btn-xs btn-danger" href="<?php echo site_url("pengaturan/deletepolker?id=".$polker['id_pola'])?>" class="hapus text-center"><i class="fa fa-fw ti-trash text-default actions_icon" data-toggle="tooltip" data-placement="top" title data-original-title="Delete"></i></a>

													</td>

												</tr>

										<?php } ?>

											</tbody>

										</table>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

            <div class="background-overlay"></div>

    </section>
          	<script> 
		$('#notifications').slideDown('slow').delay(3500).slideUp('slow');
	</script>


	<!-- Sweet Alert -->

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/sweetalert2/js/sweetalert2.min.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/sweetalert.js"></script>

	

	<!-- airdatepicker -->

	<script src="<?php echo $themes_url; ?>vendors/moment/js/moment.min.js" type="text/javascript"></script>	

    <script src="<?php echo $themes_url; ?>vendors/datetime/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.min.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.en.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>js/custom_js/advanceddate_pickers.js"></script>

	

	<!-- DataTable -->

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/jquery.dataTables.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/datatables/js/dataTables.bootstrap.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/datatables_custom.js"></script>

	

	<!-- datatable -->

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/editable-table/js/mindmup-editabletable.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/bootstrap-table/js/bootstrap-table.min.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/tableExport.jquery.plugin/tableExport.min.js"></script>

	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/bootstrap_tables.js"></script>

