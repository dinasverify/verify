				<!--smoott-->   
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">
    <link href="<?php echo $themes_url; ?>vendors/hover/css/hover-min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $themes_url; ?>vendors/laddabootstrap/css/ladda-themeless.min.css">
    <link href="<?php echo $themes_url; ?>css/buttons_sass.css" rel="stylesheet">
    <link href="<?php echo $themes_url; ?>css/advbuttons.css" rel="stylesheet">
	<link href="<?php echo $themes_url; ?>vendors/daterangepicker/css/daterangepicker.css" rel="stylesheet" type="text/css"/>


	<link href="<?php echo $themes_url; ?>vendors/daterangepicker/css/daterangepicker.css" rel="stylesheet" type="text/css"/>

    <link href="<?php echo $themes_url; ?>vendors/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>

    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/datedropper/datedropper.css">

    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/timedropper/css/timedropper.css">

    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/jquerydaterangepicker/css/daterangepicker.min.css">

		<!-- Content Header (Page header) -->

		<section class="content-header">

			<h1>Tambah Golongan Gaji</h1>

            <ol class="breadcrumb">

                <li>

                    <a href="<?php site_url('dashboard');?>">

                        <i class="fa fa-fw ti-home"></i> Dashboard

                    </a>

                </li>

                <li> <a href="<?php site_url('pengaturan');?>">Pengaturan</a></li>

				<li> <a href="<?php site_url('pengaturan/gaji');?>">Gaji & LTHR</a></li>

				<li> Tambah Golongan Gaji</li>

            </ol>

		</section>

		<!-- Main content -->

        <section class="content p-l-r-15">

            <div class="row">			

                <div class="col-md-12">

                    <div class="panel">

                        <div class="panel-heading">

							<h4 class="panel-title">

                                <i class="ti-support"></i> Tambah Golongan Gaji

                            </h4>	
                               <span class="pull-right">
	                                <i class="fa fa-fw ti-angle-up clickable"></i>
	                                <i class="fa  fa-fw ti-close removepanel clickable"></i>
                            </span>

                        </div>

						<div class="panel-body">

							<div class="col-xs-12">

								<form class="form-horizontal" action="<?php echo base_url('pengaturan/tambahitempendapatan');?>" method="post">

									<div class="form-group">

										<label for="" class="control-label col-md-2">Jabatan</label>

										<div class="col-md-10">

											<select name="namagrup"class="form-control">

												<?php 

												

												if(sizeof($item_grup_gaji)>0){

													

													$no = 1;

													for($i=0;$i<sizeof($item_grup_gaji);$i++){

														$idgrup 	= $item_grup_gaji[$i]["id_grup"];

														$nama_grup 	= $item_grup_gaji[$i]["nama_grup"];

														echo "<option value=".$no++.">".$nama_grup."</option>";

													}

												}

												?>

											</select>

										</div>

									</div>

									<div class="form-group">

										<label for="" class="control-label col-md-2">Komponen Pendapatan</label>

										<div class="col-md-10">

											<select name="komponen_pend"class="form-control">

												<?php if(sizeof($komponen_pend)>0){

													for($i=0;$i<sizeof($komponen_pend);$i++){

														$id_pend 	= $komponen_pend[$i]['id_kom_pend'];

														$nama_pend 	= $komponen_pend[$i]['nama_pendapatan'];

														echo "<option value=".$id_pend.">(".$id_pend.") ".$nama_pend."</option>";

													}

												}?>

											</select>

										</div>

									</div>
									<hr>
									<div class="form-group">

										<div class="col-md-12">

											<div class="pull-right">

												<button name="submit" type="submit" class="btn btn-labeled btn-primary" name="simpan" value="simpan">

													<span class="btn-label">

														<i class="ti-save"></i>

													</span> Simpan

												</button>

												<button type="button" class="btn btn-labeled btn-danger" onClick="document.location='<?php echo site_url('pengaturan/penggajian'); ?>'">

													<span class="btn-label">

														<i class="ti-close"></i>

													</span> Batal

												</button>

											</div>

										</div>

									</div>

								</form>

							</div>

						</div>

						

					</div>

				</div>

			</div>

			<div class="background-overlay"></div>

        </section>

	<!-- airdatepicker -->

	<script src="<?php echo $themes_url; ?>vendors/moment/js/moment.min.js" type="text/javascript"></script>	

    <script src="<?php echo $themes_url; ?>vendors/datetime/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.min.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.en.js" type="text/javascript"></script>

    <script src="<?php echo $themes_url; ?>js/custom_js/advanceddate_pickers.js"></script>

	

	<!-- Select2 -->

	<script src="<?php echo $themes_url; ?>vendors/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/select2/js/select2.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/selectize/js/standalone/selectize.min.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/selectric/js/jquery.selectric.min.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>js/custom_js/custom_elements.js" type="text/javascript"></script>

	

	<!-- bootstrap time picker -->

	<script src="<?php echo $themes_url; ?>vendors/clockpicker/js/bootstrap-clockpicker.min.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/datedropper/datedropper.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>vendors/timedropper/js/timedropper.js" type="text/javascript"></script>

	<script src="<?php echo $themes_url; ?>js/custom_js/datepickers.js" type="text/javascript"></script> 