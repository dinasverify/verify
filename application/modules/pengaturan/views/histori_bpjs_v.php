	<!-- Sweet Alert -->
	<link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/sweetalert2/css/sweetalert2.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom_css/sweet_alert2.css">
		<!-- Content Header (Page header) -->
		<section class="content-header">
           <h1>Histori BPJS</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php base_url('dashboard');?>">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li> <a href="<?php base_url('pengaturan');?>">Pengaturan</a></li>
				<li> <a href="<?php base_url('pengaturan/bpjs');?>">BPJS</a></li>
				<li> Histori BPJS </li>
			</ol>
		</section>
		<!-- Main content -->
        <section class="content p-l-r-15">
            <div class="row">			
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
							<h4 class="panel-title">
                                <i class="ti-briefcase"></i> Histori BPJS
                            </h4>	
                        </div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<ul class="nav nav-tabs" role="tablist">
										<li role="presentation">
											<a href="<?php echo site_url('pengaturan/bpjs'); ?>" class="text-hr">
												<span>
													<i class="fa fa-arrow-left" aria-hidden="true"></i>
												</span>
											</a>
										</li>
										<li role="presentation" class="active">
											<a href="#historiKer" class="text-hr" aria-controls="historiKer" role="tab" data-toggle="tab">
												<span class="hidden-xs">
													Histori BPJS Ketenagakerjaan
												</span>
												<span class="hidden-sm hidden-md hidden-lg">
													BPJS Ker
												</span>
											</a>
										</li>
										<li role="presentation">
											<a href="#historiKes" class="text-hr" aria-controls="historiKes" role="tab" data-toggle="tab">
												<span class="hidden-xs">
													Histori BPJS Kesehatan 
												</span>
												<span class="hidden-sm hidden-md hidden-lg">
													BPJS Kes
												</span>
											</a>
										</li>
									</ul>

									<div class="tab-content">
										<div role="tabpanel" class="tab-pane active" id="historiKer">
											<table class="table tabel-striped">
												<thead>
													<th width="30">No.</th>
													<th width="200">Tanggal Berlaku</th>
													<th>Detail</th>
													<th>Status Berlaku (Ya/Tidak)</th>
													<th width="50">*****</th>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
										<div role="tabpanel" class="tab-pane" id="historiKes">
											<table class="table tabel-stiped">
												<thead>
													<th width="30">No.</th>
													<th width="200">Tanggal Berlaku</th>
													<th>Detail</th>
													<th>Status Berlaku (Ya/Tidak)</th>
													<th width="50">*****</th>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
									</div>
									
									<div id="infoUMP" class="modal fade animated" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-body">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<img src="<?php echo base_url('upload/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>
													<p class="text-center">
														Besar UMP akan mempengaruhi perhitungan BPJS. Jika Gaji Pokok+Tunjangan Tetap kurang dari UMP, maka basis pengali perhitungan iuran BPJS akan menggunakan nilai UMP.
													</p>
												</div>
											</div>
										</div>
									</div>
									
									<div id="infoBPJS" class="modal fade animated" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-body">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<img src="<?php echo base_url('upload/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>
													<p class="text-center">
														Pengaturan ini akan mempengaruhi penghitungan gaji.
													</p>
												</div>
											</div>
										</div>
									</div>
									
									<div id="infoMetode" class="modal fade animated" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-body">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<img src="<?php echo base_url('upload/employee.png'); ?>" alt="" class="center-block" style="width:20%;height:50%"></br>
													<p class="text-center">
														Metode Gross-up yaitu PPh 21 dibayar perusahaan dan dimasukkan sebagai tunjangan penghasilan untuk karyawan. Metode Gross yaitu karyawan membayar sendiri PPh 21-nya. Metode Nett yaitu PPh 21 dibayar perusahaan, tapi tidak dimasukkan sebagai unsur penghasilan karyawan.
													</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
            <!-- row-->
        </section>
		<div class="background-overlay"></div>		
	<!-- Sweet Alert -->
	<script type="text/javascript" src="<?php echo $themes_url; ?>vendors/sweetalert2/js/sweetalert2.min.js"></script>
	<script type="text/javascript" src="<?php echo $themes_url; ?>js/custom_js/sweetalert.js"></script>