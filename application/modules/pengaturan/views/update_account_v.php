<!--smoott-->   
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>css/custom.css">
    <link href="<?php echo $themes_url; ?>vendors/hover/css/hover-min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $themes_url; ?>vendors/laddabootstrap/css/ladda-themeless.min.css">
    <link href="<?php echo $themes_url; ?>css/buttons_sass.css" rel="stylesheet">
    <link href="<?php echo $themes_url; ?>css/advbuttons.css" rel="stylesheet">

<script>
$(document).ready(function () {

	$("#oldpass").change(function (e){
			e.preventDefault();
			var id = <?=$id;?>;
			var oldpassword=$("#oldpass").val();
			var minLength=5;
			var newpassword=$("#newpass").val();
			var cfpassword=$("#newpass2").val();
			check_password_ajax(id,oldpassword,newpassword,cfpassword,minLength);
    }); 
    
	$("#newpass").change(function(e){
			e.preventDefault();
			var id = <?=$id;?>;
			var oldpassword=$("#oldpass").val();
			var minLength=5;
			var newpassword=$("#newpass").val();
			var cfpassword=$("#newpass2").val();
			check_password_ajax(id,oldpassword,newpassword,cfpassword,minLength);	

	});
	
	$("#newpass2").change(function(e){
			e.preventDefault();
			var id = <?=$id;?>;
			var oldpassword=$("#oldpass").val();
			var minLength=5;
			var newpassword=$("#newpass").val();
			var cfpassword=$("#newpass2").val();
			check_password_ajax(id,oldpassword,newpassword,cfpassword,minLength);				
	});
	
	$("#email").change(function (e){
			e.preventDefault();
			var email = $(this).val();
			check_username_ajax(email);
    });
	
	function check_password_ajax(id,oldpassword,newpassword,cfpassword,minLength){
		$.post('<?php echo site_url();?>personalia/cek_pass', {'id':id, 'password':oldpassword} ,function(data) 
		{
		if (data.status==0)
		{
		$("#msg").html(data.msg);	
		btn_submit.disabled = true;
		}else{
			$("#msg").html("");		
			if (newpassword.length < minLength) {
				$("#pass1-check").html("<font color='red'>Panjang Password tidak boleh kurang dari 8 karakter!</font>");
				btn_submit.disabled = true;
			}else{
				$("#pass1-check").html("");
				if(cfpassword!=newpassword){
					$("#pass2-check").html("<font color='red'>Konfirmasi Password Tidak Sama.</font>");
					btn_submit.disabled = true;
				}else{
					$("#pass2-check").html("");				
					btn_submit.disabled = false;
				}
			}		
		}
		});			
	}	
	
	function check_username_ajax(email)
	{
		$.post('<?php echo site_url();?>personalia/cek_email', {'email':email}, function(data) {
			$("#user-result").html(data);
		});
	}
	
	function isEmail(email) {
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return regex.test(email);
	}
	
	$("#editpassForm").submit(function(e) {
		e.preventDefault();
		if(!isEmail($("#email").val())){
			alert('Format Email Salah, Silahkan Cek Ulang Email Anda.');	
			return;
		}

		if ($.trim( $('#user-result').html()).length){

			alert('Email Sudah Terdaftar Silahkan Gunakan Email Lain.');	
			return;
		}
		
		if ($("#oldpass").val()==='' || $("#newpass").val()==='' ||  $("#newpass2").val()==='' || $("#email").val()==='')
		{
			$('#alert').html('<font color="red">Harap Lengkapi Form</font>');
		}else{
			var id = <?=$id;?>;
			var newpass = $("#newpass").val();
			var email = $("#email").val();
			updatepasswordemail(id,newpass,email);
		}
	});
		
	function updatepasswordemail(id,newpass,email)
	{
		$.post('<?php echo site_url();?>personalia/update_password_email', {'id':id, 'newpass':newpass, 'email':email} ,function(data) 
		{
			if (data.status==1){
				$('#alert-success').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button><i class="fa fa-check"></i><p> ' +data.msg+ '</p></div>');
			}else{
				$('#alert-success').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button><i class="fa fa-info-circle"></i><p> ' +data.msg+ ' </p></div>');

			}
		});			
	}
});
</script>

		<!-- Content Header (Page header) -->
		<section class="content-header">
            <h1>Personalia</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url($themes_url,'dashboard');?>">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li> <a href="<?php echo site_url('personalia/account_setting');?>">Pengaturan Account</a></li>
                <li> Edit Akun </li>
				<li><?php echo $nama_karyawan;?></li>
            </ol>
		</section>
		<!--section ends-->
        <section class="content p-l-r-15">
            <div class="row">
				<div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="fa fa-fw ti-settings"></i> Edit Data Account
                            </h3>
                            <span class="pull-right">
                                    <i class="fa fa-fw ti-angle-up clickable"></i>
                                    <i class="fa  fa-fw ti-close removepanel clickable"></i>
                            </span>
                        </div>
                        <div class="panel-body">
							<?php if($msg != ""){ ?>
							<div class="row">
									<div class="col-xs-12">
										<div class="alert alert-danger alert-gdj" role="alert">
											<i class="fa fa-info-circle icon_id"></i>
											<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
											<?php echo $msg;?> 
										</div>
									</div>
							</div>
							<?php } ?>
							<div id="alert-success"></div>
							<form class="form-horizontal" action="<?php echo base_url('pengaturan/updateaccount?id='.$id_user);?>" method="post">
								<div class="form-group">
                                    <label for="" class="col-md-2 control-label">Email</label>
									<div class="col-md-10">
										<input type="email" id="email" name="email" class="form-control" required="" value="<?php echo $email;?>" placeholder="Email">
										<div id="msg"></div>
									</div>
								</div>
								<div class="form-group">
                                    <label for="" class="col-md-2 control-label">User Group</label>
									<div class="col-md-10">
									<?php if($user == 1){ ?>
                                        <select name="usergroup" class="form-control">
											<option value=''>Pilih User Group</option>
											<option <?php if($id_user==1){echo "selected";}?> value="1">Super Admin</option>
											<option <?php if($id_user==2){echo "selected";}?> value="2">Admin</option>
											<option <?php if($id_user==3){echo "selected";}?> value="3">Member</option>
											
											
										</select>
									<?php  }elseif($user == 2){ ?>
										 <select name="usergroup" class="form-control">
											<option value=''>Pilih User Group</option>
											<option <?php if($id_user==2){echo "selected";}?> value="2">Admin</option>
											<option <?php if($id_user==3){echo "selected";}?> value="3">Member</option>
										</select>
									<?php }elseif($user == 3){ ?>
										<select name="usergroup" class="form-control">
											<option value=''>Pilih User Group</option>
											<option <?php if($id_user==3){echo "selected";}?> value="3">Member</option>
										</select>
									<?php } ?>
									<!-- <?php if($user == 2){ ?>
										<select name="usergroup" class="form-control">
											<option value=''>Pilih User Group</option>
											<option value="3">User</option>
											<option value="2">Admin</option>
										</select>
									<?php } ?>
									<?php if($usergroup == 3){ ?>
										<select name="usergroup" class="form-control">
											<option value=''>Pilih User Group</option>
											<option value="3">User</option>
										</select>
									<?php } ?> --> 
									
									</div>
                                </div>
								<div class="form-group">
                                    <label for="" class="col-md-2 control-label">Password Lama</label>
									<div class="col-md-10">
										<input type="password" id="oldpass" name="oldpass" class="form-control" required="" value="" placeholder="Password Lama">
										<div id="msg"></div>
									</div>
								</div>
								<div class="form-group">
                                    <label for="" class="col-md-2 control-label">Password Baru</label>
									<div class="col-md-10">
										<input type="password" id="newpass" name="newpass" class="form-control" required="" value="" placeholder="Password Lama">
										<div id="pass1-check"></div>
									</div>
								</div>
								<div class="form-group">
                                    <label for="" class="col-md-2 control-label">Konfirmasi Password</label>
									<div class="col-md-10">
										<input type="password" id="newpass2" name="newpass2" class="form-control" required="" value="" placeholder="Konfirmasi Password">
										<div id="pass2-check"></div>
									</div>
								</div>
								<div class="row">
									<hr>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<div class="pull-right">
											<button type="submit" class="btn btn-sm btn-labeled btn-primary" name="update">
												<span class="btn-label">
													<i class="ti-save"></i>
												</span> update
											</button>
											<button type="button" class="btn btn-sm btn-labeled btn-danger" onclick="document.location='<?php echo site_url('pengaturan/accountuser'); ?>'">
												<span class="btn-label">
													<span class="glyphicon glyphicon-remove"></span>
												</span> Batal
											</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="background-overlay"></div>
		</section>
	
	<!-- airdatepicker -->
	<script src="<?php echo $themes_url; ?>vendors/moment/js/moment.min.js" type="text/javascript"></script>	
    <script src="<?php echo $themes_url; ?>vendors/datetime/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.min.js" type="text/javascript"></script>
    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.en.js" type="text/javascript"></script>
    <script src="<?php echo $themes_url; ?>js/custom_js/advanceddate_pickers.js"></script>
	
	<!-- Select2 -->
	<script src="<?php echo $themes_url; ?>vendors/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/select2/js/select2.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/selectize/js/standalone/selectize.min.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/selectric/js/jquery.selectric.min.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>js/custom_js/custom_elements.js" type="text/javascript"></script>
