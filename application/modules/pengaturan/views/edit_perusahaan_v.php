	<link href="<?php echo $themes_url; ?>vendors/daterangepicker/css/daterangepicker.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $themes_url; ?>vendors/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/datedropper/datedropper.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/timedropper/css/timedropper.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $themes_url; ?>vendors/jquerydaterangepicker/css/daterangepicker.min.css">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>Pengajuan Izin</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php site_url('dashboard');?>">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li> <a href="<?php site_url('sakit');?>">Sakit & Izin</a></li>
				<li> <a href="<?php site_url('sakit/pengajuanizin');?>">Pengajuan Izin</a></li>
				<li> Tambah Pengajuan Izin Lainnya </li>
            </ol>
		</section>
		<!-- Main content -->
        <section class="content p-l-r-15">
            <div class="row">			
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
							<h4 class="panel-title">
                                <i class="menu-icon fa fa-cogs fa-lg fa-fw"></i> Setting Nama Perusahaan 
                            </h4>	
                        </div>
						<div class="panel-body">
							<div class="col-xs-12">
								<form class="form-horizontal" action="<?php echo base_url('pengaturan/editsetperusahaan?id='.$id_perusahaan);?>" method="post">
									<div class="form-group">
										<label for="" class="control-label col-md-3">Nama Perusahaan</label>
										<div class="col-md-7">
											<input type='hidden' class='form-control' name='id_perusahaan' id='namaperusahaan'  value="<?php echo $id_perusahaan;?>" required="">
											<input type='text' class='form-control' name='namaperusahaan' id='namaperusahaan'  value="<?php echo $namaperusanaan;?>" required="">
										</div>
									</div>
									<div class="form-group">
										<label for="" class="control-label col-md-3">Bergerak dibidang</label>
										<div class="col-md-7">
											<input type='text' class='form-control' name='bidang' id='bidang'  value="<?php echo $bidang;?>" required="">
										</div>
									</div>
									<div class="form-group">
										<label for="" class="control-label col-md-3">Alamat</label>
										<div class="col-md-7">
											<input type='text' class='form-control' name='alamat' id='alamat'  value="<?php echo $alamat;?>" required="">
										</div>
									</div>
									<div class="form-group">
										<label for="" class="control-label col-md-3">Logo Dalam</label>
										<div class="col-md-7">
											<input id="input-22" type="file" name="file_logodalam" class="file" accept="image/jpg,image/jpeg,image/png" data-preview-file-type="text" data-preview-class="bg-default">
										</div>
									</div>
									
									<div class="row">
										<hr>
									</div>
									<div class="form-group">
										<div class="col-md-10">
											<div class="pull-right">
												<button type="submit" class="btn btn-sm btn-labeled btn-primary" name="simpan" value="simpan">
													<span class="btn-label">
														<i class="ti-save"></i>
													</span> Simpan
												</button>
												<button type="button" class="btn btn-sm btn-labeled btn-danger"onClick="document.location='<?php echo site_url('pengaturan/izin'); ?>'">
													<span class="btn-label">
														<span class="glyphicon glyphicon-remove"></span>
													</span> Batal
												</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<div class="background-overlay"></div>
        </section>
	<!-- airdatepicker -->
	<script src="<?php echo $themes_url; ?>vendors/moment/js/moment.min.js" type="text/javascript"></script>	
    <script src="<?php echo $themes_url; ?>vendors/datetime/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.min.js" type="text/javascript"></script>
    <script src="<?php echo $themes_url; ?>vendors/airdatepicker/js/datepicker.en.js" type="text/javascript"></script>
    <script src="<?php echo $themes_url; ?>js/custom_js/advanceddate_pickers.js"></script>
	
	<!-- Select2 -->
	<script src="<?php echo $themes_url; ?>vendors/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/select2/js/select2.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/selectize/js/standalone/selectize.min.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/selectric/js/jquery.selectric.min.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>js/custom_js/custom_elements.js" type="text/javascript"></script>
	
	<!-- bootstrap time picker -->
	<script src="<?php echo $themes_url; ?>vendors/clockpicker/js/bootstrap-clockpicker.min.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/datedropper/datedropper.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>vendors/timedropper/js/timedropper.js" type="text/javascript"></script>
	<script src="<?php echo $themes_url; ?>js/custom_js/datepickers.js" type="text/javascript"></script> 