<!DOCTYPE html>
<html>

<head>
    <title>Registrasi</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php echo base_url($folder_themes.'img/ico-pjn.png'); ?>"/>
    <!-- Bootstrap -->
    <link href="<?php echo base_url($folder_themes.'css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- end of bootstrap -->
    <!--page level css -->
    <link type="text/css" href="<?php echo base_url($folder_themes.'vendors/themify/css/themify-icons.css'); ?>" rel="stylesheet"/>
    <link href="<?php echo base_url($folder_themes.'vendors/iCheck/css/all.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url($folder_themes.'vendors/bootstrapvalidator/css/bootstrapValidator.min.css'); ?>" rel="stylesheet"/>
    <link href="<?php echo base_url($folder_themes.'css/login.css'); ?>" rel="stylesheet">
    <!--end page level css-->
</head>

<body id="sign-in">
<div class="preloader">
    <div class="loader_img"><img src="<?php echo base_url($folder_themes.'img/loader.gif'); ?>" alt="loading..." height="64" width="64"></div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 login-form">
            <div class="panel-header">
                <h2 class="text-center">
                    <img src="<?php echo base_url($folder_themes.'img/logo-pjn.png'); ?>" style="width:190px;height:85px;border:0;" alt="Logo">
                </h2>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12">
                        <?php if($this->session->flashdata('pesan')){ ?>
                        <div class="alert alert-danger text-center">
                            <i class="fa fa-info-circle icon_id"></i>
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                            <p class="alert"><?php echo $this->session->flashdata('pesan'); ?></p>
                        </div>
                        <?php } ?>
                        <form action="<?php echo site_url('auth/registrasi'); ?>" id="authentication" method="post" class="login_validator">
                            <div class="form-group">
                                <label for="firstname" class="sr-only"> Nama Depan</label>
                                <input type="text" class="form-control  form-control-lg" id="firstname" name="firstname"
                                       placeholder="Nama Depan">
                            </div>
                            <div class="form-group">
                                <label for="lastname" class="sr-only"> Nama Belakang</label>
                                <input type="text" class="form-control  form-control-lg" id="lastname" name="lastname"
                                       placeholder="Nama Belakang">
                            </div>
                            <div class="form-group">
                                <label for="email" class="sr-only"> Email</label>
                                <input type="text" class="form-control  form-control-lg" id="email" name="email"
                                       placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label for="telepon" class="sr-only"> Handphone</label>
                                <input type="text" class="form-control  form-control-lg" id="telepon" name="telepon"
                                       placeholder="Handphone">
                            </div>
                            <div class="form-group">
                                <label for="password" class="sr-only">Password</label>
                                <input type="password" class="form-control form-control-lg" id="password"
                                       name="password" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <label for="password" class="sr-only">Password Ulang</label>
                                <input type="password" class="form-control form-control-lg" id="password"
                                       name="password" placeholder="Password Ulang">
                            </div>
                            <div class="form-group">
                                <input type="submit" name="daftar" value="Daftar" class="btn btn-primary btn-block"/>
                            </div>
                            <p class="text-center">
                            	<a href="<?php echo site_url('auth/login'); ?>" id="login" class="login"> Klik Login </a> jika sudah memiliki akun.
                            </p>
	
                        </form>
                    </div>
                </div>
            </div>
            <div class="panel-header">
                <p class="text-center">&copy; 2018. PT. RS Jantung Harapan Kita</p>
            </div>              
        </div>
    </div>
</div>
<!-- global js -->
<script src="<?php echo base_url($folder_themes.'js/jquery.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url($folder_themes.'js/bootstrap.min.js'); ?>" type="text/javascript"></script>
<!-- end of global js -->
<!-- page level js -->
<script type="text/javascript" src="<?php echo base_url($folder_themes.'vendors/iCheck/js/icheck.js'); ?>"></script>
<script src="<?php echo base_url($folder_themes.'vendors/bootstrapvalidator/js/bootstrapValidator.min.js'); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url($folder_themes.'js/custom_js/login.js'); ?>"></script>
<!-- end of page level js -->
</body>

</html>
