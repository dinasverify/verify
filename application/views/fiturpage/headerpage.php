<!doctype html>
<!--[if lt IE 7]><html lang="en" class="no-js ie6"><![endif]-->
<!--[if IE 7]><html lang="en" class="no-js ie7"><![endif]-->
<!--[if IE 8]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <title>Human Resource</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="shortcut icon" href="<?php echo $themes_url; ?>Oleose-master/favicon.png">
    
    <!-- Bootstrap 3.3.2 -->
    <link rel="stylesheet" href="<?php echo $themes_url; ?>Oleose-master/assets/css/bootstrap.min.css">
    
    <link rel="stylesheet" href="<?php echo $themes_url; ?>Oleose-master/assets/css/animate.css">
    <link rel="stylesheet" href="<?php echo $themes_url; ?>Oleose-master/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo $themes_url; ?>Oleose-master/assets/css/slick.css">
    <link rel="stylesheet" href="<?php echo $themes_url; ?>Oleose-master/assets/js/rs-plugin/css/settings.css">

    <link rel="stylesheet" href="<?php echo $themes_url; ?>Oleose-master/assets/css/styles.css">


    <script type="text/javascript" src="<?php echo $themes_url; ?>Oleose-master/assets/js/modernizr.custom.32033.js"></script>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <div class="pre-loader">
        <div class="load-con">
            <img src="<?php echo $themes_url; ?>payroll/assets/logo.png" class="animated fadeInDown" alt="">
            <div class="spinner">
              <div class="bounce1"></div>
              <div class="bounce2"></div>
              <div class="bounce3"></div>
            </div>
        </div>
    </div>
   
    <header style="background:#0088cc;">
       
    </header>