<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Auth {

	/**
	 *
	 * @var array $exceptions Urls which don't need auth (publicly accessible). 
	 * Use ci's query string format
	 */
	public $exceptions;

	/**
	 *  
	 * @var array $permissions Additional permissions which you don't have in db. Use ci's query string format, example:
	 * array(
	 * 		array(
	 * 			'group_id' => 1,
	 * 			'url' => array('menu', 'dashboard/edit', 'user_management')
	 * 		),
	 * 		array(
	 * 			'group_id' => 2,
	 * 			'url' => array('dashboard', )
	 * 		)
	 * );
	 */
	public $permissions;

	function __construct() {

		// Get CI instance
		$this->ci = & get_instance();
		//$this->ci->output->enable_profiler(TRUE);
		// Get current controller & method
		//$controller = get_class($this->ci);
		//$method 	= get_class_methods($this->ci);
		//$current_controller = $this->ci->router->fetch_class();
		//$current_method = $this->ci->router->fetch_method();
		//print_r($current_controller); print_r(','); print_r($current_method); exit();

		$this->exceptions = array(
			'welcome' => 1,
			'auth' => 1, // handled by ion auth
			//'rest' => 1,
			'api' => 1,
			//'ads/detail' => 1, // for mobile
			//'uploader' => 1,
			//'kiosk' => 1,
            //'payment' => 1,
            //'bookingscheduler' => 1,
            'home' => 1,
            //'topup' => 1,
            'card_tap' => 1,
            'job' => 1,
            'message' => 1,
            'tambah_kartu' => 1
            
		);

		$this->permissions = array(
			array(
				'group_id' => 1,
				'url' => array('menu', 'home')
			),
		);


		$this->ci->load->library('uri');


		//$this->unit_test();
	}

	function check_permission() {

		// Get current uri string	
		$uri_string = $this->ci->uri->uri_string();
		
		// empty uri string redirect to default controller
		if (empty($uri_string)) {
			redirect($this->ci->router->default_controller);
		}

		// If current uri string is in exception list, then do not continue check auth
		if ($this->in_exceptions($uri_string)) {
			return;
		}

		// Get logged in user & group info
		$user_id = $this->ci->ion_auth->get_user_id();
		if (empty($user_id)) {
			redirect('auth/login');
			//show_error('Access Denied', 401);			
		}
		// @TODO optimize, use session
		$user_groups = $this->ci->ion_auth->get_users_groups($user_id)->result();

		if (empty($user_groups)) {
			show_error('You don\'t belong to a group', 401);
		}

		foreach ($user_groups as $group) {
			$group_ids[] = (int) $group->id;
		}

		// Check access
		$has_access = $this->has_access($group_ids, $uri_string);
		if ($has_access) {
			return;
		}

		// Show error when no access found
		show_error('Access Denied', 401); // not authed
	}

	function in_exceptions($uri_string) {

		// check for exact string
		// Use isset for better performance http://nickology.com/2012/07/03/php-faster-array-lookup-than-using-in_array/
		if (isset($this->exceptions[$uri_string])) {
			return TRUE;
		}

		// check for strpos
		// use strpos instead of regex http://micro-optimization.com/strpos-vs-preg_match.html
		foreach ($this->exceptions as $key => $value) {
			//var_dump($key);
			if (!empty($key)) {
				if (strpos($uri_string, $key) === 0) { // starts with
					return TRUE;
				}
			}
		}

		return FALSE;
	}

	function has_access($group_ids, $uri_string) {
		if (empty($group_ids)) {
			return FALSE;
		}

		// Check from variable
		if (!empty($this->permissions)) {
			foreach ($group_ids as $group_id) {
				foreach ($this->permissions as $key => $p) {
					foreach ($p['url'] as $url) {
						if ($group_id == $p['group_id'] && strpos($uri_string, $url) === 0) {
							return TRUE;
						}
					}
				}
			}
		}

		// check from db
		$in_sql = implode(',', array_fill(0, count($group_ids), '?'));
		$sql = "SELECT DISTINCT REPLACE(t_menu.url, 'index.php/', '') AS url
				  FROM t_menu, t_menu_group 
				 WHERE t_menu.id = t_menu_group.menu_id
				   AND t_menu.status = TRUE
				   AND group_id IN($in_sql)";

		$query = $this->ci->db->query($sql, $group_ids);

		$result = $query->result_array($query);
		foreach ($result as $row) {
			if (strpos($uri_string, $row['url']) === 0) {
				return TRUE;
			}
		}

		return FALSE;
	}

	function unit_test() {

		echo 'Auth test';
		$this->ci->load->library('unit_test');
		$this->ci->load->library('uri');

		$urls = array('', 'menu', 'store', 'blablabla', 'test2');
		$groups = array(1, 2); // group id

		foreach ($urls as $u) {

			$test = $this->in_exceptions($u);
			switch ($u) {
				case '':
				case 'welcome':
					$expected_result = TRUE;
					echo $this->ci->unit->run($test, $expected_result, 'In Exception', 'is TRUE ? "" OR welcome == ' . $u);
					break;
				case 'beacon':
				case 'beacon/welcome':
				case 'ads':
					$expected_result = FALSE;
					echo $this->ci->unit->run($test, $expected_result, 'In Exception', 'is FALSE ? beacon OR beacon/welcome OR ads != ' . $u);
					break;
			}


			$test = $this->has_access($groups, $u);
			switch ($u) {
				case 'menu':
				case 'store':
				case 'ads':
					$expected_result = TRUE;
					echo $this->ci->unit->run($test, $expected_result, 'Has Access', 'is TRUE ? menu OR store OR ads == ' . $u);
					break;
				case 'unknown_url':
				default:
					$expected_result = FALSE;
					echo $this->ci->unit->run($test, $expected_result, 'Has Access', 'is FALSE ? default == ' . $u);
					break;
			}
		}
	}

}

?>
